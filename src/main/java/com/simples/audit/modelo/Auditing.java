package com.simples.audit.modelo;

import com.simples.controleacesso.modelo.Aplicacao;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.xpert.audit.model.AbstractAuditing;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author
 */
@Entity(name = "log.tb_auditoria")
@Table(schema = "log")
public class Auditing extends AbstractAuditing implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Usuario usuario;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "auditing")
    private List<Metadata> metadatas;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Aplicacao aplicacao;
    
    @Transient
    private String descricao;
    
    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Aplicacao getAplicacao() {
        return aplicacao;
    }

    public void setAplicacao(Aplicacao aplicacao) {
        this.aplicacao = aplicacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public List getMetadatas() {
        return metadatas;
    }

    @Override
    public void setMetadatas(List metadatas) {
        this.metadatas = metadatas;
    }

    @Override
    public String getUserName() {
        if (usuario != null) {
            return usuario.getNome();
        }
        return null;
    }
}
