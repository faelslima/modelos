package com.simples.audit.modelo;

import com.xpert.audit.model.AbstractAuditing;
import com.xpert.audit.model.AbstractMetadata;
import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author
 */
@Entity(name = "log.tb_metadados")
@Table(schema = "log")
public class Metadata extends AbstractMetadata implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private Auditing auditing;
    
    @Override
    public Long getId() {
        return id;
    }

    /**
     * max size to be save in coluns "newValue" and "oldValue"
     *
     * @return
     */
    @Override
    public Integer getMaxSizeValues() {
        return 255;
    }

    @Override
    public AbstractAuditing getAuditing() {
        return auditing;
    }

    @Override
    public void setAuditing(AbstractAuditing auditing) {
        this.auditing = (Auditing) auditing;
    }
}
