package com.simples.comum.bo;

import com.xpert.core.crud.AbstractBusinessObject;
import com.simples.comum.dao.BancoDAO;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.simples.comum.modelo.Banco;
import com.xpert.persistence.query.LikeType;
import com.xpert.persistence.query.QueryBuilder;
import javax.faces.model.SelectItem;

/**
 *
 * @author Rafael Lima
 */
@Stateless
public class BancoBO extends AbstractBusinessObject<Banco> {

    @EJB
    private BancoDAO bancoDAO;
    
    @Override
    public BancoDAO getDAO() {
        return bancoDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return null;
    }

    @Override
    public void validate(Banco banco) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    public List<Banco> getBancoByName(String banco) {
        QueryBuilder queryBuilder = bancoDAO.getQueryBuilder();        
        List<Banco> listaBancos = queryBuilder
                .from(Banco.class)
                .startGroup()
                .like("codigo", banco, LikeType.BOTH)
                .or()
                .like("descricao", banco, LikeType.BOTH)
                .endGroup()
                .orderBy("descricao ASC")
                .setMaxResults(10)
                .getResultList();
        return listaBancos;
    }

    public SelectItem[] getSelectItens() {
        SelectItem[] listaItens = null;
        List<Banco> list = getDAO().listAll("descricao");

        if (list != null && !list.isEmpty()) {
            listaItens = new SelectItem[list.size()];
            for (int x = 0; x < list.size(); x++) {
                SelectItem item = new SelectItem(list.get(x), list.get(x).toString());
                listaItens[x] = item;
            }
        }
        return listaItens;
    }

}
