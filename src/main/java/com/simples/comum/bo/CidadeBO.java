package com.simples.comum.bo;

import com.xpert.core.crud.AbstractBusinessObject;
import com.simples.comum.dao.CidadeDAO;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.simples.comum.modelo.Cidade;
import com.simples.util.Utils;
import com.xpert.persistence.query.Restriction;
import com.xpert.persistence.query.Restrictions;
import java.util.ArrayList;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;

/**
 *
 * @author Rafael Lima
 */
@Stateless
public class CidadeBO extends AbstractBusinessObject<Cidade> {

    @EJB
    private CidadeDAO cidadeDAO;

    @Override
    public CidadeDAO getDAO() {
        return cidadeDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return null;
    }

    @Override
    public void validate(Cidade cidade) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    public List<Cidade> getCidadeByName(String cidade) {
        Restrictions rest = new Restrictions();
        rest.like("comum.sem_acento(descricao)", Utils.removeAcentuacao(cidade));
       
        return cidadeDAO.getQueryBuilder()
                .from(Cidade.class)
                .add(rest)
                .setMaxResults(10)
                .getResultList();
    }

    public SelectItem[] getSelectItens() {
        SelectItem[] listaItens = null;
        List<Cidade> list = getDAO().listAll("uf, descricao");

        if (list != null && !list.isEmpty()) {
            listaItens = new SelectItem[list.size()];
            for (int x = 0; x < list.size(); x++) {
                SelectItem item = new SelectItem(list.get(x), list.get(x).toString());
                listaItens[x] = item;
            }
        }
        return listaItens;
    }
    
    public List<SelectItem> getSelectItensCidadeGroup() {

        List<SelectItem> lista = new ArrayList<SelectItem>();

        lista.add(getSelectItemGroup("AC", "Acre"));
        lista.add(getSelectItemGroup("AL", "Alagoas"));
        lista.add(getSelectItemGroup("AP", "Amapá"));
        lista.add(getSelectItemGroup("AM", "Amazonas"));
        lista.add(getSelectItemGroup("BA", "Bahia"));
        lista.add(getSelectItemGroup("CE", "Ceará"));
        lista.add(getSelectItemGroup("DF", "Distrito Federal"));
        lista.add(getSelectItemGroup("ES", "Espírito Santo"));
        lista.add(getSelectItemGroup("GO", "Goiás"));
        lista.add(getSelectItemGroup("MA", "Maranhão"));
        lista.add(getSelectItemGroup("MT", "Mato Grosso"));
        lista.add(getSelectItemGroup("MS", "Mato Grosso do Sul"));
        lista.add(getSelectItemGroup("MG", "Minas Gerais"));
        lista.add(getSelectItemGroup("PA", "Pará"));
        lista.add(getSelectItemGroup("PB", "Paraíba"));
        lista.add(getSelectItemGroup("PR", "Paraná"));
        lista.add(getSelectItemGroup("PE", "Pernambuco"));
        lista.add(getSelectItemGroup("PI", "Piauí"));
        lista.add(getSelectItemGroup("RJ", "Rio de Janeiro"));
        lista.add(getSelectItemGroup("RN", "Rio Grande do Norte"));
        lista.add(getSelectItemGroup("RS", "Rio Grande do Sul"));
        lista.add(getSelectItemGroup("RO", "Rondônia"));
        lista.add(getSelectItemGroup("RR", "Roraima"));
        lista.add(getSelectItemGroup("SC", "Santa Catarina"));
        lista.add(getSelectItemGroup("SE", "Sergipe"));
        lista.add(getSelectItemGroup("TO", "Tocantins"));

        return lista;
    }

    private SelectItemGroup getSelectItemGroup(String uf, String nomeEstado) {

        SelectItemGroup UF = new SelectItemGroup(nomeEstado);
        List<Cidade> cidades = getDAO().list(new Restriction("uf", uf), "descricao");
        SelectItem[] itens = new SelectItem[cidades.size()];

        for (int x = 0; x < cidades.size(); x++) {
            itens[x] = new SelectItem(cidades.get(x), cidades.get(x).toString());
        }

        UF.setSelectItems(itens);
        return UF;
    }

}
