package com.simples.comum.bo;

import com.xpert.core.crud.AbstractBusinessObject;
import com.simples.comum.dao.CnaeDAO;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.simples.comum.modelo.Cnae;
import com.simples.util.Utils;
import com.xpert.persistence.query.Restriction;
import com.xpert.persistence.query.Restrictions;

/**
 *
 * @author Rafael Lima
 */
@Stateless
public class CnaeBO extends AbstractBusinessObject<Cnae> {

    @EJB
    private CnaeDAO cnaeDAO;

    @Override
    public CnaeDAO getDAO() {
        return cnaeDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return null;
    }

    @Override
    public void validate(Cnae cnae) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    public Cnae getCnae(String subclasse) {
        if (subclasse != null && !subclasse.isEmpty()) {
            subclasse = Utils.removeSpecialChars(subclasse);
            if (subclasse != null && subclasse.length() > 7) {
                subclasse = subclasse.substring(0, 6);
            }
            Restrictions rest = new Restrictions();
            rest.like("translate(subclasse,'-./','')", subclasse);
            return getDAO().unique(rest);
        }
        return null;
    }
}
