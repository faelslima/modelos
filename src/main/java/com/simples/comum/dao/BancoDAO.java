package com.simples.comum.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.comum.modelo.Banco;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface BancoDAO extends BaseDAO<Banco> {

    public void gerarBancos();
    
}
