package com.simples.comum.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.comum.modelo.Cidade;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface CidadeDAO extends BaseDAO<Cidade> {

    public void gerarCidades();

}
