package com.simples.comum.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.comum.modelo.Cnae;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface CnaeDAO extends BaseDAO<Cnae> {

    public void gerarCnaes();
    
}
