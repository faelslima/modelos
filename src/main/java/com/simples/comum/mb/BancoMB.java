package com.simples.comum.mb;


import java.io.Serializable;
import com.xpert.core.crud.AbstractBaseBean;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.simples.comum.bo.BancoBO;
import com.simples.comum.modelo.Banco;
import com.simples.util.Utils;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author Rafael Lima
 */
@ManagedBean
@ViewScoped
public class BancoMB extends AbstractBaseBean<Banco> implements Serializable {

    @EJB
    private BancoBO bancoBO;

    @Override
    public BancoBO getBO() {
        return bancoBO;
    }
    
    @Override
    public void createDataModel() {
        if (Utils.getCurrentView().startsWith("list")) {
            super.createDataModel();
        }
    }

    @Override
    public String getDataModelOrder() {
        return "id";
    }
    
    public List<Banco> getBancoByName(String banco) {
        return bancoBO.getBancoByName(banco);
    }
    
    public SelectItem[] getSelectItens() {
        return getBO().getSelectItens();
    }
}
