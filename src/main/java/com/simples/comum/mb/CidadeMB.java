package com.simples.comum.mb;

import java.io.Serializable;
import com.xpert.core.crud.AbstractBaseBean;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import com.simples.comum.bo.CidadeBO;
import com.simples.comum.modelo.Cidade;
import com.simples.util.Utils;
import java.util.List;
import javax.faces.model.SelectItem;

/**
 *
 * @author Rafael Lima
 */
@ManagedBean
@ViewScoped
public class CidadeMB extends AbstractBaseBean<Cidade> implements Serializable {

    private List<Cidade> listaCidades;

    @EJB
    private CidadeBO cidadeBO;

    @Override
    public CidadeBO getBO() {
        return cidadeBO;
    }

    @Override
    public void createDataModel() {
        if (Utils.getCurrentView().startsWith("list")) {
            super.createDataModel();
        }
    }

    @Override
    public String getDataModelOrder() {
        return "id";
    }

    public List<Cidade> getCidadeByName(String cidade) {
        return cidadeBO.getCidadeByName(cidade);
    }

    public List<Cidade> getListaCidades() {
        listaCidades = getDAO().listAll("uf, descricao");
        return listaCidades;
    }

    public void setListaCidades(List<Cidade> listaCidades) {
        this.listaCidades = listaCidades;
    }

    public SelectItem[] getSelectItens() {
        return getBO().getSelectItens();
    }

}
