/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.comum.modelo;

import com.simples.util.Utils;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author rafael
 */
@Entity(name = "comum.ti_cidade")
@Table(schema = "comum")
public class Cidade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "codigo_ibge", length = 8)
    private String codigoIbge;

    @NotBlank
    @Size(min = 3, max = 200, message = "Nome da Cidade não deve ser menor que 3 caracteres.")
    @Column(length = 200)
    private String descricao;

    @NotBlank
    @Size(min = 2, message = "UF precisa conter 2 caracteres")
    @Column(length = 2)
    private String uf;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getCodigoIbge() {
        return codigoIbge;
    }
    
    public void setCodigoIbge(String codigoIbge) {
        this.codigoIbge = codigoIbge;
    }
    
    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public String getUf() {
        return uf;
    }
    
    public void setUf(String uf) {
        this.uf = uf;
    }
    
    public String getCidadeEstado() {
        if (descricao != null && !descricao.equals("")) {
            return Utils.getCapitular(descricao) + ", " + uf;
        }
        return "";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cidade other = (Cidade) obj;
        return !(this.id != other.id && (this.id == null || !this.id.equals(other.id)));
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        if (descricao != null && uf != null) {
            return Utils.getCapitular(descricao) + ", " + uf.toUpperCase();
        }
        return null;
    }
    //</editor-fold>
}
