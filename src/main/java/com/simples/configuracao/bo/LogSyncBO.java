package com.simples.configuracao.bo;

import com.simples.configuracao.dao.ErroSistemaDAO;
import com.simples.configuracao.modelo.ErroSistema;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.rest.modelo.LogSyncDTO;
import com.simples.util.Constantes;
import com.simples.util.SessaoUtils;
import com.simples.util.Utils;
import static com.simples.util.Utils.postJson;
import static com.simples.util.Utils.removeAcentuacao;
import com.xpert.faces.utils.FacesUtils;
import com.xpert.i18n.I18N;
import java.util.Date;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * @author Rafael Lima
 */
@Stateless
public class LogSyncBO {

    @EJB
    private ErroSistemaDAO erroSistemaDAO;

    public static void registerLog(String tipo, String subTipo, String historico, boolean erro, String sistema) {
        Map<String, String> headers = FacesUtils.getHeaders();
        if (headers != null && !headers.isEmpty()) {
            String serverName = headers.get("serverName");
            boolean ambienteDeTeste = serverName.contains("localhost") || serverName.contains("test");

            if (!ambienteDeTeste) {
                LogSyncDTO log = new LogSyncDTO();
                log.setDataHora(new Date());
                log.setErro(erro ? "S" : "N");

                String usuario = SessaoUtils.getUser() != null ? SessaoUtils.getUser().getUserLogin() : SessaoUtils.getUsuarioCliente() != null ? SessaoUtils.getUsuarioCliente().getUsername() : "SEM USERNAME";

                log.setUsuario(usuario);
                log.setEstacaoIp(FacesUtils.getIP());

                String aplicacao = (sistema == null || sistema.isEmpty()) ? SessaoUtils.getAplicacaoLogada() != null ? removeAcentuacao(SessaoUtils.getAplicacaoLogada().getDescricao().toUpperCase()) : null : sistema;

                log.setSistema(aplicacao);
                log.setTipo(removeAcentuacao(tipo));
                log.setSubtipo(removeAcentuacao(subTipo));
                log.setHistorico(removeAcentuacao(historico));
                log.setEstacaoHostname(headers.get("host"));
                log.setEstacaoVariaveisAmbiente(headers.get("user-agent"));
                log.setVersaoSistema(I18N.get("app.versao"));

                Empresa empresa = SessaoUtils.getEmpresa();
                String empresaRazaoSocial = empresa != null ? Utils.removeAcentuacao(empresa.getRazaoSocial()) : null;
                String empresaRazaoCnpj = empresa != null ? empresa.getCnpj() : null;

                log.setLicencaEmpresaRazaoSocial(empresaRazaoSocial);
                log.setLicencaEmpresaCnpj(empresaRazaoCnpj);

                String gson = LogSyncDTO.toJson(log);
                postJson(Constantes.URL_XDATA_LOGSERVICE, gson);
            }
        }
    }

    public static void registerLog(String tipo, String subTipo, String historico, boolean erro) {
        registerLog(tipo, subTipo, historico, erro, null);
    }

    public void registraLogErroRestFull(ErroSistema erro) {
        Map<String, String> headers = FacesUtils.getHeaders();
        if (headers != null && !headers.isEmpty()) {
            String serverName = headers.get("serverName");
            boolean ambienteDeTeste = serverName.contains("localhost") || serverName.contains("app-test");

            if (!ambienteDeTeste) {
                LogSyncDTO log = new LogSyncDTO();
                log.setDataHora(erro.getData());
                log.setErro("S");

                String usuario = erro.getUsuario() != null ? erro.getUsuario().getUserLogin() : "SEM USERNAME";

                log.setUsuario(usuario);
                log.setEstacaoIp(FacesUtils.getIP());
                log.setHistorico((removeAcentuacao(erro.getPilhaErro())).replace("<br/>", "\n"));

                String aplicacao = SessaoUtils.getAplicacaoLogada() != null ? removeAcentuacao(SessaoUtils.getAplicacaoLogada().getDescricao().toUpperCase()) : null;

                log.setSistema(aplicacao);
                log.setTipo("ERRO INTERNO");
                log.setSubtipo(removeAcentuacao(erro.getFuncionalidade().toUpperCase()));
                log.setEstacaoHostname(headers.get("host"));
                log.setEstacaoVariaveisAmbiente(headers.get("user-agent"));
                log.setVersaoSistema(I18N.get("app.versao"));

                registerLog(log, erro);
            }
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private void registerLog(LogSyncDTO log, ErroSistema erro) {
        String gson = LogSyncDTO.toJson(log);
        String[] tentativaEnvioJson = postJson(Constantes.URL_XDATA_LOGSERVICE, gson);
        if (tentativaEnvioJson != null) {
            erro.setQuantidadeTentativasEnvioLog(1);
            String tipoCodigo = tentativaEnvioJson[0];
            if (tipoCodigo.equals("ERRO")) {
                String codigoErro = tentativaEnvioJson[1];
                if (erro.getCodigoErroEnvioLog() != null) {
                    codigoErro = erro.getCodigoErroEnvioLog() + " | " + codigoErro;
                }
                erro.setCodigoErroEnvioLog(codigoErro);

            } else if (tipoCodigo.equals("SUCESSO")) {
                erro.setCodigoSucessoLog(tentativaEnvioJson[1]);
            }
            erroSistemaDAO.saveOrMerge(erro, false);
        }
    }

}
