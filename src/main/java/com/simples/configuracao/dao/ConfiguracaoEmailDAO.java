package com.simples.configuracao.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.email.modelo.ConfiguracaoEmail;
import javax.ejb.Local;
import javax.mail.Session;

/**
 *
 * @author ayslan
 */
@Local
public interface ConfiguracaoEmailDAO extends BaseDAO<ConfiguracaoEmail> {
    
    public Session getJavaMailSession();
    
}
