/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.configuracao.dao;

import com.simples.configuracao.modelo.ErroSistema;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Rafael
 */
@Local
public interface ErroSistemaDAO extends BaseDAO<ErroSistema> {
    
}
