package com.simples.configuracao.dao;

import com.simples.email.modelo.ModeloEmail;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author ayslan
 */
@Local
public interface ModeloEmailDAO extends BaseDAO<ModeloEmail> {
    
}
