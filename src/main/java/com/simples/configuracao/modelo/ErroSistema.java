package com.simples.configuracao.modelo;

import com.simples.controleacesso.modelo.Aplicacao;
import com.simples.util.Constantes;
import com.simples.controleacesso.modelo.Usuario;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author ayslan
 */
@Entity(name = "log.tb_erro_sistema")
@Table(schema = "log")
public class ErroSistema implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    private Date data;

    @Column(length = 500)
    private String informacaoNavegador;

    @Column(columnDefinition = Constantes.TIPO_TEXTO_BANCO)
    private String pilhaErro;

    @Column(length = 190)
    private String funcionalidade;

    @Column(length = 500)
    private String url;
    @ManyToOne
    private Usuario usuario;
    
    @ManyToOne(fetch = FetchType.LAZY)
    private Aplicacao aplicacao;
    
    @Column(name = "quantidade_tentantivas_envio_log")
    private Integer quantidadeTentativasEnvioLog;
    
    @Column(name = "codigo_erro_envio_log", columnDefinition = "text")
    private String codigoErroEnvioLog;
    
    @Column(name = "codigo_sucesso_log")
    private String codigoSucessoLog;

    private static final String PACOTE_PADRAO_SISTEMA = "com.simples.financeiro";

    public String getPilhaErroFormatada() {
        if (pilhaErro != null && pilhaErro.length() > 60) {
            try {
                BufferedReader bufferedReader = new BufferedReader(new StringReader(pilhaErro));
                String linha;
                StringBuilder stringBuilder = new StringBuilder();
                while ((linha = bufferedReader.readLine()) != null) {
                    if (linha.contains(PACOTE_PADRAO_SISTEMA)) {
                        stringBuilder.append("<span style='background: #EEDD82'>").append(linha.replace(PACOTE_PADRAO_SISTEMA, "<b>" + PACOTE_PADRAO_SISTEMA + "</b>")).append("</span>");
                    } else {
                        stringBuilder.append(linha);
                    }
                }
                bufferedReader.close();
                return stringBuilder.toString();
            } catch (IOException ex) {
                return pilhaErro;
            }
        }
        return pilhaErro;
    }

    public String getResumo() {
        if (pilhaErro != null && pilhaErro.length() > 60) {
            return pilhaErro.substring(0, 59) + "...";
        }
        return pilhaErro;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getInformacaoNavegador() {
        return informacaoNavegador;
    }

    public void setInformacaoNavegador(String informacaoNavegador) {
        this.informacaoNavegador = informacaoNavegador;
    }

    public Integer getQuantidadeTentativasEnvioLog() {
        return quantidadeTentativasEnvioLog;
    }

    public void setQuantidadeTentativasEnvioLog(Integer quantidadeTentativasEnvioLog) {
        this.quantidadeTentativasEnvioLog = quantidadeTentativasEnvioLog;
    }

    public String getCodigoErroEnvioLog() {
        return codigoErroEnvioLog;
    }

    public void setCodigoErroEnvioLog(String codigoErroEnvioLog) {
        this.codigoErroEnvioLog = codigoErroEnvioLog;
    }

    public String getCodigoSucessoLog() {
        return codigoSucessoLog;
    }

    public void setCodigoSucessoLog(String codigoSucessoLog) {
        this.codigoSucessoLog = codigoSucessoLog;
    }

    public String getPilhaErro() {
        return pilhaErro;
    }

    public void setPilhaErro(String pilhaErro) {
        this.pilhaErro = pilhaErro;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFuncionalidade() {
        return funcionalidade;
    }

    public void setFuncionalidade(String funcionalidade) {
        this.funcionalidade = funcionalidade;
    }

    public Aplicacao getAplicacao() {
        return aplicacao;
    }

    public void setAplicacao(Aplicacao aplicacao) {
        this.aplicacao = aplicacao;
    }
}
