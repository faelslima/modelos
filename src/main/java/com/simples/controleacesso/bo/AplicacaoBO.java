package com.simples.controleacesso.bo;

import com.simples.controleacesso.dao.AplicacaoDAO;
import com.xpert.core.crud.AbstractBusinessObject;
import com.xpert.persistence.dao.BaseDAO;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.simples.controleacesso.modelo.Aplicacao;

/**
 *
 * @author ayslan
 */
@Stateless
public class AplicacaoBO extends AbstractBusinessObject<Aplicacao> {

    @EJB
    private AplicacaoDAO aplicacaoDAO;

    @Override
    public BaseDAO getDAO() {
        return aplicacaoDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return null;
    }

    /**
     * Registra um acesso ao sistema de um usuario especifico
     *
     * @param aplicacao
     * @throws com.xpert.core.exception.BusinessException
     */
    @Override
    public void validate(Aplicacao aplicacao) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    public Aplicacao getAplicacao(String nomeAplicacao) {
        Aplicacao app = aplicacaoDAO.unique("descricao", nomeAplicacao.toUpperCase());
            if (app == null) {
                app = new Aplicacao();
                app.setDescricao(nomeAplicacao);
                app = aplicacaoDAO.merge(app, false);
            }
        return app;
    }
}
