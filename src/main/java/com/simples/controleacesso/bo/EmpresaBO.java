package com.simples.controleacesso.bo;

import com.simples.controleacesso.dao.EmpresaLicenciadaDAO;
import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.xpert.core.crud.AbstractBusinessObject;
import com.simples.financeiro.dao.administracao.EmpresaDAO;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.util.SessaoUtils;
import com.xpert.core.validation.UniqueFields;
import com.xpert.persistence.query.Restrictions;
import javax.faces.model.SelectItem;

/**
 *
 * @author Rafael Lima
 */
@Stateless
public class EmpresaBO extends AbstractBusinessObject<Empresa> {

    @EJB
    private EmpresaDAO empresaDAO;
    @EJB
    private EmpresaLicenciadaDAO empresaLicenciadaDAO;

    @Override
    public EmpresaDAO getDAO() {
        return empresaDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return new UniqueFields().add("cnpj");
    }

    @Override
    public void validate(Empresa empresa) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    @Override
    public void save(Empresa empresa) throws BusinessException {
        super.save(empresa);
    }

    public Empresa getEmpresaByCnpj(String cnpj) {
        Restrictions rest = new Restrictions();
        rest.add("cnpj", cnpj);
        return empresaDAO.unique(rest);
    }
    
    public SelectItem[] getSelectItens(List<Empresa> list) {
        SelectItem[] listaItens = null;

        if (list != null && !list.isEmpty()) {
            listaItens = new SelectItem[list.size()];
            for (int x = 0; x < list.size(); x++) {
                Empresa emp = list.get(x) ;
                SelectItem item = new SelectItem(emp, emp.toString());
                listaItens[x] = item;
            }
        }
        return listaItens;
    }

    public SelectItem[] getSelectItens(Usuario usuario) {
        List<Empresa> list = null;
        if (SessaoUtils.isSuperUsuario()) {
            list = getDAO().listAll("razaoSocial");
            
        } else {
            Empresa empresa = SessaoUtils.getEmpresa();
            if (empresa != null && usuario.getEmpresasFinanceiro() != null) {
                list = getDAO().getInitialized(usuario.getEmpresasFinanceiro());
            }
        }
        return getSelectItens(list);
    }
    
    public SelectItem[] getSelectItensEmpresasLicenciadas(List<EmpresaLicenciada> list) {
        SelectItem[] listaItens = null;

        if (list != null && !list.isEmpty()) {
            listaItens = new SelectItem[list.size()];
            for (int x = 0; x < list.size(); x++) {
                EmpresaLicenciada emp = list.get(x) ;
                SelectItem item = new SelectItem(emp, emp.toString());
                listaItens[x] = item;
            }
        }
        return listaItens;
    }

    public SelectItem[] getSelectItensEmpresasLicenciadas(Usuario usuario) {
        List<EmpresaLicenciada> list = null;
        if (SessaoUtils.isSuperUsuario()) {
            list = empresaLicenciadaDAO.listAll("razaoSocial");
            
        } else {
            EmpresaLicenciada empresa = SessaoUtils.getEmpresaLicenciada();
//            if (empresa != null && usuario.getEmpresasLicenciadas()!= null) {
//                list = getDAO().getInitialized(usuario.getEmpresasLicenciadas());
//            }
        }
        return getSelectItensEmpresasLicenciadas(list);
    }
}
