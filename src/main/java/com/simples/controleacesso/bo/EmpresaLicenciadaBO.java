package com.simples.controleacesso.bo;

import com.simples.controleacesso.dao.EmpresaLicenciadaDAO;
import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.xpert.core.crud.AbstractBusinessObject;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Rafael Lima
 */
@Stateless
public class EmpresaLicenciadaBO extends AbstractBusinessObject<EmpresaLicenciada> {

    @EJB
    private EmpresaLicenciadaDAO empresaLicenciadaDAO;

    @Override
    public EmpresaLicenciadaDAO getDAO() {
        return empresaLicenciadaDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return null;
    }

    @Override
    public void validate(EmpresaLicenciada empresaLicenciada) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    @Override
    public void save(EmpresaLicenciada empresaLicenciada) throws BusinessException {
        super.save(empresaLicenciada);
    }
}
