package com.simples.controleacesso.bo;

import com.simples.controleacesso.dao.PerfilDAO;
import com.simples.controleacesso.dao.PerfilPermissaoDAO;
import com.simples.controleacesso.dao.PermissaoDAO;
import com.simples.controleacesso.dao.UsuarioDAO;
import com.simples.controleacesso.modelo.Perfil;
import com.simples.controleacesso.modelo.PerfilPermissao;
import com.simples.controleacesso.modelo.Permissao;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.util.SessaoUtils;
import com.xpert.core.crud.AbstractBusinessObject;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import com.xpert.core.validation.UniqueFields;
import com.xpert.persistence.query.LikeType;
import com.xpert.persistence.query.Restrictions;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.LinkedList;
import java.util.UUID;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 *
 * @author Ayslan
 */
@Stateless
public class PerfilBO extends AbstractBusinessObject<Perfil> {

    @EJB
    private PerfilDAO perfilDAO;
    @EJB
    private UsuarioDAO usuarioDAO;
    @EJB
    private PermissaoBO permissaoBO;
    @EJB
    private PermissaoDAO permissaoDAO;
    @EJB
    private PerfilPermissaoDAO perfilPermissaoDAO;

    @Override
    public PerfilDAO getDAO() {
        return perfilDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return new UniqueFields().add("descricao");
    }

    /**
     * metodo para garantir que uma permissao nao seja adicionada sem que o
     * usuario logado possua ela
     *
     * @param perfil
     */
    public void verificarPermissoesPerfil(Perfil perfil) {
        //para o super usuario isso nao e necessario
        if (SessaoUtils.getUser().isSuperUsuario() == false) {

            List<Permissao> permissoesUsuarioLogado = permissaoBO.getPermissoes(SessaoUtils.getUser());
            List<Permissao> permissoesBanco = new LinkedList();
            if (perfil.getId() != null) {
                permissoesBanco = permissaoDAO.getPermissoes(perfil);
            }
            //iterar sobre a lista das permissoes selecionadas na tela, caso o usuario nao tenha acesso a ela, ela sera removida
            List<Permissao> permissoesARemover = new LinkedList();
            List<Permissao> permissoesPerfil = new LinkedList();

            for (PerfilPermissao pp : perfil.getPerfilPermissoes()) {
                if (!permissoesPerfil.contains(pp.getPermissao())) {
                    permissoesPerfil.add(pp.getPermissao());
                }
            }

            for (Permissao permissao : permissoesPerfil) {
                //se o usuario nao contem essa permissao e ela nao esta adicionada no banco, remover
                if (!permissoesUsuarioLogado.contains(permissao) && !permissoesBanco.contains(permissao)) {
                    permissoesARemover.add(permissao);
                }
            }
            perfil.getPerfilPermissoes().removeAll(permissoesARemover);

        }
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void save(Perfil perfil) throws BusinessException {

        boolean novo = perfil.getId() == null;
        verificarPermissoesPerfil(perfil);

        if (perfil.getEmpresaLicenciada() == null) {
            perfil.setEmpresaLicenciada(SessaoUtils.getEmpresaLicenciada());
        }

        List<PerfilPermissao> listaPermissoes = new LinkedList();
        listaPermissoes.addAll(perfil.getPerfilPermissoes());
        perfil.setPerfilPermissoes(null);

        super.save(perfil);

        perfil.setPerfilPermissoes(listaPermissoes);
        postSave(perfil, novo);
        if (novo) {
            //adicionar o perfil ao usuario logado
            Usuario usuario = SessaoUtils.getUser();
            //recarregar do banco
            if (usuario != null) {
                usuario = usuarioDAO.find(usuario.getId());
                //usuario.getPerfis().add(perfil);
                usuarioDAO.merge(usuario);
            }
        }

    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void postSave(Perfil perfil, boolean perfilNovo) {
        if (!perfilNovo) {
            String sqlDelete = "DELETE FROM " + PerfilPermissao.class.getName() + " pp WHERE pp.perfil = :perfil ";
            perfilDAO.getEntityManager().createQuery(sqlDelete).setParameter("perfil", perfil).executeUpdate();
        }

        for (PerfilPermissao pp : perfil.getPerfilPermissoes()) {
            if (pp.getId() == null) {
                pp.setId(UUID.randomUUID().toString());
                pp.setPerfil(perfil);

                perfilPermissaoDAO.saveOrMerge(pp);
            }
        }
    }

    @Override
    public void validate(Perfil perfil) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    public Perfil getPerfilAplicacaoLogada(String nome) {
        Restrictions rest = new Restrictions();
        rest.like("descricao", nome, LikeType.BEGIN);
        rest.isNull("empresaLicenciada");

        return getDAO().unique(rest);
    }
}
