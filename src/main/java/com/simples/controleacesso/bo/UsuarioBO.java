package com.simples.controleacesso.bo;

import com.simples.controleacesso.dao.PerfilDAO;
import com.simples.controleacesso.dao.UsuarioDAO;
import com.simples.controleacesso.dao.UsuarioEmailDAO;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.controleacesso.modelo.Perfil;
import com.simples.controleacesso.modelo.TipoRecuperacaoSenha;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.controleacesso.modelo.UsuarioEmail;
import com.simples.financeiro.modelo.controleacesso.PerfilFinanceiro;
import com.simples.util.SessaoUtils;
import com.xpert.core.crud.AbstractBusinessObject;
import com.xpert.persistence.dao.BaseDAO;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.exception.BusinessException;
import com.xpert.core.validation.UniqueFields;
import com.xpert.persistence.query.Restrictions;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import com.xpert.utils.Encryption;
import java.security.NoSuchAlgorithmException;
import java.util.LinkedList;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author Ayslan
 */
@Stateless
public class UsuarioBO extends AbstractBusinessObject<Usuario> {

    private static final int TAMANHO_SENHA_ALEATORIA = 8;
    @EJB
    private UsuarioDAO usuarioDAO;
    @EJB
    private UsuarioEmailDAO usuarioEmailDAO;
    @EJB
    private PerfilDAO perfilDAO;
    @EJB
    private SolicitacaoRecuperacaoSenhaBO solicitacaoRecuperacaoSenhaBO;
    @EJB
    private PerfilBO perfilBO;

    @Override
    public BaseDAO getDAO() {
        return usuarioDAO;
    }

    /**
     * nao pode-se repetir o email , cpf e login
     *
     * @return
     */
    @Override
    public List<UniqueField> getUniqueFields() {
        return new UniqueFields()
                .add("userLogin");
    }

    public void enviarSenhaCadastro(Usuario usuario) throws BusinessException {
        solicitacaoRecuperacaoSenhaBO.save(usuario.getEmailPrincipal(), TipoRecuperacaoSenha.NOVO_USUARIO);
        usuarioDAO.merge(usuario);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public void save(Usuario usuario) throws BusinessException {
        boolean novo = usuario.getId() == null;

        if (novo) {
            try {
                //setar senha aleatoria para nao deixar campo em branco
                usuario.setUserPassword(Encryption.getSHA256(RandomStringUtils.random(10)));
            } catch (NoSuchAlgorithmException ex) {
                throw new RuntimeException(ex);
            }
        }
        //super usuario pode remover o perfil mesmo sem te-lo
        if (!SessaoUtils.getUser().isSuperUsuario()) {
            /*
             caso nao venha o perfil marcado e esse o usuario que estiver cadastrando nao possuir esse perfil, ele deve ser adicionado, 
             pois nesse caso o usuario logado nao tem acesso a remover o perfil q ele nao tem acesso
             
            List<Perfil> perfisUsuarioLogado = SessaoUtils.getUser().getPerfis();
            List<Perfil> perfisNovosCadastro = usuario.getPerfis();
            if (usuario.getId() != null) {
                List<Perfil> perfisAtuaisUsuario = perfilDAO.getPerfis(usuario);
                for (Perfil perfil : perfisAtuaisUsuario) {
                    //se nao conter, mas estiver removendo
                    if (!perfisNovosCadastro.contains(perfil) && !perfisUsuarioLogado.contains(perfil) && perfisAtuaisUsuario.contains(perfil)) {
                        perfisNovosCadastro.add(perfil);
                    }
                }
            }**/
        }

        Empresa empresa = SessaoUtils.getEmpresa();
        EmpresaLicenciada empresaLicenciada = SessaoUtils.getEmpresaLicenciada();
//        usuario.setEmpresasLicenciadas((List<EmpresaLicenciada>) getDAO().getInitialized(usuario.getEmpresasLicenciadas()));
//        if (usuario.getEmpresasLicenciadas() == null) {
//            usuario.setEmpresasLicenciadas(new LinkedList());
//        }
        usuario.setEmails(usuarioEmailDAO.getInitialized(usuario.getEmails()));
        if (usuario.getEmails() == null) {
            usuario.setEmails(new LinkedList());
        }

        if (!SessaoUtils.isSuperUsuario()) {
            if (usuario.getEmpresasFinanceiro() != null) {
                usuario.setEmpresasFinanceiro((List<Empresa>) getDAO().getInitialized(usuario.getEmpresasFinanceiro()));
                if (usuario.getEmpresasFinanceiro() == null || !usuario.getEmpresasFinanceiro().contains(empresa)) {
                    if (usuario.getEmpresasFinanceiro() == null) {
                        usuario.setEmpresasFinanceiro(new LinkedList());
                    }
                    usuario.getEmpresasFinanceiro().add(empresa);
                }
            }
//            if (empresaLicenciada != null && !usuario.getEmpresasLicenciadas().contains(empresaLicenciada)) {
//                usuario.getEmpresasLicenciadas().add(empresaLicenciada);
//            }
        }

        //salvar usuario
        super.save(usuario);

        String emailPrincipal = usuario.getEmailPrincipal();
        if (emailPrincipal != null && !emailPrincipal.isEmpty()) {
            atualizaListaEmailUsuario(usuario, emailPrincipal);
        }
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    private void atualizaListaEmailUsuario(Usuario usuario, String emailPrincipal) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ").append(UsuarioEmail.class.getName()).append(" ue ");
        sb.append("WHERE ue.usuario = :usuario");
        getDAO().getEntityManager().createQuery(sb.toString()).setParameter("usuario", usuario).executeUpdate();
        
        for (UsuarioEmail ue : usuario.getEmails()) {
            ue.setId(null);
            ue.setUsuario(usuario);
            if (ue.getEmail().equals(emailPrincipal)) {
                ue.setPrincipal(true);
            } else {
                ue.setPrincipal(false);
            }
            usuarioEmailDAO.saveOrMerge(ue);
        }
    }

    public static String getSenhaAleatoria() {
        return RandomStringUtils.randomAlphanumeric(TAMANHO_SENHA_ALEATORIA);
    }

    @Override
    public void validate(Usuario usuario) throws BusinessException {
    }

    @Override
    public boolean isAudit() {
        return true;
    }

    /**
     * retorn o usuario a partir do cpf
     *
     * @param cpf
     * @return
     */
    public Usuario getUsuario(String cpf) {
        return usuarioDAO.unique("cpf", cpf);
    }

    public Usuario getInicialized(Usuario usuario) {
        usuario.setEmpresa((Empresa) getDAO().getInitialized(usuario.getEmpresa()));
        return usuario;
    }

    public Empresa getEmpresa(Usuario user) {
        Empresa empresa = null;
        if (user != null) {
            empresa = (Empresa) getDAO().getInitialized(user.getEmpresa());
            if (empresa == null) {
                List<Empresa> empresas = (List<Empresa>) getDAO().getInitialized(user.getEmpresasFinanceiro());
                if (empresas != null && !empresas.isEmpty()) {
                    empresa = (Empresa) getDAO().getInitialized(empresas.get(0));
                }
            }
        }
        return empresa;
    }

    public List<Perfil> getListPerfis(Usuario usuario, String nomeAplicacao) {
        Restrictions rest = new Restrictions();
        rest.memberOf(usuario, "usuarios");
        rest.or();
        rest.add("empresaLicenciada", SessaoUtils.getEmpresaLicenciada());
        if (SessaoUtils.isSuperUsuario()) {
            rest.or();
            rest.isNull("empresaLicenciada");
        }
        return perfilBO.getDAO().list(rest, "descricao");
    }

    public List<PerfilFinanceiro> getListPerfisFinanceiro(Usuario usuario) {
        Restrictions rest = new Restrictions();
        if (!SessaoUtils.isSuperUsuario()) {
            rest.memberOf(usuario, "usuarios");
        }
        return getDAO().getQueryBuilder().from(PerfilFinanceiro.class).add(rest).orderBy("descricao").getResultList(PerfilFinanceiro.class);
    }

}
