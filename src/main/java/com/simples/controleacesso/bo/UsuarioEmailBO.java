/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.controleacesso.bo;

import com.simples.controleacesso.dao.UsuarioEmailDAO;
import com.simples.controleacesso.modelo.UsuarioEmail;
import com.xpert.core.crud.AbstractBusinessObject;
import com.xpert.core.exception.BusinessException;
import com.xpert.core.validation.UniqueField;
import com.xpert.core.validation.UniqueFields;
import com.xpert.persistence.dao.BaseDAO;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Rafael
 */
@Stateless
public class UsuarioEmailBO extends AbstractBusinessObject<UsuarioEmail>{

    @EJB
    private UsuarioEmailDAO usuarioEmailDAO;
    
    @Override
    public BaseDAO getDAO() {
        return usuarioEmailDAO;
    }

    @Override
    public List<UniqueField> getUniqueFields() {
        return new UniqueFields().add("email");
    }

    @Override
    public boolean isAudit() {
       return true;
    }

    @Override
    public void validate(UsuarioEmail object) throws BusinessException {
    }

    public UsuarioEmail getUsuarioEmail(String email) {
        return (UsuarioEmail) getDAO().unique("email", email);
    }
}
