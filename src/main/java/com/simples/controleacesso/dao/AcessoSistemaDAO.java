package com.simples.controleacesso.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.controleacesso.modelo.AcessoSistema;
import javax.ejb.Local;

/**
 *
 * @author ayslan
 */
@Local
public interface AcessoSistemaDAO extends BaseDAO<AcessoSistema> {
    
}
