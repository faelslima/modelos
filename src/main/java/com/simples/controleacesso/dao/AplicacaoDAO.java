package com.simples.controleacesso.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.controleacesso.modelo.Aplicacao;
import javax.ejb.Local;

/**
 *
 * @author ayslan
 */
@Local
public interface AplicacaoDAO extends BaseDAO<Aplicacao> {
}
