package com.simples.controleacesso.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.controleacesso.modelo.EmpresaLicenciada;
import javax.ejb.Local;

/**
 *
 * @author ayslan
 */
@Local
public interface EmpresaLicenciadaDAO extends BaseDAO<EmpresaLicenciada> {
}
