package com.simples.controleacesso.dao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.controleacesso.modelo.HistoricoSituacaoUsuario;
import javax.ejb.Local;

/**
 *
 * @author ayslan
 */
@Local
public interface HistoricoSituacaoUsuarioDAO extends BaseDAO<HistoricoSituacaoUsuario> {
    
}
