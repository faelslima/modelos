package com.simples.controleacesso.dao;

import com.simples.controleacesso.modelo.Perfil;
import com.simples.controleacesso.modelo.PerfilPermissao;
import com.simples.controleacesso.modelo.Usuario;
import com.xpert.persistence.dao.BaseDAO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ayslan
 */
@Local
public interface PerfilPermissaoDAO extends BaseDAO<PerfilPermissao> {

    /**
     * retorn os perfis do usuario
     *
     * @param usuario
     * @return
     */
    public List<Perfil> getPerfis(Usuario usuario);
}
