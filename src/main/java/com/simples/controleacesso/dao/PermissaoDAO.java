package com.simples.controleacesso.dao;

import com.simples.controleacesso.modelo.Perfil;
import com.simples.controleacesso.modelo.Permissao;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.controleacesso.PerfilFinanceiro;
import com.xpert.persistence.dao.BaseDAO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Ayslan
 */
@Local
public interface PermissaoDAO extends BaseDAO<Permissao> {

    /**
     * retorna todas as permissoes
     *
     * @return
     */
    public List<Permissao> getPermissoes();
    public List<Permissao> getPermissoesFinanceiro();
    public List<Permissao> getPermissoesPatrimonio();
    public List<Permissao> getPermissoesProtocolo();

    /**
     * retorna todas as permissoes
     *
     * @param apenasAtivas Indica se sao apenas as ativas
     * @return
     */
    public List<Permissao> getPermissoes(boolean apenasAtivas);
    public List<Permissao> getPermissoesFinanceiro(boolean apenasAtivas);
    public List<Permissao> getPermissoesPatrimonio(boolean apenasAtivas);
    public List<Permissao> getPermissoesProtocolo(boolean apenasAtivas);

    /**
     * retorna as permissoes desse perfil
     *
     * @param perfil
     * @return
     */
    public List<Permissao> getPermissoes(Perfil perfil);
    public List<Permissao> getPermissoesFinanceiro(Perfil perfil);
    public List<Permissao> getPermissoesPatrimonio(Perfil perfil);
    public List<Permissao> getPermissoesProtocolo(Perfil perfil);

    /**
     * retorna as permissoes desse usuario
     *
     * @param usuario
     * @return
     */
    public List<Permissao> getPermissoes(Usuario usuario);
    public List<Permissao> getPermissoesFinanceiro(Usuario usuario);
    public List<Permissao> getPermissoesPatrimonio(Usuario usuario);
    public List<Permissao> getPermissoesProtocolo(Usuario usuario);

    /**
     * retorna as permissoes menu desse perfil
     *
     * @param perfil
     * @return
     */
    public List<Permissao> getPermissoesMenu(Perfil perfil);

    /**
     * retorn as permissoes de atalaho desse usuario
     *
     * @param usuario
     * @return
     */
    public List<Permissao> getPermissoesAtalhos(Usuario usuario);

    /**
     * retorn as permissoes desse usuario
     *
     * @param usuario
     * @param apenasAtivas Indica se sao apenas as ativas
     * @return
     */
    public List<Permissao> getPermissoes(Usuario usuario, boolean apenasAtivas);
    public List<Permissao> getPermissoesFinanceiro(Usuario usuario, boolean apenasAtivas);
    public List<Permissao> getPermissoesPatrimonio(Usuario usuario, boolean apenasAtivas);
    public List<Permissao> getPermissoesProtocolo(Usuario usuario, boolean apenasAtivas);
    
    

    /**
     * retorna as permissoes desse perfil
     *
     * @param perfil
     * @return
     */
    public List<Permissao> getPermissoesFinanceiro(PerfilFinanceiro perfil);

    /**
     * retorna as permissoes menu desse perfil
     *
     * @param perfil
     * @return
     */
    public List<Permissao> getPermissoesFinanceiroMenu(PerfilFinanceiro perfil);

    /**
     * retorn as permissoes de atalaho desse usuario
     *
     * @param usuario
     * @return
     */
    public List<Permissao> getPermissoesFinanceiroAtalhos(Usuario usuario);

    public List<Permissao> getPermissoesAreaCliente();

}
