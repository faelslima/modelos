package com.simples.controleacesso.dao;

import com.simples.controleacesso.modelo.UsuarioCredenciamento;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Ayslan
 */
@Local
public interface UsuarioCredenciamentoDAO extends BaseDAO<UsuarioCredenciamento> {

}
