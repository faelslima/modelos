package com.simples.controleacesso.dao;

import com.simples.controleacesso.modelo.UsuarioCredenciamentoPerfil;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Ayslan
 */
@Local
public interface UsuarioCredenciamentoPerfilDAO extends BaseDAO<UsuarioCredenciamentoPerfil> {

}
