package com.simples.controleacesso.dao;

import com.simples.controleacesso.modelo.UsuarioEmail;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Ayslan
 */
@Local
public interface UsuarioEmailDAO extends BaseDAO<UsuarioEmail> {

}
