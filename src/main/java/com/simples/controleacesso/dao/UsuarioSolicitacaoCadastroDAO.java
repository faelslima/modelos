package com.simples.controleacesso.dao;

import com.simples.controleacesso.modelo.UsuarioSolicitacaoCadastro;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Ayslan
 */
@Local
public interface UsuarioSolicitacaoCadastroDAO extends BaseDAO<UsuarioSolicitacaoCadastro> {

}
