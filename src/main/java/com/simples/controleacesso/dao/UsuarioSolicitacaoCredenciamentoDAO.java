package com.simples.controleacesso.dao;

import com.simples.controleacesso.modelo.UsuarioSolicitacaoCredenciamento;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Ayslan
 */
@Local
public interface UsuarioSolicitacaoCredenciamentoDAO extends BaseDAO<UsuarioSolicitacaoCredenciamento> {

}
