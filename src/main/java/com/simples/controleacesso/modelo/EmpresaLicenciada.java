/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.controleacesso.modelo;

import com.google.gson.annotations.SerializedName;
import com.simples.comum.modelo.Banco;
import com.simples.comum.modelo.Cidade;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.util.Utils;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Rafael
 */
@Entity(name = "acesso.tb_empresa_licenciada")
@Table(schema = "acesso")
public class EmpresaLicenciada implements Serializable {

    @Id
    @Column(length = 36)
    @SerializedName(value = "Id")
    private String id;

    @Column(name = "razao_social")
    @SerializedName(value = "RazaoSocial")
    private String razaoSocial;

    @Column(length = 14)
    @SerializedName(value = "Cnpj")
    private String cnpj;

    private byte[] brasao;

    @Column(name = "brasao_content_type", length = 20)
    private String brasaoContentType;


    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getCnpjComMascara() {
        return Utils.formatarCpfCnpj(cnpj);
    }

    public byte[] getBrasao() {
        return brasao;
    }

    public String getBrasaoContentType() {
        return brasaoContentType;
    }

    public void setBrasaoContentType(String brasaoContentType) {
        this.brasaoContentType = brasaoContentType;
    }

    public void setBrasao(byte[] brasao) {
        this.brasao = brasao;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpresaLicenciada other = (EmpresaLicenciada) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return razaoSocial;
    }
    //</editor-fold>

}
