package com.simples.controleacesso.modelo;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

@Entity(name = "acesso.tb_perfil")
@Table(schema = "acesso", uniqueConstraints = @UniqueConstraint(columnNames = {"empresa_licenciada_id", "descricao"}))
public class Perfil implements Serializable {

    @Id
    @Column(length = 36)
    private String id;

    @NotBlank
    private String descricao;
    private Boolean ativo = true;
    
    @Size(max = 20)
    private String perfil;
    private Boolean administrativo = false;

//    @ManyToMany(targetEntity = Permissao.class, fetch = FetchType.LAZY)
//    @JoinTable(name = "tb_perfil_permissao", joinColumns = @JoinColumn(name = "perfil_id"), inverseJoinColumns = @JoinColumn(name = "permissao_id"), schema = "acesso")
//    @OrderBy(value = "descricao")
//    private List<Permissao> permissoes = new LinkedList<Permissao>();
//    
    @OneToMany(targetEntity = PerfilPermissao.class, fetch = FetchType.LAZY)
    private List<PerfilPermissao> perfilPermissoes;

    @ManyToMany(targetEntity = Permissao.class, fetch = FetchType.LAZY)
    @JoinTable(name = "tb_perfil_permissao_menu", joinColumns = @JoinColumn(name = "perfil_id"), inverseJoinColumns = @JoinColumn(name = "permissao_id"), schema = "acesso")
    @OrderBy(value = "descricao")
    private List<Permissao> permissoesAtalho = new LinkedList<Permissao>();
    
//    @OrderBy("nome")
//    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "perfis")
//    private List<Usuario> usuarios;
    
    @ManyToOne(fetch = FetchType.LAZY) 
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada_id"))
    private EmpresaLicenciada empresaLicenciada;
    
    public Perfil() {
    }

//    public List<Usuario> getUsuarios() {
//        return usuarios;
//    }
//
//    public void setUsuarios(List<Usuario> usuarios) {
//        this.usuarios = usuarios;
//    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public Boolean getAdministrativo() {
        return administrativo;
    }

    public void setAdministrativo(Boolean administrativo) {
        this.administrativo = administrativo;
    }

    public void setDescricao(String descricao) {
        if (descricao != null) {
            descricao = descricao.trim().toUpperCase();
        }
        this.descricao = descricao;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public List<Permissao> getPermissoes() {
//        return permissoes;
//    }
//
//    public void setPermissoes(List<Permissao> permissoes) {
//        this.permissoes = permissoes;
//    }

    public List<PerfilPermissao> getPerfilPermissoes() {
        return perfilPermissoes;
    }

    public void setPerfilPermissoes(List<PerfilPermissao> perfilPermissoes) {
        this.perfilPermissoes = perfilPermissoes;
    }

    public List<Permissao> getPermissoesAtalho() {
        return permissoesAtalho;
    }

    public void setPermissoesAtalho(List<Permissao> permissoesAtalho) {
        this.permissoesAtalho = permissoesAtalho;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    @Override
    public String toString() {
        return descricao;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Perfil other = (Perfil) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
}
