/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.controleacesso.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */
@Entity(name = "acesso.tb_perfil_permissao")
@Table(schema = "acesso")
public class PerfilPermissao implements Serializable {

    @Id
    @Column(length = 36)
    private String id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "perfil_id", foreignKey = @ForeignKey(name = "fk_perfil_id"))
    private Perfil perfil;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "permissao_id", foreignKey = @ForeignKey(name = "fk_pemissao_id"))
    private Permissao permissao;
    
    @Column(name = "permissao")
    private String chave;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public void setPermissao(Permissao permissao) {
        this.permissao = permissao;
    }

    public String getChave() {
        return chave;
    }

    public void setChave(String chave) {
        this.chave = chave;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PerfilPermissao other = (PerfilPermissao) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.chave, other.chave)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return chave;
    }
}
