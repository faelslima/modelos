package com.simples.controleacesso.modelo;

import com.google.gson.annotations.SerializedName;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.financeiro.modelo.controleacesso.PerfilFinanceiro;
import com.xpert.audit.NotAudited;
import com.xpert.security.model.User;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

@Entity(name = "acesso.tb_usuario")
@Table(schema = "acesso", uniqueConstraints = @UniqueConstraint(columnNames = "usuario", name = "uk_usuario"))
@Inheritance(strategy = InheritanceType.JOINED)
public class Usuario implements Serializable, User {

    @Id
    @Column(length = 36)
    private String id;

    @NotBlank
    @SerializedName(value = "Nome")
    private String nome;

    @Size(max = 20)
    @Column(name = "usuario")
    @SerializedName(value = "Usuario")
    private String userLogin;

    @NotAudited
    @Size(min = 5)
    @Column(name = "senha")
    @SerializedName(value = "Senha")
    private String userPassword;
    
    @Column(name = "suporte")
    @SerializedName(value = "Suporte")
    private boolean superUsuario;

    @Column(name = "pode_alterar_senha")
    @SerializedName(value = "PodeAlterarSenha")
    private Boolean podeAlterarSenha;

    @Column(name = "senha_descartavel")
    @SerializedName(value = "SenhaDescartavel")
    private Boolean senhaDescartavel;

    private boolean ativo;

//    @OrderBy(value = "descricao")
//    @ManyToMany(targetEntity = Perfil.class, fetch = FetchType.LAZY)
//    @JoinTable(name = "tb_usuario_perfil", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "perfil_id"), schema = "acesso")
//    private List<Perfil> perfis;
//
//    @OrderBy(value = "razaoSocial")
//    @ManyToMany(fetch = FetchType.LAZY, targetEntity = EmpresaLicenciada.class)
//    @JoinTable(name = "tb_usuario_empresa_licenciada", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "empresa_licenciada_id"), schema = "acesso")
//    private List<EmpresaLicenciada> empresasLicenciadas;

    @OrderBy(value = "descricao")
    @ManyToMany(targetEntity = PerfilFinanceiro.class, fetch = FetchType.LAZY)
    @JoinTable(name = "tb_usuario_perfil", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "perfil_id"), schema = "financeiro")
    private List<PerfilFinanceiro> perfisFinanceiro;

    @ManyToMany(targetEntity = Empresa.class, fetch = FetchType.LAZY)
    @JoinTable(name = "tb_usuario_empresa", joinColumns = @JoinColumn(name = "usuario_id"), inverseJoinColumns = @JoinColumn(name = "empresa_id"), schema = "financeiro")
    private List<Empresa> empresasFinanceiro;

    @OrderBy("email asc, principal desc")
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "usuario")
    private List<UsuarioEmail> emails;

    @Transient
    private Empresa empresa;

    @Transient
    private EmpresaLicenciada empresaLicenciada;

    @Transient
    @Email
    private String emailPrincipal;
    
    public Usuario() {
    }

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    @Override
    public boolean isActive() {
        return ativo;
    }

    public boolean isAtivo() {
        return ativo;
    }
    
    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public List<UsuarioEmail> getEmails() {
        return emails;
    }

    public void setEmails(List<UsuarioEmail> emails) {
        this.emails = emails;
    }

    public Boolean getPodeAlterarSenha() {
        return podeAlterarSenha;
    }

    public void setPodeAlterarSenha(Boolean podeAlterarSenha) {
        this.podeAlterarSenha = podeAlterarSenha;
    }

    public String getEmailPrincipal() {
        return emailPrincipal;
    }

    public void setEmailPrincipal(String emailPrincipal) {
        this.emailPrincipal = emailPrincipal;
    }

    public Boolean getSenhaDescartavel() {
        return senhaDescartavel;
    }

    public void setSenhaDescartavel(Boolean senhaDescartavel) {
        this.senhaDescartavel = senhaDescartavel;
    }

    public boolean isSuperUsuario() {
        return superUsuario;
    }

    public void setSuperUsuario(boolean superUsuario) {
        this.superUsuario = superUsuario;
    }

    public List<PerfilFinanceiro> getPerfisFinanceiro() {
        return perfisFinanceiro;
    }

    public void setPerfisFinanceiro(List<PerfilFinanceiro> perfisFinanceiro) {
        this.perfisFinanceiro = perfisFinanceiro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if (nome != null) {
            nome = nome.trim().toUpperCase();
        }
        this.nome = nome;
    }

    public List<Empresa> getEmpresasFinanceiro() {
        return empresasFinanceiro;
    }

    public void setEmpresasFinanceiro(List<Empresa> empresasFinanceiro) {
        this.empresasFinanceiro = empresasFinanceiro;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

//    public List<Perfil> getPerfis() {
//        return perfis;
//    }
//
//    public void setPerfis(List<Perfil> perfis) {
//        this.perfis = perfis;
//    }
//
//    public List<EmpresaLicenciada> getEmpresasLicenciadas() {
//        return empresasLicenciadas;
//    }
//
//    public void setEmpresasLicenciadas(List<EmpresaLicenciada> empresasLicenciadas) {
//        this.empresasLicenciadas = empresasLicenciadas;
//    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    @Override
    public String getUserLogin() {
        return userLogin;
    }

    @Override
    public void setUserLogin(String userLogin) {
        if (userLogin != null) {
            userLogin = userLogin.trim().toUpperCase();
        }
        this.userLogin = userLogin;
    }

    @Override
    public String getUserPassword() {
        return userPassword;
    }

    @Override
    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Override
    public void setActive(boolean active) {
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return getNome();
    }
    //</editor-fold>
}
