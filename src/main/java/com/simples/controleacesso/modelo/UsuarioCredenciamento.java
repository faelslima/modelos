/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.controleacesso.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael Lima
 */
@Entity(name = "acesso.tb_usuario_credenciamento")
@Table(schema = "acesso")
public class UsuarioCredenciamento implements Serializable {

    @Id
    @Size(max = 36)
    private String id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada_id"))
    private EmpresaLicenciada empresaLicenciada;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_credenciamento")
    private Date dataHoraCredenciamento;

    @NotNull
    private Boolean ativo;

    @NotNull
    private Boolean administrador;

    @ManyToOne
    @JoinColumn(name = "solicitacao_credenciamento_id", foreignKey = @ForeignKey(name = "fk_solicitacao_credenciamento_id"))
    private UsuarioSolicitacaoCredenciamento solicitacaoCredenciamento;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "credenciamento")
    private List<UsuarioCredenciamentoPerfil> perfis;
    
    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public Usuario getUser() {
        return user;
    }
    
    public void setUser(Usuario user) {
        this.user = user;
    }
    
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }
    
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }
    
    public Date getDataHoraCredenciamento() {
        return dataHoraCredenciamento;
    }
    
    public void setDataHoraCredenciamento(Date dataHoraCredenciamento) {
        this.dataHoraCredenciamento = dataHoraCredenciamento;
    }
    
    public Boolean getAtivo() {
        return ativo;
    }
    
    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }
    
    public Boolean getAdministrador() {
        return administrador;
    }
    
    public void setAdministrador(Boolean administrador) {
        this.administrador = administrador;
    }

    public List<UsuarioCredenciamentoPerfil> getPerfis() {
        return perfis;
    }

    public void setPerfis(List<UsuarioCredenciamentoPerfil> perfis) {
        this.perfis = perfis;
    }
    
    public UsuarioSolicitacaoCredenciamento getSolicitacaoCredenciamento() {
        return solicitacaoCredenciamento;
    }
    
    public void setSolicitacaoCredenciamento(UsuarioSolicitacaoCredenciamento solicitacaoCredenciamento) {
        this.solicitacaoCredenciamento = solicitacaoCredenciamento;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioCredenciamento other = (UsuarioCredenciamento) obj;
        return Objects.equals(this.id, other.id);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return user != null ? user.toString() : "";
    }
    //</editor-fold>
}
