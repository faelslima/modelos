/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.controleacesso.modelo;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael Lima
 */
@Entity(name = "acesso.tb_usuario_credenciamento_perfil")
@Table(schema = "acesso")
public class UsuarioCredenciamentoPerfil implements Serializable {

    @Id
    @Size(max = 36)
    private String id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "perfil_id", foreignKey = @ForeignKey(name = "fk_perfil_id"))
    private Perfil perfil;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "credenciamento_id", foreignKey = @ForeignKey(name = "fk_credenciamento_id"))
    private UsuarioCredenciamento credenciamento;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public Perfil getPerfil() {
        return perfil;
    }
    
    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
    
    public UsuarioCredenciamento getCredenciamento() {
        return credenciamento;
    }
    
    public void setCredenciamento(UsuarioCredenciamento credenciamento) {
        this.credenciamento = credenciamento;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioCredenciamentoPerfil other = (UsuarioCredenciamentoPerfil) obj;
        return Objects.equals(this.id, other.id);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return perfil != null ? perfil.toString() : "";
    }
    //</editor-fold>
}
