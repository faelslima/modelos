/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.controleacesso.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * @author Rafael Lima
 */
@Entity(name = "acesso.tb_usuario_solicitacao_credenciamento")
@Table(schema = "acesso")
public class UsuarioSolicitacaoCredenciamento implements Serializable {

    @Id
    @Size(max = 36)
    @SerializedName(value = "Id")
    private String id;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora")
    @SerializedName(value = "DataHora")
    private Date dataHora;

    @NotBlank
    @SerializedName(value = "Nome")
    private String nome;

    @NotBlank
    @Email
    @Size(max = 100)
    @SerializedName(value = "Email")
    private String email;

    @NotBlank
    @Size(max = 100)
    @SerializedName(value = "Usuario")
    private String usuario;
    
    @NotNull
    @SerializedName(value = "Confirmado")
    private Boolean confirmado;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_confirmacao")
    @SerializedName(value = "DataHoraConfirmacao")
    private Date dataHoraConfirmacao;
    
    @SerializedName(value = "Autorizado")
    private Boolean autorizado;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_autorizacao")
    @SerializedName(value = "DataHoraAutorizacao")
    private Date dataHoraAutorizacao;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada_id"))
    @SerializedName(value = "EmpresaLicenciada")
    private EmpresaLicenciada empresaLicenciada;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    @SerializedName(value = "UsuarioCadastro")
    private Usuario user;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public Date getDataHora() {
        return dataHora;
    }
    
    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }
    
    public String getNome() {
        return nome;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getUsuario() {
        return usuario;
    }
    
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }
    
    public Boolean getConfirmado() {
        return confirmado;
    }
    
    public void setConfirmado(Boolean confirmado) {
        this.confirmado = confirmado;
    }
    
    public Date getDataHoraConfirmacao() {
        return dataHoraConfirmacao;
    }
    
    public void setDataHoraConfirmacao(Date dataHoraConfirmacao) {
        this.dataHoraConfirmacao = dataHoraConfirmacao;
    }
    
    public Boolean getAutorizado() {
        return autorizado;
    }
    
    public void setAutorizado(Boolean autorizado) {
        this.autorizado = autorizado;
    }
    
    public Date getDataHoraAutorizacao() {
        return dataHoraAutorizacao;
    }
    
    public void setDataHoraAutorizacao(Date dataHoraAutorizacao) {
        this.dataHoraAutorizacao = dataHoraAutorizacao;
    }
    
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }
    
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }
    
    public Usuario getUser() {
        return user;
    }
    
    public void setUser(Usuario user) {
        this.user = user;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UsuarioSolicitacaoCredenciamento other = (UsuarioSolicitacaoCredenciamento) obj;
        return Objects.equals(this.id, other.id);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return nome;
    }
    //</editor-fold>
    
     //<editor-fold defaultstate="collapsed" desc="Gson">
    private static Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new DateSerializer());
        builder.registerTypeAdapter(Date.class, new DateDeserializer());
        return builder.create();
    }
    
    public static String toJson(UsuarioSolicitacaoCadastro solicitacaoCredenciamento) {
        return getGson().toJson(solicitacaoCredenciamento);
    }
    
    public static UsuarioSolicitacaoCadastro fromJson(String json) {
        return getGson().fromJson(json, UsuarioSolicitacaoCadastro.class);
    }
    
    /**
     * Converte uma Date em um valor ISO 8601.
     *
     * @param data Date a ser convertido em String.
     * @return String com a data. Ex: "2013-01-17T00:00:00.124-02:00"
     */
    public static String dateToJson(Date data) {
        if (data == null) {
            return null;
        }
        // O toString do Joda DateTime por padrão retorna um String de data em ISO 8601.
        DateTime dt = new DateTime(data, DateTimeZone.forID("-03:00"));
        return dt.toString().replace("-03:00", "Z");
    }
    
    /**
     * Coverte uma data ISO 8601 em DateTime.
     *
     * @param strDt
     * @return Date Ex: "2013-01-17T00:00:00.124-02:00"
     */
    public static Date jsonToDate(String strDt) {
        if (strDt == null) {
            return null;
        }
        DateTime dt = new DateTime(strDt);
        return dt.toDate();
    }
    
    /**
     * Serializer de Date.
     */
    private static class DateSerializer implements JsonSerializer<Date> {
        @Override
        public JsonElement serialize(Date t, Type type, JsonSerializationContext jsc) {
            return new JsonPrimitive(dateToJson(t));
        }
    }
    
    /**
     * Deserializer de Date.
     */
    private static class DateDeserializer implements JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return jsonToDate(json.getAsJsonPrimitive().getAsString());
        }
    }
    //</editor-fold>
}
