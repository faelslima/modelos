package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.ContaCorrente;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ContaCorrenteDAO extends BaseDAO<ContaCorrente> {
    
}
