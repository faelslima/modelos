package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.ContaCorrenteTipoLancamento;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ContaCorrenteTipoLancamentoDAO extends BaseDAO<ContaCorrenteTipoLancamento> {
    public void gerarTipoLancamentos();
}
