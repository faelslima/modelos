package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.Contrato;
import com.simples.financeiro.modelo.cadastro.UsuarioCliente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ContratoDAO extends BaseDAO<Contrato> {
    
    public List<Contrato> getContratos(UsuarioCliente usuarioCliente);
}
