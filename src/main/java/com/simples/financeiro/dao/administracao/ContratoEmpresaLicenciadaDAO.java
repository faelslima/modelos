package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.ContratoEmpresaLicenciada;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ContratoEmpresaLicenciadaDAO extends BaseDAO<ContratoEmpresaLicenciada> {
    
}
