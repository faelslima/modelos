package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.financeiro.modelo.cadastro.Cliente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Rafael
 */
@Local
public interface ContratoMensalidadeDAO extends BaseDAO<ContratoMensalidade> {

    public List<ContratoMensalidade> getMensalidades(Cliente cliente);

}
