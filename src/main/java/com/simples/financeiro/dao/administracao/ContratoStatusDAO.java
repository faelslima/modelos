package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.ContratoStatus;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ContratoStatusDAO extends BaseDAO<ContratoStatus> {
    
    public void gerarStatus();
}
