package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.Empresa;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface EmpresaDAO extends BaseDAO<Empresa> {
    
}
