package com.simples.financeiro.dao.administracao;

import com.simples.financeiro.modelo.administracao.Contrato;
import com.simples.financeiro.modelo.administracao.Licenca;
import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.financeiro.modelo.cadastro.UsuarioCliente;
import com.xpert.persistence.dao.BaseDAO;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface LicencaDAO extends BaseDAO<Licenca> {

    public String getStringArquivoLicenca(String cnpjEmpresaLicenciada);
    
    public String getStringArquivoLicencaSemCriptografia(String cnpjEmpresaLicenciada);
    
    public String getStringArquivoLicenca(UsuarioCliente usuarioCliente);
    
    public String getStringArquivoLicencaSemCriptografia(UsuarioCliente usuarioCliente);

    public List<Licenca> getLicencasPorClienteEOrdenadoPorSistemaEPeriodoLicenciado(Cliente cliente);
    
    public List<Licenca> getLicencasPorClienteEOrdenadoPorSistemaEPeriodoLicenciado(UsuarioCliente usuarioCliente);
    
    public List<Licenca> getLicencasPorContratoEOrdenadoPorSistemaEPeriodoLicenciado(Contrato contrato);
    
    public List<Licenca> getLicencasOrdenadasPorContrato(UsuarioCliente contrato);
    
    public String getStringArquivoLicenca(Contrato contrato);
    
    public String getStringArquivoLicencaSemCriptografia(Contrato contrato);
}
