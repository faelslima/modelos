package com.simples.financeiro.dao.administracao;

import com.simples.financeiro.modelo.administracao.LicencaTemporaria;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface LicencaTemporariaDAO extends BaseDAO<LicencaTemporaria> {

    public void inativarLicencasVencidas();
}
