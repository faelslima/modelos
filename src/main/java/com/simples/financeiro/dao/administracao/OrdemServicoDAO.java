package com.simples.financeiro.dao.administracao;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.OrdemServico;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface OrdemServicoDAO extends BaseDAO<OrdemServico> {
    
}
