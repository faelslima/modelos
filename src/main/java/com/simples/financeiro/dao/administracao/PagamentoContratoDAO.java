package com.simples.financeiro.dao.administracao;

import com.simples.financeiro.modelo.administracao.Contrato;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.administracao.PagamentoContrato;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface PagamentoContratoDAO extends BaseDAO<PagamentoContrato> {

    public List<Contrato> getContratoPorNomeClienteContratante(String nome, Empresa empresaContratada);

    public List<Contrato> getContratoPorNomeClienteContratanteOuNumeroContrato(String consulta, Empresa empresaContratada);

}
