package com.simples.financeiro.dao.cadastro;

import com.simples.financeiro.modelo.cadastro.ClienteContato;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ClienteContatoDAO extends BaseDAO<ClienteContato> {
    
}
