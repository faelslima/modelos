package com.simples.financeiro.dao.cadastro;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.cadastro.Cliente;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ClienteDAO extends BaseDAO<Cliente> {
    
}
