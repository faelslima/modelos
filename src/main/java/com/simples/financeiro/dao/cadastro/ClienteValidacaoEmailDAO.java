package com.simples.financeiro.dao.cadastro;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.cadastro.ClienteValidacaoEmail;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface ClienteValidacaoEmailDAO extends BaseDAO<ClienteValidacaoEmail> {
    
}
