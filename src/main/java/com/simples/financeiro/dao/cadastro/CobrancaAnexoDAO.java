package com.simples.financeiro.dao.cadastro;

import com.simples.financeiro.modelo.cadastro.CobrancaAnexo;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface CobrancaAnexoDAO extends BaseDAO<CobrancaAnexo> {
    
}
