package com.simples.financeiro.dao.cadastro;

import com.simples.financeiro.modelo.cadastro.Cliente;
import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.cadastro.UsuarioCliente;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface UsuarioClienteDAO extends BaseDAO<UsuarioCliente> {

    public List<Cliente> getListaClientes(String email);
}
