package com.simples.financeiro.dao.notafiscal;

import com.simples.financeiro.modelo.notafiscal.HistoricoVinculacaoNotaFiscal;
import com.xpert.persistence.dao.BaseDAO;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface HistoricoVinculacaoNotaFiscalDAO extends BaseDAO<HistoricoVinculacaoNotaFiscal> {
}
