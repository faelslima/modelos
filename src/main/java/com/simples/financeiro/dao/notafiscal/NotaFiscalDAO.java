package com.simples.financeiro.dao.notafiscal;

import com.simples.financeiro.modelo.cadastro.Cliente;
import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.notafiscal.NotaFiscal;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface NotaFiscalDAO extends BaseDAO<NotaFiscal> {

    public List<NotaFiscal> getNotasFiscaisPorTomadorValorNotaENumeroNota(Cliente tomador, BigDecimal valorNotaFiscal, String numero);
    
    public List<NotaFiscal> getNotasFiscaisPorTomadorENumeroNota(Cliente tomador, String numero);

    public List<NotaFiscal> getNotasFiscaisComplementarPorTomadorENumeroNota(Cliente tomador, BigDecimal valorMensalidade, BigDecimal valorNotaFiscal, String numero);
   
    public List<NotaFiscal> getNotasFiscaisPorNumeroOuTomador(String pesquisa);

    public List<NotaFiscal> getNotasFiscaisPorNumero(String numeroNota);
    
    public void createStoreProcedureVincularNotaFiscalComMensalidade();

}
