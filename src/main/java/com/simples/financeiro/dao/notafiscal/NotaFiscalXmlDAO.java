package com.simples.financeiro.dao.notafiscal;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.notafiscal.NotaFiscalXml;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface NotaFiscalXmlDAO extends BaseDAO<NotaFiscalXml> {
    
}
