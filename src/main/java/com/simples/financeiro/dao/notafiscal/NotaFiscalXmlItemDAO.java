package com.simples.financeiro.dao.notafiscal;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.notafiscal.NotaFiscalXmlItem;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface NotaFiscalXmlItemDAO extends BaseDAO<NotaFiscalXmlItem> {
    
}
