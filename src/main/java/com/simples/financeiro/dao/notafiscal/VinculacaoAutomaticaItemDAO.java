package com.simples.financeiro.dao.notafiscal;

import com.xpert.persistence.dao.BaseDAO;
import com.simples.financeiro.modelo.notafiscal.VinculacaoAutomaticaItem;
import javax.ejb.Local;

/**
 *
 * @author Rafael Lima
 */
@Local
public interface VinculacaoAutomaticaItemDAO extends BaseDAO<VinculacaoAutomaticaItem> {
}
