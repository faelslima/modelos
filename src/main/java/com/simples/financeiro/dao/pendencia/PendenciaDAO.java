package com.simples.financeiro.dao.pendencia;

import com.simples.financeiro.modelo.administracao.Contrato;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.financeiro.modelo.administracao.OrdemServico;
import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.financeiro.modelo.notafiscal.NotaFiscalXml;
import com.simples.financeiro.modelo.pendencia.Pendencia;
import com.xpert.faces.primefaces.LazyDataModelImpl;
import java.math.BigDecimal;
import java.util.List;
import javax.ejb.Local;

/**
 * @author Rafael
 */
@Local
public interface PendenciaDAO  {

    public List<Pendencia> getPendencias();
    public List<Pendencia> getPendencias(Empresa empresa);
    public LazyDataModelImpl<NotaFiscalXml> getDataModelNotasFiscaisXmlInconsistentes(Empresa empresa);
    public LazyDataModelImpl<ContratoMensalidade> getDataModelMensalidadesSemNotaFiscal(Empresa empresa);
    public LazyDataModelImpl<ContratoMensalidade> getDataModelMensalidadesEmAtraso(Empresa empresa);
    public LazyDataModelImpl<ContratoMensalidade> getDataModelNotasVinculadasComValorDiferente(Empresa empresa);
    public LazyDataModelImpl<ContratoMensalidade> getDataModelNotasCanceladasVinculadas(Empresa empresa);
    public LazyDataModelImpl<Cliente> getDataModelClientesSemContrato(Empresa empresa);
    public LazyDataModelImpl<Contrato> getDataModelContratosComMaisDeUmPagamentoParcial(Empresa empresa);
    public LazyDataModelImpl<OrdemServico> getDataModelOrdemServicoSemNotaFiscal(Empresa empresa);
    public LazyDataModelImpl<OrdemServico> getDataModelOrdemServicoComPagamentoPendente(Empresa empresa);
    public BigDecimal getValorTotalEmAtraso(Empresa empresa);
}
