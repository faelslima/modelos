/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Rafael
 */
@Entity(name = "financeiro.tb_cc")
@Table(schema = "financeiro")
@Inheritance(strategy = InheritanceType.JOINED)
public class ContaCorrente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cc_tipo_lancamento_id", foreignKey = @ForeignKey(name = "fk_cc_tipo_lancamento"))
    private ContaCorrenteTipoLancamento tipoLancamento;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_lancamento")
    private Date dataLancamento;

    @NotNull
    @Column(name = "valor")
    private BigDecimal valor;

    private BigDecimal correcao = BigDecimal.ZERO;
    private BigDecimal juros = BigDecimal.ZERO;
    private BigDecimal multa = BigDecimal.ZERO;
    private BigDecimal desconto = BigDecimal.ZERO;

    @NotNull
    @Column(name = "valor_total")
    private BigDecimal valorTotal;

    @NotBlank
    @Column(columnDefinition = "text")
    private String historico;

    @Size(min = 8, max = 16)
    @Column(name = "tipo_documento")
    private String tipoDocumento;
    
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pagamento_id", foreignKey = @ForeignKey(name = "fk_pagamento_id"))
    private Pagamento pagamento;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cancelamento_id", foreignKey = @ForeignKey(name = "fk_conta_corrente_id"))
    private ContaCorrente cancelamento;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContaCorrenteTipoLancamento getTipoLancamento() {
        return tipoLancamento;
    }

    public void setTipoLancamento(ContaCorrenteTipoLancamento tipoLancamento) {
        this.tipoLancamento = tipoLancamento;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public BigDecimal getCorrecao() {
        return correcao;
    }

    public void setCorrecao(BigDecimal correcao) {
        this.correcao = correcao;
    }

    public BigDecimal getJuros() {
        return juros;
    }

    public void setJuros(BigDecimal juros) {
        this.juros = juros;
    }

    public BigDecimal getMulta() {
        return multa;
    }

    public void setMulta(BigDecimal multa) {
        this.multa = multa;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valor) {
        this.valorTotal = valor;
    }

    public void setValorTotal() {
        valorTotal = valor.add(correcao);
        valorTotal = valorTotal.add(juros);
        valorTotal = valorTotal.add(multa);
        valorTotal = valorTotal.subtract(desconto);
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Pagamento getPagamento() {
        return pagamento;
    }

    public void setPagamento(Pagamento pagamento) {
        this.pagamento = pagamento;
    }

    public ContaCorrente getCancelamento() {
        return cancelamento;
    }

    public void setCancelamento(ContaCorrente cancelamento) {
        this.cancelamento = cancelamento;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContaCorrente other = (ContaCorrente) obj;
        return !(this.id != other.id && (this.id == null || !this.id.equals(other.id)));
    }
    //</editor-fold>

}
