/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_cc_contrato")
@Table(schema = "financeiro")
@PrimaryKeyJoinColumn(name = "id", foreignKey = @ForeignKey(name = "fk_id_cc"))
public class ContaCorrenteContrato extends ContaCorrente implements Serializable {

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato_id", foreignKey = @ForeignKey(name = "fk_contrato_id"))
    private Contrato contrato;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mensalidade_id", foreignKey = @ForeignKey(name = "fk_mensalidade_id"))
    private ContratoMensalidade mensalidade;
    
    @Temporal(TemporalType.DATE)
    @Column(name = "data_vencimento")
    private Date dataVencimento;

    @Column(length = 6)
    private String competencia;
    
    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public ContratoMensalidade getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(ContratoMensalidade mensalidade) {
        this.mensalidade = mensalidade;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }
}
