/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_cc_ordem_servico")
@Table(schema = "financeiro")
@PrimaryKeyJoinColumn(name = "id", foreignKey = @ForeignKey(name = "fk_id_cc"))
public class ContaCorrenteOrdemServico extends ContaCorrente implements Serializable {

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ordem_servico_id", foreignKey = @ForeignKey(name = "fk_ordem_servico_id"))
    private OrdemServico ordemServico;

    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }

    @Override
    public Long getId() {
        return super.getId();
    }

    public void setContaCorrente(ContaCorrente contaCorrente) {
        if(contaCorrente != null) {
            setId(contaCorrente.getId());
        }
    }
}
