/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.cadastro.Cliente;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.annotations.Where;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_contrato")
@Table(schema = "financeiro", uniqueConstraints = @UniqueConstraint(name = "uk_numero_contrato", columnNames = {"numero_contrato", "ano_contrato", "empresa_contratada_id"}))
public class Contrato implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_inicio")
    private Date dataInicio;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_fim")
    private Date dataFim;

    @NotNull
    @Column(name = "numero_contrato")
    private Integer numeroContrato;

    @NotNull
    @Column(name = "ano_contrato")
    private Integer anoContrato;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_geracao")
    private Date dataGeracao;

    @Column(name = "tipo_contrato", length = 1)
    private String tipoContrato = "L";

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status_id", foreignKey = @ForeignKey(name = "fk_ti_status_id"))
    private ContratoStatus contratoStatus;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_contratante_id", foreignKey = @ForeignKey(name = "fk_cliente_id"))
    private Cliente clienteContratante;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_contratada_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresaContratada;

    @Column(name = "quantidade_parcelas")
    private Integer quantidadeParcelas;

    @Column(name = "dia_vencimento")
    private Integer diaVencimento = 30;

    @Column(name = "valor_total")
    private BigDecimal valorTotal;

    @Column(name = "valor_primeira_parcela")
    private BigDecimal valorPrimeiraParcela;

    @Column(name = "valor_demais_parcela")
    private BigDecimal valorDemaisParcelas;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario cadastradoPor;

    @OneToMany(mappedBy = "contrato", targetEntity = ContratoAnexo.class, fetch = FetchType.LAZY)
    private List<ContratoAnexo> anexos = new LinkedList();

    @OneToMany(mappedBy = "contrato", targetEntity = ContratoMensalidade.class, fetch = FetchType.LAZY)
    @Where(clause = "status = 'LAN'")
    @OrderBy(value = "competencia")
    private List<ContratoMensalidade> mensalidades = new LinkedList();

    @OneToMany(mappedBy = "contrato", fetch = FetchType.LAZY)
    @OrderBy(value = "empresaLicenciada")
    private List<ContratoEmpresaLicenciada> empresasLicenciadas = new LinkedList();

    @OneToMany(mappedBy = "contrato", targetEntity = PagamentoContrato.class, fetch = FetchType.LAZY)
    @OrderBy(value = "dataLancamento desc")
    private List<PagamentoContrato> pagamentos;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public Date getDataGeracao() {
        return dataGeracao;
    }

    public void setDataGeracao(Date dataGeracao) {
        this.dataGeracao = dataGeracao;
    }

    public String getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(String tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public ContratoStatus getContratoStatus() {
        return contratoStatus;
    }

    public void setContratoStatus(ContratoStatus contratoStatus) {
        this.contratoStatus = contratoStatus;
    }

    public Cliente getClienteContratante() {
        return clienteContratante;
    }

    public void setClienteContratante(Cliente clienteContratante) {
        this.clienteContratante = clienteContratante;
    }

    public Empresa getEmpresaContratada() {
        return empresaContratada;
    }

    public void setEmpresaContratada(Empresa empresaContratada) {
        this.empresaContratada = empresaContratada;
    }

    public Usuario getCadastradoPor() {
        return cadastradoPor;
    }

    public void setCadastradoPor(Usuario cadastradoPor) {
        this.cadastradoPor = cadastradoPor;
    }

    public Integer getQuantidadeParcelas() {
        return quantidadeParcelas;
    }

    public void setQuantidadeParcelas(Integer quantidadeParcelas) {
        this.quantidadeParcelas = quantidadeParcelas;
    }

    public Integer getDiaVencimento() {
        return diaVencimento;
    }

    public void setDiaVencimento(Integer diaVencimento) {
        this.diaVencimento = diaVencimento;
    }

    public Integer getNumeroContrato() {
        return numeroContrato;
    }

    public String getNumeroEAnoContrato() {
        if (numeroContrato != null && anoContrato != null) {
            return numeroContrato + "/" + anoContrato;
        }
        return "";
    }

    public void setNumeroContrato(Integer numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public Integer getAnoContrato() {
        return anoContrato;
    }

    public void setAnoContrato(Integer anoContrato) {
        this.anoContrato = anoContrato;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getValorPrimeiraParcela() {
        return valorPrimeiraParcela;
    }

    public void setValorPrimeiraParcela(BigDecimal valorPrimeiraParcela) {
        this.valorPrimeiraParcela = valorPrimeiraParcela;
    }

    public BigDecimal getValorDemaisParcelas() {
        return valorDemaisParcelas;
    }

    public void setValorDemaisParcelas(BigDecimal valorDemaisParcelas) {
        this.valorDemaisParcelas = valorDemaisParcelas;
    }

    public List<ContratoAnexo> getAnexos() {
        return anexos;
    }

    public void setAnexos(List<ContratoAnexo> anexos) {
        this.anexos = anexos;
    }

    public List<ContratoMensalidade> getMensalidades() {
        return mensalidades;
    }

    public void setMensalidades(List<ContratoMensalidade> mensalidades) {
        this.mensalidades = mensalidades;
    }

    public List<PagamentoContrato> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoContrato> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public List<ContratoEmpresaLicenciada> getEmpresasLicenciadas() {
        return empresasLicenciadas;
    }

    public void setEmpresasLicenciadas(List<ContratoEmpresaLicenciada> empresasLicenciadas) {
        this.empresasLicenciadas = empresasLicenciadas;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contrato other = (Contrato) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return getNumeroEAnoContrato();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="labelForAutoComplete()">
    public String labelForAutoComplete() {
        if (clienteContratante != null) {
            return "Contrato " + numeroContrato + "/" + anoContrato + " - " + clienteContratante;
        }
        return null;
    }
    //</editor-fold>

}
