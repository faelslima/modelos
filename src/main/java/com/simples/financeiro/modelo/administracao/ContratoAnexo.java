/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import com.simples.controleacesso.modelo.Usuario;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Rafael
 */
@Entity(name = "financeiro.tb_contrato_anexos")
@Table(schema = "financeiro")
public class ContratoAnexo implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato_id", foreignKey = @ForeignKey(name = "fk_contrato_id"))
    private Contrato contrato;
    
    private byte[] anexo;
    
    @Column(length = 5)
    private String extensao;
    
    @Column(length = 300)
    private String descricao;
    
    @Column(columnDefinition = "text")
    private String texto;
    
    @Column(name = "data_hora_inclusao")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dataHoraInclusao = new Date();
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario cadastradoPor;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Contrato getContrato() {
        return contrato;
    }
    
    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }
    
    public byte[] getAnexo() {
        return anexo;
    }
    
    public void setAnexo(byte[] anexo) {
        this.anexo = anexo;
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }
    
    public String getExtensao() {
        return extensao;
    }
    
    public void setExtensao(String extensao) {
        this.extensao = extensao;
    }

    public Date getDataHoraInclusao() {
        return dataHoraInclusao;
    }

    public void setDataHoraInclusao(Date dataHoraInclusao) {
        this.dataHoraInclusao = dataHoraInclusao;
    }

    public Usuario getCadastradoPor() {
        return cadastradoPor;
    }

    public void setCadastradoPor(Usuario cadastradoPor) {
        this.cadastradoPor = cadastradoPor;
    }
    
    public String getDescricao() {
        return descricao;
    }
    
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContratoAnexo other = (ContratoAnexo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
   //</editor-fold>
}
