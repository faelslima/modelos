/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.simples.financeiro.modelo.administracao;

import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.financeiro.modelo.cadastro.Produto;
import com.simples.financeiro.modelo.cadastro.Servico;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */

@Entity(name = "financeiro.tb_contrato_empresa_licenciada")
@Table(schema = "financeiro")
public class ContratoEmpresaLicenciada implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato_id", foreignKey = @ForeignKey(name = "fk_contrato_id"))
    private Contrato contrato;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_cliente_id"))
    private Cliente empresaLicenciada;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "produto_id", foreignKey = @ForeignKey(name = "fk_produto_id"))
    private Produto produto;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "servico_id", foreignKey = @ForeignKey(name = "fk_servico_id"))
    private Servico servico;
    
    @Column(name = "tipo", length = 1)
    private String tipo = "L";
    
    @NotNull
    @Column(name = "valor_mensal")
    private BigDecimal valorMensal;
    
    @OneToMany(mappedBy = "contratoEmpresaLicenciada", cascade = CascadeType.REMOVE, orphanRemoval = false)
    private List<Licenca> licencas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Cliente getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(Cliente empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Servico getServico() {
        return servico;
    }

    public void setServico(Servico servico) {
        this.servico = servico;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public BigDecimal getValorMensal() {
        return valorMensal;
    }

    public void setValorMensal(BigDecimal valorMensal) {
        this.valorMensal = valorMensal;
    }

    public List<Licenca> getLicencas() {
        return licencas;
    }

    public void setLicencas(List<Licenca> licencas) {
        this.licencas = licencas;
    }
}
