/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.notafiscal.HistoricoVinculacaoNotaFiscal;
import com.simples.financeiro.modelo.notafiscal.NotaFiscal;
import com.simples.util.Utils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;

/**
 *
 * @author Rafael
 */
@Entity(name = "financeiro.tb_contrato_mensalidade")
@Table(schema = "financeiro")
public class ContratoMensalidade implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato_id", foreignKey = @ForeignKey(name = "fk_contrato_id"))
    private Contrato contrato;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nota_fiscal_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_id"))
    private NotaFiscal notaFiscal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nota_fiscal_complementar_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_complementar_id"))
    private NotaFiscal notaFiscalComplementar;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_geracao")
    private Date dataGeracao;

    @Column(length = 6)
    private String competencia;

    /**
     * "C" -> Cancelado "L" -> Lançado (Normal) "R" -> Renegociação
     */
    @Column(length = 3)
    private String status;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_vencimento")
    private Date dataVencimento;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_pagamento")
    private Date dataPagamento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cancelamento")
    private Date dataCancelamento;

    @Column(name = "motivo_cancelamento")
    private String motivoCancelamento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_cancelamento_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario usuarioCancelamento;

    @Column(name = "valor_parcela")
    private BigDecimal valorParcela = BigDecimal.ZERO;

    @Column(name = "valor_total")
    private BigDecimal valorTotal = BigDecimal.ZERO;

    @Column(name = "valor_pago")
    private BigDecimal valorPago = BigDecimal.ZERO;

    private BigDecimal correcao = BigDecimal.ZERO;
    private BigDecimal juros = BigDecimal.ZERO;
    private BigDecimal multa = BigDecimal.ZERO;
    private BigDecimal desconto = BigDecimal.ZERO;
    
    @Column(name = "justificativa_alteracao_valor", columnDefinition = "text")
    private String justificativaAlteracaoValor;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = ContaCorrenteContrato.class, mappedBy = "mensalidade")
    private List<ContaCorrenteContrato> movimentacoesContaCorrente;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = Licenca.class, mappedBy = "mensalidade")
    private List<Licenca> listaLicencas;

    @OneToMany(fetch = FetchType.LAZY, targetEntity = HistoricoVinculacaoNotaFiscal.class, mappedBy = "mensalidade")
    @OrderBy(value = "dataHora")
    private List<HistoricoVinculacaoNotaFiscal> historicoVinculacaoNotaFiscal;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public BigDecimal getValorParcela() {
        return valorParcela;
    }

    public void setValorParcela(BigDecimal valorParcela) {
        this.valorParcela = valorParcela;
        setValorTotal();
    }

    public Date getDataGeracao() {
        return dataGeracao;
    }

    public void setDataGeracao(Date dataGeracao) {
        this.dataGeracao = dataGeracao;
    }

    public Date getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(Date dataPagamento) {
        this.dataPagamento = dataPagamento;
    }

    public Date getDataCancelamento() {
        return dataCancelamento;
    }

    public void setDataCancelamento(Date dataCancelamento) {
        this.dataCancelamento = dataCancelamento;
    }

    public String getMotivoCancelamento() {
        return motivoCancelamento;
    }

    public void setMotivoCancelamento(String motivoCancelamento) {
        this.motivoCancelamento = motivoCancelamento;
    }

    public Usuario getUsuarioCancelamento() {
        return usuarioCancelamento;
    }

    public void setUsuarioCancelamento(Usuario usuarioCancelamento) {
        this.usuarioCancelamento = usuarioCancelamento;
    }

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public void setValorTotal() {
        valorTotal = valorParcela.add(correcao);
        valorTotal = valorTotal.add(juros);
        valorTotal = valorTotal.add(multa);
        valorTotal = valorTotal.subtract(desconto);
    }

    public String getJustificativaAlteracaoValor() {
        return justificativaAlteracaoValor;
    }

    public void setJustificativaAlteracaoValor(String justificativaAlteracaoValor) {
        this.justificativaAlteracaoValor = justificativaAlteracaoValor;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
        setValorTotal();
    }

    public BigDecimal getCorrecao() {
        return correcao;
    }

    public void setCorrecao(BigDecimal correcao) {
        this.correcao = correcao;
        setValorTotal();
    }

    public BigDecimal getJuros() {
        return juros;
        
    }

    public void setJuros(BigDecimal juros) {
        this.juros = juros;
        setValorTotal();
    }

    public BigDecimal getMulta() {
        return multa;
    }

    public void setMulta(BigDecimal multa) {
        this.multa = multa;
        setValorTotal();
    }

    public List<ContaCorrenteContrato> getMovimentacoesContaCorrente() {
        return movimentacoesContaCorrente;
    }

    public void setMovimentacoesContaCorrente(List<ContaCorrenteContrato> movimentacoesContaCorrente) {
        this.movimentacoesContaCorrente = movimentacoesContaCorrente;
    }

    public List<Licenca> getListaLicencas() {
        return listaLicencas;
    }

    public void setListaLicencas(List<Licenca> listaLicencas) {
        this.listaLicencas = listaLicencas;
    }

    public List<HistoricoVinculacaoNotaFiscal> getHistoricoVinculacaoNotaFiscal() {
        return historicoVinculacaoNotaFiscal;
    }

    public void setHistoricoVinculacaoNotaFiscal(List<HistoricoVinculacaoNotaFiscal> historicoVinculacaoNotaFiscal) {
        this.historicoVinculacaoNotaFiscal = historicoVinculacaoNotaFiscal;
    }

    public NotaFiscal getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public NotaFiscal getNotaFiscalComplementar() {
        return notaFiscalComplementar;
    }

    public void setNotaFiscalComplementar(NotaFiscal notaFiscalComplementar) {
        this.notaFiscalComplementar = notaFiscalComplementar;
    }

    public BigDecimal getValorPago() {
        return valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ContratoMensalidade other = (ContratoMensalidade) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    @Override
    public String toString() {
        return Utils.getCompetencia(competencia);
    }
}
