/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import com.simples.comum.modelo.Banco;
import com.simples.comum.modelo.Cidade;
import com.simples.util.Utils;
import java.io.Serializable;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author rafael
 */
@Entity(name = "financeiro.tb_empresa")
@Table(schema = "financeiro")
public class Empresa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Campo CNPJ não pode ser vazio")
    @Size(min = 14, message = "É necessário que o cnpj possua 14 dígitos")
    @Column(length = 14, unique = true)
    private String cnpj;

    @NotBlank(message = "Informe Nome/Razão Social da empresa.")
    @Column(name = "razao_social", length = 200)
    private String razaoSocial;

    @ManyToOne
    @JoinColumn(name = "cidade_id", foreignKey = @ForeignKey(name = "fk_cidade_id"))
    private Cidade cidade;

    @Size(max = 100)
    @Column(name = "endereco_logradouro", length = 100)
    private String enderecoLogradouro;

    @Size(max = 10, message = "Informe um número de até 10 caracteres")
    @Column(name = "endereco_numero")
    private String enderecoNumero;

    @Size(max = 50, message = "Complemento possui muitos caracteres, tamanho máximo é 50.")
    @Column(name = "endereco_complemento")
    private String enderecoComplemento;

    @Size(max = 60, message = "Digite um texto menor que 60 caracteres")
    @Column(name = "endereco_bairro")
    private String enderecoBairro;

    @Size(max = 8, message = "O CEP deve conter 8 dígitos.")
    @Column(name = "endereco_cep", length = 8)
    private String enderecoCep;

    @Size(max = 100)
    @Column(name = "representante_nome")
    private String representanteNome;

    @Size(max = 100)
    @Column(name = "representante_rg")
    private String representanteRg;

    @Size(max = 11)
    @Column(name = "representante_cpf")
    private String representanteCpf;

    @ManyToOne
    @JoinColumn(name = "representante_naturalidade_id", foreignKey = @ForeignKey(name = "fk_rep_naturalidade_id"))
    private Cidade representanteNaturalidade;

    @Size(max = 100)
    @Column(name = "representante_logradouro")
    private String representanteLogradouro;

    @Size(max = 100)
    @Column(name = "representante_logradouro_numero")
    private String representanteLogradouroNumero;

    @Size(max = 100)
    @Column(name = "representante_bairro")
    private String representanteBairro;

    @ManyToOne
    @JoinColumn(name = "representante_cidade_id", foreignKey = @ForeignKey(name = "fk_rep_cidade_id"))
    private Cidade representanteCidade;

    @Size(max = 100)
    @Column(name = "representante_cep")
    private String representanteCep;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "banco_id", foreignKey = @ForeignKey(name = "fk_banco_id"))
    private Banco banco;

    @Size(max = 5)
    private String agencia;

    @Size(max = 10)
    private String conta;

    @Column(length = 100)
    private String slogan;

    private byte[] brasao;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }
    
    public String getCnpjComMascara() {
        return Utils.formatarCpfCnpj(cnpj);
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial.toUpperCase(new Locale("pt", "BR"));
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getEnderecoLogradouro() {
        return enderecoLogradouro;
    }

    public void setEnderecoLogradouro(String enderecoLogradouro) {
        this.enderecoLogradouro = enderecoLogradouro.toUpperCase(new Locale("pt", "BR"));
    }

    public String getEnderecoNumero() {
        return enderecoNumero;
    }

    public void setEnderecoNumero(String enderecoNumero) {
        this.enderecoNumero = enderecoNumero.toUpperCase(new Locale("pt", "BR"));
    }

    public String getEnderecoComplemento() {
        return enderecoComplemento;
    }

    public void setEnderecoComplemento(String enderecoComplemento) {
        this.enderecoComplemento = enderecoComplemento.toUpperCase(new Locale("pt", "BR"));
    }

    public String getEnderecoBairro() {
        return enderecoBairro;
    }

    public void setEnderecoBairro(String enderecoBairro) {
        this.enderecoBairro = enderecoBairro.toUpperCase(new Locale("pt", "BR"));
    }

    public String getEnderecoCep() {
        return enderecoCep;
    }

    public void setEnderecoCep(String enderecoCep) {
        this.enderecoCep = enderecoCep;
    }

    public String getRepresentanteNome() {
        return representanteNome;
    }

    public void setRepresentanteNome(String representanteNome) {
        this.representanteNome = representanteNome;
    }

    public String getRepresentanteRg() {
        return representanteRg;
    }

    public void setRepresentanteRg(String representanteRg) {
        this.representanteRg = representanteRg;
    }

    public String getRepresentanteCpf() {
        return representanteCpf;
    }

    public void setRepresentanteCpf(String representanteCpf) {
        this.representanteCpf = representanteCpf;
    }

    public Cidade getRepresentanteNaturalidade() {
        return representanteNaturalidade;
    }

    public void setRepresentanteNaturalidade(Cidade representanteNaturalidade) {
        this.representanteNaturalidade = representanteNaturalidade;
    }

    public String getRepresentanteLogradouro() {
        return representanteLogradouro;
    }

    public void setRepresentanteLogradouro(String representanteLogradouro) {
        this.representanteLogradouro = representanteLogradouro;
    }

    public String getRepresentanteLogradouroNumero() {
        return representanteLogradouroNumero;
    }

    public void setRepresentanteLogradouroNumero(String representanteLogradouroNumero) {
        this.representanteLogradouroNumero = representanteLogradouroNumero;
    }

    public String getRepresentanteBairro() {
        return representanteBairro;
    }

    public void setRepresentanteBairro(String representanteBairro) {
        this.representanteBairro = representanteBairro;
    }

    public Cidade getRepresentanteCidade() {
        return representanteCidade;
    }

    public void setRepresentanteCidade(Cidade representanteCidade) {
        this.representanteCidade = representanteCidade;
    }

    public String getRepresentanteCep() {
        return representanteCep;
    }

    public void setRepresentanteCep(String representanteCep) {
        this.representanteCep = representanteCep;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan.toUpperCase(new Locale("pt", "BR"));
    }

    public byte[] getBrasao() {
        return brasao;
    }

    public void setBrasao(byte[] brasao) {
        this.brasao = brasao;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Empresa other = (Empresa) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return razaoSocial;
    }
    //</editor-fold>
}
