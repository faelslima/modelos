/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Rafael
 */
@Entity(name = "financeiro.tb_licenca")
@Table(schema = "financeiro")
public class Licenca implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Informe a empresa licenciada")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato_empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_contrato_empresa_licenciada_id"))
    private ContratoEmpresaLicenciada contratoEmpresaLicenciada;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mensalidade_id", foreignKey = @ForeignKey(name = "fk_mensalidade_id"))
    private ContratoMensalidade mensalidade;

    @NotNull(message = "Informe a data de início da licença.")
    @Column(name = "data_inicio")
    @Temporal(TemporalType.DATE)
    private Date dataInicio;

    @NotNull(message = "Informe a data de encerramento da licença.")
    @Column(name = "data_fim")
    @Temporal(TemporalType.DATE)
    private Date dataFim;

    @NotBlank
    @Size(min = 3, max = 20)
    private String tipo; //[PAGAMENTO, PAGAMENTO PARCIAL, CORTESIA, CREDITO, TEMPORARIA]
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "licenca_temporaria_id", foreignKey = @ForeignKey(name = "fk_licenca_temporaria_id"))
    private LicencaTemporaria licencaTemporaria;
    
    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContratoEmpresaLicenciada getContratoEmpresaLicenciada() {
        return contratoEmpresaLicenciada;
    }

    public void setContratoEmpresaLicenciada(ContratoEmpresaLicenciada contratoEmpresaLicenciada) {
        this.contratoEmpresaLicenciada = contratoEmpresaLicenciada;
    }

    public ContratoMensalidade getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(ContratoMensalidade mensalidade) {
        this.mensalidade = mensalidade;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }
    
    public void setDataInicio(String competencia) {
        try {
            this.dataInicio = new SimpleDateFormat("yyyyMM").parse(competencia);
        } catch (ParseException ex) {
            Logger.getLogger(Licenca.class.getName()).log(Level.SEVERE, null, ex);
            this.dataInicio = null;
        }
    }

    public LicencaTemporaria getLicencaTemporaria() {
        return licencaTemporaria;
    }

    public void setLicencaTemporaria(LicencaTemporaria licencaTemporaria) {
        this.licencaTemporaria = licencaTemporaria;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Licenca other = (Licenca) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        if (dataInicio != null && dataFim != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            return "Período: " + sdf.format(dataInicio) + " - " + sdf.format(dataFim);
        }
        return null;
    }
    //</editor-fold>
}
