/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import com.simples.controleacesso.modelo.Usuario;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael
 */
@Entity(name = "financeiro.tb_licenca_liberacao_temporaria")
@Table(schema = "financeiro")
public class LicencaTemporaria implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mensalidade_id", foreignKey = @ForeignKey(name = "fk_mensalidade_id"))
    private ContratoMensalidade mensalidade;
    
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_cadastro")
    private Date dataHoraCadastro;
    
    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_vigencia_fim")
    private Date dataVigenciaFim;
    
    private boolean ativo;
    
    @Size(min = 10)
    @Column(columnDefinition = "text")
    private String justificativa;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ContratoMensalidade getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(ContratoMensalidade mensalidade) {
        this.mensalidade = mensalidade;
    }

    public Date getDataHoraCadastro() {
        return dataHoraCadastro;
    }

    public void setDataHoraCadastro(Date dataHoraCadastro) {
        this.dataHoraCadastro = dataHoraCadastro;
    }

    public Date getDataVigenciaFim() {
        return dataVigenciaFim;
    }

    public void setDataVigenciaFim(Date dataVigenciaFim) {
        this.dataVigenciaFim = dataVigenciaFim;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public String getJustificativa() {
        return justificativa;
    }

    public void setJustificativa(String justificativa) {
        this.justificativa = justificativa;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final LicencaTemporaria other = (LicencaTemporaria) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String toString = "";
        if(dataVigenciaFim != null) {
            toString += "Válido até " + new SimpleDateFormat("dd/MM/yyyy").format(dataVigenciaFim);
        }
        return toString;
    }
}
