/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.simples.financeiro.modelo.administracao;

import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_modelo_documento")
@Table(schema = "financeiro")
public class ModeloDocumento implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresa;
    
    @NotBlank(message = "Informe um nome para esse modelo de documento.")
    @Column(name = "nome_documento", length = 50)
    private String nomeDocumento;
    
    @Column(columnDefinition = "text")
    private String cabecalho;
    
    @Column(columnDefinition = "text")
    private String conteudo;
    
    @Column(columnDefinition = "text")
    private String rodape;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Empresa getEmpresa() {
        return empresa;
    }
    
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    
    public String getNomeDocumento() {
        return nomeDocumento;
    }
    
    public void setNomeDocumento(String nomeDocumento) {
        this.nomeDocumento = nomeDocumento;
    }
    
    public String getCabecalho() {
        return cabecalho;
    }
    
    public void setCabecalho(String cabecalho) {
        this.cabecalho = cabecalho;
    }
    
    public String getConteudo() {
        return conteudo;
    }
    
    public void setConteudo(String conteudo) {
        this.conteudo = conteudo;
    }
    
    public String getRodape() {
        return rodape;
    }
    
    public void setRodape(String rodape) {
        this.rodape = rodape;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ModeloDocumento other = (ModeloDocumento) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return nomeDocumento;
    }
    //</editor-fold>
}
