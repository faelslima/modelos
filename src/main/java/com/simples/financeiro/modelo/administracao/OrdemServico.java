/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.financeiro.modelo.notafiscal.NotaFiscal;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_ordem_servico")
@Table(schema = "financeiro")
public class OrdemServico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_inicio")
    private Date dataInicio;

    @NotNull
    @Temporal(TemporalType.DATE)
    @Column(name = "data_fim")
    private Date dataFim;

    @NotNull
    @Column(name = "numero_os")
    private Integer numeroOrdemServico;

    @NotNull
    @Column(name = "ano_os")
    private Integer anoOrdemServico;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data__hora_geracao")
    private Date dataHoraGeracao;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_contratante_id", foreignKey = @ForeignKey(name = "fk_cliente_id"))
    private Cliente clienteContratante;

    @Size(max = 3)
    @Column(columnDefinition = "NOT NULL DEFAULT 'LAN'")
    private String status = "LAN"; //LAN - LANÇADO / CAN - CANCELADO
    
    @Column(name = "motivo_cancelamento")
    private String motivoCancelamento;
    
    @Column(columnDefinition = "TEXT")
    private String historico;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "empresa_contratada_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresaContratada;

    @Column(name = "valor_total")
    private BigDecimal valorTotal = BigDecimal.ZERO;

    @Column(name = "valor_pago")
    private BigDecimal valorPago = BigDecimal.ZERO;

    @ManyToOne
    @JoinColumn(name = "nota_fiscal_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_id"))
    private NotaFiscal notaFiscal;

    @ManyToOne
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario cadastradoPor;

    @OneToMany(mappedBy = "ordemServico")
    @OrderBy(value = "valorServico desc")
    private List<ServicoLicenciado> servicosLicenciados = new LinkedList();

    @OneToMany(mappedBy = "ordemServico", targetEntity = PagamentoOrdemServico.class, fetch = FetchType.LAZY)
    @OrderBy(value = "dataLancamento desc")
    private List<PagamentoOrdemServico> pagamentos;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getNumeroOrdemServico() {
        return numeroOrdemServico;
    }

    public void setNumeroOrdemServico(Integer numeroOrdemServico) {
        this.numeroOrdemServico = numeroOrdemServico;
    }

    public Integer getAnoOrdemServico() {
        return anoOrdemServico;
    }

    public void setAnoOrdemServico(Integer anoOrdemServico) {
        this.anoOrdemServico = anoOrdemServico;
    }

    public Date getDataHoraGeracao() {
        return dataHoraGeracao;
    }

    public void setDataHoraGeracao(Date dataHoraGeracao) {
        this.dataHoraGeracao = dataHoraGeracao;
    }

    public Cliente getClienteContratante() {
        return clienteContratante;
    }

    public void setClienteContratante(Cliente clienteContratante) {
        this.clienteContratante = clienteContratante;
    }

    public NotaFiscal getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMotivoCancelamento() {
        return motivoCancelamento;
    }

    public void setMotivoCancelamento(String motivoCancelamento) {
        this.motivoCancelamento = motivoCancelamento;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public Empresa getEmpresaContratada() {
        return empresaContratada;
    }

    public void setEmpresaContratada(Empresa empresaContratada) {
        this.empresaContratada = empresaContratada;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public BigDecimal getValorPago() {
        return valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

    public Usuario getCadastradoPor() {
        return cadastradoPor;
    }

    public void setCadastradoPor(Usuario cadastradoPor) {
        this.cadastradoPor = cadastradoPor;
    }

    public List<ServicoLicenciado> getServicosLicenciados() {
        return servicosLicenciados;
    }

    public void setServicosLicenciados(List<ServicoLicenciado> servicosLicenciados) {
        this.servicosLicenciados = servicosLicenciados;
    }

    public List<PagamentoOrdemServico> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentoOrdemServico> pagamentos) {
        this.pagamentos = pagamentos;
    }

    public Date getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    public Date getDataFim() {
        return dataFim;
    }

    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    public String getNumeroEAnoOrdemServico() {
        if (numeroOrdemServico != null && anoOrdemServico != null) {
            return numeroOrdemServico + "/" + anoOrdemServico;
        }
        return "";
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OrdemServico other = (OrdemServico) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return getNumeroEAnoOrdemServico();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="labelForAutoComplete()">
    public String labelForAutoComplete() {
        if (clienteContratante != null) {
            return "OS " + numeroOrdemServico + "/" + anoOrdemServico + " - " + clienteContratante;
        }
        return null;
    }
    //</editor-fold>

}
