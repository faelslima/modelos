/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.notafiscal.NotaFiscal;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael
 */
@Entity(name = "financeiro.tb_pagamento")
@Table(schema = "financeiro")
@Inheritance(strategy = InheritanceType.JOINED)
public class Pagamento implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * TIPO DE RECEBIMENTO
     *
     * DIN - Dinheiro CHE - Cheque TRA - Transferência DEP - Depósito COR -
     * CORTESIA.
     */
    @NotNull
    @Column(name = "tipo_pagamento", length = 3)
    private String tipoPagamento;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresa;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nota_fiscal_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_id"))
    private NotaFiscal notaFiscal;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_lancamento")
    private Date dataLancamento = new Date();

    @NotNull
    @Column(name = "valor_pagamento")
    private BigDecimal valorPagamento = BigDecimal.ZERO;

    @NotNull
    @Column(name = "valor_disponivel")
    private BigDecimal valorDisponivel = BigDecimal.ZERO;

    @NotNull
    @Column(name = "valor_desconto")
    private BigDecimal valorDesconto = BigDecimal.ZERO;

    @NotNull
    @Column(name = "correcao")
    private BigDecimal correcao = BigDecimal.ZERO;

    @NotNull
    @Column(name = "juros")
    private BigDecimal juros = BigDecimal.ZERO;

    @NotNull
    @Column(name = "multa")
    private BigDecimal multa = BigDecimal.ZERO;

    private BigDecimal desconto = BigDecimal.ZERO;

    @Column(columnDefinition = "text")
    private String observacao;

    @Column(name = "motivo_cancelamento", columnDefinition = "text")
    private String motivoCancelamento;

    @Size(min = 8, max = 16)
    @Column(name = "tipo_documento")
    private String tipoDocumento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario criadoPor;

    //ESSES ATRIBUTOS TRANSIENTE SÃO USADOS PARA FILTRAR AS COLUNAS NO GRID
    @Transient
    private Integer numeroDocumento; //numero do contrato ou numero da os

    @Transient
    private Integer anoDocumento; //ano do contrato ou ano da os

    @Transient
    private String clienteContratanteNome;

    @Transient
    private String clienteContratanteCpfCnpj;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public BigDecimal getValorPagamento() {
        return valorPagamento;
    }

    public void setValorPagamento(BigDecimal valorPagamento) {
        this.valorPagamento = valorPagamento;
    }

    public BigDecimal getValorDisponivel() {
        return valorDisponivel;
    }

    public void setValorDisponivel(BigDecimal valorDisponivel) {
        this.valorDisponivel = valorDisponivel;
    }

    public BigDecimal getValorDesconto() {
        return valorDesconto;
    }

    public void setValorDesconto(BigDecimal valorDesconto) {
        this.valorDesconto = valorDesconto;
    }

    public BigDecimal getCorrecao() {
        return correcao;
    }

    public void setCorrecao(BigDecimal correcao) {
        this.correcao = correcao;
    }

    public BigDecimal getJuros() {
        return juros;
    }

    public void setJuros(BigDecimal juros) {
        this.juros = juros;
    }

    public BigDecimal getMulta() {
        return multa;
    }

    public void setMulta(BigDecimal multa) {
        this.multa = multa;
    }

    public BigDecimal getDesconto() {
        return desconto;
    }

    public void setDesconto(BigDecimal desconto) {
        this.desconto = desconto;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getMotivoCancelamento() {
        return motivoCancelamento;
    }

    public void setMotivoCancelamento(String motivoCancelamento) {
        this.motivoCancelamento = motivoCancelamento;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public Integer getNumeroDocumento() {
        return numeroDocumento;
    }

    public void setNumeroDocumento(Integer numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }

    public Integer getAnoDocumento() {
        return anoDocumento;
    }

    public void setAnoDocumento(Integer anoDocumento) {
        this.anoDocumento = anoDocumento;
    }

    public String getClienteContratanteNome() {
        return clienteContratanteNome;
    }

    public void setClienteContratanteNome(String clienteContratanteNome) {
        this.clienteContratanteNome = clienteContratanteNome;
    }

    public String getClienteContratanteCpfCnpj() {
        return clienteContratanteCpfCnpj;
    }

    public void setClienteContratanteCpfCnpj(String clienteContratanteCpfCnpj) {
        this.clienteContratanteCpfCnpj = clienteContratanteCpfCnpj;
    }

    public Usuario getCriadoPor() {
        return criadoPor;
    }

    public void setCriadoPor(Usuario criadoPor) {
        this.criadoPor = criadoPor;
    }

    public NotaFiscal getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        this.notaFiscal = notaFiscal;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pagamento other = (Pagamento) obj;
        return !(this.id != other.id && (this.id == null || !this.id.equals(other.id)));
    }
    //</editor-fold>

    @Override
    public String toString() {
        return valorPagamento + "";
    }
}
