/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_pagamento_contrato")
@Table(schema = "financeiro")
@PrimaryKeyJoinColumn(name = "id", foreignKey = @ForeignKey(name = "fk_pagamento_id"))
public class PagamentoContrato extends Pagamento implements Serializable {
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contrato_id", foreignKey = @ForeignKey(name = "fk_contrato_id"))
    private Contrato contrato;
    
    @OneToMany(mappedBy = "pagamento", targetEntity = ContaCorrente.class, fetch = FetchType.LAZY)
    @OrderBy(value = "dataLancamento desc")
    private List<ContaCorrente> movimentacaoContaCorrente;

    public Contrato getContrato() {
        return contrato;
    }

    public void setContrato(Contrato contrato) {
        this.contrato = contrato;
    }

    public List<ContaCorrente> getMovimentacaoContaCorrente() {
        return movimentacaoContaCorrente;
    }

    public void setMovimentacaoContaCorrente(List<ContaCorrente> movimentacaoContaCorrente) {
        this.movimentacaoContaCorrente = movimentacaoContaCorrente;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
