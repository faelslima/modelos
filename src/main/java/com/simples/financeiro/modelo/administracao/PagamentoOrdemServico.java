/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.administracao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_pagamento_ordem_servico")
@Table(schema = "financeiro")
@PrimaryKeyJoinColumn(name = "id", foreignKey = @ForeignKey(name = "fk_pagamento_id"))
public class PagamentoOrdemServico extends Pagamento implements Serializable {
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ordem_servico_id", foreignKey = @ForeignKey(name = "fk_ordem_servico_id"))
    private OrdemServico ordemServico;
    
    @OneToMany(mappedBy = "pagamento", targetEntity = ContaCorrente.class, fetch = FetchType.LAZY)
    @OrderBy(value = "dataLancamento desc")
    private List<ContaCorrente> movimentacaoContaCorrente;
    
    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }
    
    public List<ContaCorrente> getMovimentacaoContaCorrente() {
        return movimentacaoContaCorrente;
    }

    public void setMovimentacaoContaCorrente(List<ContaCorrente> movimentacaoContaCorrente) {
        this.movimentacaoContaCorrente = movimentacaoContaCorrente;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
