/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.cadastro;

import com.simples.util.Utils;
import com.simples.financeiro.modelo.administracao.Contrato;
import com.simples.financeiro.modelo.administracao.ContratoEmpresaLicenciada;
import com.simples.comum.modelo.Banco;
import com.simples.comum.modelo.Cidade;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.financeiro.modelo.administracao.ServicoLicenciado;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_cliente")
@Table(schema = "financeiro")
public class Cliente implements Serializable {

    @Id
    @Column(length = 36)
    private String id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresa;

    @NotBlank(message = "Campo nome não pode ser vazio.")
    @Column(length = 100)
    private String nome;

    @Size(min = 11, max = 14, message = "Por favor, informe o CPF/CNPJ.")
    @Column(name = "cpf_cnpj", length = 14)
    private String cpfCnpj;

    @Size(max = 200)
    @Column(name = "representante_nome")
    private String representanteNome;

    @Size(max = 11)
    @Column(name = "representante_cpf")
    private String representanteCpf;

    @Size(min = 1, message = "Informe se o tipo de pessoa é física ou jurídica.")
    @Column(name = "tipo_pessoa", length = 1)
    private String tipoPessoa = "F";

    @Size(max = 70, message = "Logradouro deve possuir no máximo 70 caracteres.")
    private String logradouro;

    @Size(max = 10, message = "Número deve possuir no máximo 10 caracteres.")
    @Column(name = "logradouro_numero")
    private String logradouroNumero;

    @Size(max = 10)
    @Column(name = "logradouro_cep")
    private String logradouroCep;

    @Size(max = 70)
    private String complemento;

    @Size(max = 70)
    private String bairro;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "cidade_id", foreignKey = @ForeignKey(name = "fk_cidade_id"))
    private Cidade cidade;

    @Size(max = 12)
    private String telefone1;

    @Size(max = 12)
    private String telefone2;

    @Email
    @Size(max = 100)
    private String email;

    @Column(name = "email_validado")
    private boolean emailValidado;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "banco_id", foreignKey = @ForeignKey(name = "fk_banco_id"))
    private Banco banco;

    @Size(max = 5)
    private String agencia;

    @Size(max = 10)
    private String conta;

    @Size(max = 80)
    private String senha;

    @Size(max = 30)
    private String slug;

    @Size(max = 10)
    @Column(name = "tipo_cliente")
    private String tipoCliente;//Prefeitura - Câmara - Outro

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_pai_id", foreignKey = @ForeignKey(name = "fk_cliente_id"))
    private Cliente clientePai;

    @OneToMany(mappedBy = "clienteContratante")
    @OrderBy(value = "id desc")
    private List<Contrato> listaContratos;

    @OneToMany(mappedBy = "clientePai")
    @OrderBy(value = "nome")
    private List<Cliente> listaClientesFilhos;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente")
    private List<ClienteContato> listaContatos;

    @Transient
    private List<ContratoEmpresaLicenciada> produtosLicenciados = new LinkedList();

    @Transient
    private List<ServicoLicenciado> servicosLicenciados = new LinkedList();

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        if (nome != null) {
            nome = nome.toUpperCase(new Locale("pt", "BR"));
        }
        this.nome = nome;
    }

    public String getRepresentanteNome() {
        return representanteNome;
    }

    public void setRepresentanteNome(String representanteNome) {
        this.representanteNome = representanteNome;
    }

    public String getRepresentanteCpf() {
        return representanteCpf;
    }

    public void setRepresentanteCpf(String representanteCpf) {
        this.representanteCpf = representanteCpf;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }
    
    public String getCpfCnpjComMascara() {
        return Utils.formatarCpfCnpj(cpfCnpj);
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro.toUpperCase(new Locale("pt", "BR"));
    }

    public String getLogradouroNumero() {
        return logradouroNumero;
    }

    public void setLogradouroNumero(String logradouroNumero) {
        this.logradouroNumero = logradouroNumero.toUpperCase(new Locale("pt", "BR"));
    }

    public String getLogradouroCep() {
        return logradouroCep;
    }

    public void setLogradouroCep(String logradouroCep) {
        this.logradouroCep = logradouroCep;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento.toUpperCase(new Locale("pt", "BR"));
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro.toUpperCase(new Locale("pt", "BR"));
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email.toLowerCase(new Locale("pt", "BR"));
    }

    public boolean isEmailValidado() {
        return emailValidado;
    }

    public void setEmailValidado(boolean emailValidado) {
        this.emailValidado = emailValidado;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getConta() {
        return conta;
    }

    public void setConta(String conta) {
        this.conta = conta;
    }

    public String getSenha() {
        return senha;
    }

    public String getCpfCnpjNome() {
        return Utils.formatarCpfCnpj(cpfCnpj) + " - " + nome;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Cliente getClientePai() {
        return clientePai;
    }

    public void setClientePai(Cliente clientePai) {
        this.clientePai = clientePai;
    }

    public List<Contrato> getListaContratos() {
        return listaContratos;
    }

    public void setListaContratos(List<Contrato> listaContratos) {
        this.listaContratos = listaContratos;
    }

    public List<Cliente> getListaClientesFilhos() {
        return listaClientesFilhos;
    }

    public void setListaClientesFilhos(List<Cliente> listaClientesFilhos) {
        this.listaClientesFilhos = listaClientesFilhos;
    }

    public List<ContratoEmpresaLicenciada> getProdutosLicenciados() {
        return produtosLicenciados;
    }

    public void setProdutosLicenciados(List<ContratoEmpresaLicenciada> produtosLicenciados) {
        this.produtosLicenciados = produtosLicenciados;
    }

    public List<ServicoLicenciado> getServicosLicenciados() {
        return servicosLicenciados;
    }

    public void setServicosLicenciados(List<ServicoLicenciado> servicosLicenciados) {
        this.servicosLicenciados = servicosLicenciados;
    }

    public List<ClienteContato> getListaContatos() {
        return listaContatos;
    }

    public void setListaContatos(List<ClienteContato> listaContatos) {
        this.listaContatos = listaContatos;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cliente other = (Cliente) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return nome;
    }
    //</editor-fold>
}
