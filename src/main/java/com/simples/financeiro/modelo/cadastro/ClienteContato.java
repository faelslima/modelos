/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.cadastro;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_cliente_contato")
@Table(schema = "financeiro")
public class ClienteContato implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "fk_cliente_id"))
    private Cliente cliente;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contato_id", foreignKey = @ForeignKey(name = "fk_contato_id"))
    private Contato contato;
    
    @NotNull
    @Column(columnDefinition = "boolean NOT NULL DEFAULT false")
    private Boolean administrativo = Boolean.FALSE;

    @NotNull
    @Column(columnDefinition = "boolean NOT NULL DEFAULT false")
    private Boolean cobranca = Boolean.FALSE;

    @NotNull
    @Column(columnDefinition = "boolean NOT NULL DEFAULT false")
    private Boolean tecnico = Boolean.FALSE;
    
    @Temporal(TemporalType.DATE)
    @Column(name="periodo_inicio")
    private Date periodoInicio;
    
    @Temporal(TemporalType.DATE)
    @Column(name="periodo_fim")
    private Date periodoFim;
    
    @Size(max = 20)
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getAdministrativo() {
        return administrativo;
    }

    public void setAdministrativo(Boolean administrativo) {
        this.administrativo = administrativo;
    }

    public Boolean getCobranca() {
        return cobranca;
    }

    public void setCobranca(Boolean cobranca) {
        this.cobranca = cobranca;
    }

    public Boolean getTecnico() {
        return tecnico;
    }

    public void setTecnico(Boolean tecnico) {
        this.tecnico = tecnico;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Date getPeriodoInicio() {
        return periodoInicio;
    }

    public void setPeriodoInicio(Date periodoInicio) {
        this.periodoInicio = periodoInicio;
    }

    public Date getPeriodoFim() {
        return periodoFim;
    }

    public void setPeriodoFim(Date periodoFim) {
        this.periodoFim = periodoFim;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteContato other = (ClienteContato) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        if (!Objects.equals(this.cliente, other.cliente)) {
            return false;
        }
        if (!Objects.equals(this.contato, other.contato)) {
            return false;
        }
        return true;
    }
}
