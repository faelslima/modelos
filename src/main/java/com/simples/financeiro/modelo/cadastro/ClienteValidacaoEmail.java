/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.cadastro;

import com.simples.financeiro.modelo.administracao.Empresa;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Rafael
 */
@Entity(name = "financeiro.tb_cliente_validacao_email")
@Table(schema = "financeiro")
public class ClienteValidacaoEmail implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "fk_cliente_id"))
    private Cliente cliente;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresa;
    
    private String email;
    
    @NotBlank
    @Size(max = 64)
    private String token;
    
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_envio")
    private Date dataHoraEnvio;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_validacao")
    private Date dataHoraValidacao;
    
    @Column(name = "user_agent_validacao")
    private String userAgentValidacao;
    
    @Column(name = "ip_validacao", length = 64)
    private String ipValidacao;
    
    @Column(name = "email_confirmado")
    private boolean emailConfirmado;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getDataHoraEnvio() {
        return dataHoraEnvio;
    }

    public void setDataHoraEnvio(Date dataHoraEnvio) {
        this.dataHoraEnvio = dataHoraEnvio;
    }

    public Date getDataHoraValidacao() {
        return dataHoraValidacao;
    }

    public void setDataHoraValidacao(Date dataHoraValidacao) {
        this.dataHoraValidacao = dataHoraValidacao;
    }

    public String getUserAgentValidacao() {
        return userAgentValidacao;
    }

    public void setUserAgentValidacao(String userAgentValidacao) {
        this.userAgentValidacao = userAgentValidacao;
    }

    public String getIpValidacao() {
        return ipValidacao;
    }

    public void setIpValidacao(String ipValidacao) {
        this.ipValidacao = ipValidacao;
    }

    public boolean isEmailConfirmado() {
        return emailConfirmado;
    }

    public void setEmailConfirmado(boolean emailConfirmado) {
        this.emailConfirmado = emailConfirmado;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 23 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteValidacaoEmail other = (ClienteValidacaoEmail) obj;
        return !(this.id != other.id && (this.id == null || !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return emailConfirmado ? "Confirmado" : "Não Confirmado";
    }
}
