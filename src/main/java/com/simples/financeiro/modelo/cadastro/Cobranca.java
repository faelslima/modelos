/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.cadastro;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.financeiro.modelo.cadastro.Contato;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_cobranca")
@Table(schema = "financeiro")
public class Cobranca implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente_id", foreignKey = @ForeignKey(name = "fk_cliente_id"))
    private Cliente cliente;
    
    @ManyToOne
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresa;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "contato_id", foreignKey = @ForeignKey(name = "fk_contato_id"))
    private Contato contato = new Contato();
    
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora")
    private Date dataHora = new Date();
    
    @Size(max = 20)
    private String situacao;
    
    @Column(columnDefinition = "TEXT NOT NULL")
    private String observacao;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario usuario;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cobranca")
    private List<CobrancaContrato> listaCobrancas;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cobranca")
    private List<CobrancaAnexo> listaAnexos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Contato getContato() {
        return contato;
    }

    public void setContato(Contato contato) {
        this.contato = contato;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<CobrancaContrato> getListaCobrancas() {
        return listaCobrancas;
    }

    public void setListaCobrancas(List<CobrancaContrato> listaCobrancas) {
        this.listaCobrancas = listaCobrancas;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<CobrancaAnexo> getListaAnexos() {
        return listaAnexos;
    }

    public void setListaAnexos(List<CobrancaAnexo> listaAnexos) {
        this.listaAnexos = listaAnexos;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cobranca other = (Cobranca) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return situacao;
    }
}
