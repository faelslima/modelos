/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.cadastro;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_cobranca_anexo")
@Table(schema = "financeiro")
public class CobrancaAnexo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cobranca_id", foreignKey = @ForeignKey(name = "fk_cobranca_id"))
    private Cobranca cobranca;

    @NotNull
    private byte[] anexo;

    @NotNull
    @Column(name = "anexo_nome", length = 100)
    private String anexoNome;

    @NotNull
    @Column(name = "anexo_extensao", length = 20)
    private String anexoExtensao;

    @NotNull
    @Column(name = "anexo_tamanho", length = 20)
    private String anexoTamanho;

    @NotNull
    @Column(name = "anexo_content_type", length = 60)
    private String anexoContentType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Cobranca getCobranca() {
        return cobranca;
    }

    public void setCobranca(Cobranca cobranca) {
        this.cobranca = cobranca;
    }

    public byte[] getAnexo() {
        return anexo;
    }

    public void setAnexo(byte[] anexo) {
        this.anexo = anexo;
    }

    public String getAnexoNome() {
        return anexoNome;
    }

    public void setAnexoNome(String anexoNome) {
        this.anexoNome = anexoNome;
    }

    public String getAnexoExtensao() {
        return anexoExtensao;
    }

    public void setAnexoExtensao(String anexoExtensao) {
        this.anexoExtensao = anexoExtensao;
    }

    public String getAnexoTamanho() {
        return anexoTamanho;
    }

    public void setAnexoTamanho(String anexoTamanho) {
        this.anexoTamanho = anexoTamanho;
    }
    
    public void setAnexoTamanho(long tamanho) {
        //Recebe como entrada o anexoTamanho de um arquivo em bytes e retorna em KB, MB, GB, TB, PB ou EB
        int unit = 1024;
        if (tamanho < unit) {
            this.anexoTamanho = tamanho + " Bytes";

        } else {
            int exp = (int) (Math.log(tamanho) / Math.log(unit));
            String unidadeMedida = ("KMGTPE").charAt(exp - 1) + "";
            this.anexoTamanho = String.format("%.1f %sB", tamanho / Math.pow(unit, exp), unidadeMedida);
        }
    }

    public String getAnexoContentType() {
        return anexoContentType;
    }

    public void setAnexoContentType(String anexoContentType) {
        this.anexoContentType = anexoContentType;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CobrancaAnexo other = (CobrancaAnexo) obj;
        if (!Objects.equals(this.anexoNome, other.anexoNome)) {
            return false;
        }
        if (!Objects.equals(this.anexoExtensao, other.anexoExtensao)) {
            return false;
        }
        if (!Objects.equals(this.anexoContentType, other.anexoContentType)) {
            return false;
        }
        return Objects.equals(this.cobranca, other.cobranca);
    }

    

    @Override
    public String toString() {
        return anexoNome + "." + anexoExtensao;
    }
}
