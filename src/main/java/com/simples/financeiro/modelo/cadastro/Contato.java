/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.cadastro;

import com.simples.comum.modelo.Cidade;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.util.Utils;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Rafael
 */
@Entity(name = "financeiro.tb_contato")
@Table(schema = "financeiro")
public class Contato implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresa;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cidade_id", foreignKey = @ForeignKey(name = "fk_cidade_id"))
    private Cidade cidade;

    @NotBlank
    @Size(max = 100)
    private String nome;

    @Size(max = 12)
    private String telefone1;

    @Size(max = 12)
    private String telefone2;

    @Size(max = 12)
    private String telefone3;

    @Size(max = 12)
    private String telefone4;

    @Size(max = 200)
    @Email
    private String email;

    @Size(max = 11)
    private String cpf;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_nascimento")
    private Date dataNascimento;

    @Size(max = 60)
    private String cargo;
    
    @Column(columnDefinition = "TEXT")
    private String observacao;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "contato")
    @OrderBy(value = "periodoFim desc")
    private List<ClienteContato> listaClientes;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public void setId(String id) {
        this.id = Long.valueOf(id);
    }

    public String getNome() {
        return Utils.getCapitular(nome);
    }

    public void setNome(String nome) {
        if (nome != null) {
            nome = nome.toUpperCase(new Locale("pt", "BR"));
        }
        this.nome = nome;
    }

    public String getTelefone1() {
        return telefone1;
    }

    public void setTelefone1(String telefone1) {
        this.telefone1 = telefone1;
    }

    public String getTelefone2() {
        return telefone2;
    }

    public void setTelefone2(String telefone2) {
        this.telefone2 = telefone2;
    }

    public String getTelefone3() {
        return telefone3;
    }

    public void setTelefone3(String telefone3) {
        this.telefone3 = telefone3;
    }

    public String getTelefone4() {
        return telefone4;
    }

    public void setTelefone4(String telefone4) {
        this.telefone4 = telefone4;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email != null) {
            email = email.toLowerCase(new Locale("pt", "BR"));
        }
        this.email = email;
    }

    public Cidade getCidade() {
        return cidade;
    }

    public void setCidade(Cidade cidade) {
        this.cidade = cidade;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public String getCargo() {
        return Utils.getCapitular(cargo);
    }

    public void setCargo(String cargo) {
        if (nome != null) {
            cargo = cargo.toUpperCase(new Locale("pt", "BR"));
        }
        this.cargo = cargo;
    }

    public List<ClienteContato> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<ClienteContato> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public String getNomeTelefone() {
        String nomeTelefone = "" + Utils.getCapitular(nome);
        if (telefone1 != null && !telefone1.isEmpty()) {
            nomeTelefone += " - " + Utils.formatarTelefone(telefone1);

        } else if (email != null && !email.isEmpty()) {
            nomeTelefone += " - " + email.toLowerCase();
        }
        return nomeTelefone;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Contato other = (Contato) obj;
        return Objects.equals(this.id, other.id);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return getNomeTelefone();
    }
    //</editor-fold>
}
