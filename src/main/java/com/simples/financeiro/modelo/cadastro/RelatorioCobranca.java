/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.cadastro;

import com.simples.financeiro.modelo.administracao.Contrato;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.util.Utils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author Rafael Lima
 */
public class RelatorioCobranca implements Serializable {
    private Cliente contratante;
    private BigDecimal valorTotalEmAtraso;
    private Integer quantidadeParcelasEmAtraso;
    private Integer totalDiasEmAtraso;
    private Integer anoContrato;
    private List<Contrato> contratos;
    private List<ContratoMensalidade> mensalidadesEmAtraso;
    private CobrancaContrato cobrancaContrato;
    private Date dataPrevista;
    private Long totalCobrancas;

    public Cliente getContratante() {
        return contratante;
    }

    public void setContratante(Cliente contratante) {
        this.contratante = contratante;
    }

    public List<Contrato> getContratos() {
        return contratos;
    }

    public void setContratos(List<Contrato> contratos) {
        this.contratos = contratos;
    }

    public BigDecimal getValorTotalEmAtraso() {
        return valorTotalEmAtraso;
    }

    public void setValorTotalEmAtraso(BigDecimal valorTotalEmAtraso) {
        this.valorTotalEmAtraso = valorTotalEmAtraso;
    }

    public Integer getQuantidadeParcelasEmAtraso() {
        return quantidadeParcelasEmAtraso;
    }

    public void setQuantidadeParcelasEmAtraso(Integer quantidadeParcelasEmAtraso) {
        this.quantidadeParcelasEmAtraso = quantidadeParcelasEmAtraso;
    }

    public Integer getTotalDiasEmAtraso() {
        return totalDiasEmAtraso;
    }

    public void setTotalDiasEmAtraso(Integer totalDiasEmAtraso) {
        this.totalDiasEmAtraso = totalDiasEmAtraso;
    }

    public List<ContratoMensalidade> getMensalidadesEmAtraso() {
        return mensalidadesEmAtraso;
    }

    public void setMensalidadesEmAtraso(List<ContratoMensalidade> mensalidadesEmAtraso) {
        this.mensalidadesEmAtraso = mensalidadesEmAtraso;
    }

    public CobrancaContrato getCobrancaContrato() {
        return cobrancaContrato;
    }

    public void setCobrancaContrato(CobrancaContrato cobrancaContrato) {
        this.cobrancaContrato = cobrancaContrato;
    }

    public Date getDataPrevista() {
        return dataPrevista;
    }

    public void setDataPrevista(Date dataPrevista) {
        this.dataPrevista = dataPrevista;
    }

    public Long getTotalCobrancas() {
        return totalCobrancas;
    }

    public void setTotalCobrancas(Long totalCobrancas) {
        this.totalCobrancas = totalCobrancas;
    }

    public Integer getAnoContrato() {
        return anoContrato;
    }

    public void setAnoContrato(Integer anoContrato) {
        this.anoContrato = anoContrato;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 71 * hash + Objects.hashCode(this.contratante);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RelatorioCobranca other = (RelatorioCobranca) obj;
        if (!Objects.equals(this.contratante, other.contratante)) {
            return false;
        }
        if (!Objects.equals(this.anoContrato, other.anoContrato)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Utils.formatarParaMonetario(valorTotalEmAtraso);
    }
}
