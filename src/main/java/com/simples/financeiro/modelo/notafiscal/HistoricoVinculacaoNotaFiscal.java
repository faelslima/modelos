/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.notafiscal;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.financeiro.modelo.administracao.OrdemServico;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_historico_vinculacao_nota_fiscal")
@Table(schema = "financeiro")
public class HistoricoVinculacaoNotaFiscal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nota_fiscal_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_id"))
    private NotaFiscal notaFiscal;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nota_fiscal_complementar_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_complementar_id"))
    private NotaFiscal notaFiscalComplementar;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mensalidade_id", foreignKey = @ForeignKey(name = "fk_mensalidade_id"))
    private ContratoMensalidade mensalidade;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ordem_servico_id", foreignKey = @ForeignKey(name = "fk_ordem_servico_id"))
    private OrdemServico ordemServico;

    @NotEmpty
    @Column(name = "tipo_operacao", length = 1)
    private String tipoOperacao; //V - Vinculação / D - Desvinculação
    
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora")
    private Date dataHora;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario usuario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NotaFiscal getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public NotaFiscal getNotaFiscalComplementar() {
        return notaFiscalComplementar;
    }

    public void setNotaFiscalComplementar(NotaFiscal notaFiscalComplementar) {
        this.notaFiscalComplementar = notaFiscalComplementar;
    }

    public ContratoMensalidade getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(ContratoMensalidade mensalidade) {
        this.mensalidade = mensalidade;
    }

    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }

    public String getTipoOperacao() {
        return tipoOperacao;
    }

    public void setTipoOperacao(String tipoOperacao) {
        this.tipoOperacao = tipoOperacao;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final HistoricoVinculacaoNotaFiscal other = (HistoricoVinculacaoNotaFiscal) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        String tipo = "";
        if (tipoOperacao != null) {
            tipo = tipoOperacao.equals("V") ? "Vinculação" : "Desvinculação";
        }
        return tipo;
    }
}
