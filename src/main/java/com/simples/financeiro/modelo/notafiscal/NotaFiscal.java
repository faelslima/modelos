/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.notafiscal;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.financeiro.modelo.administracao.OrdemServico;
import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.util.Utils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Rafael
 */
@Entity(name = "financeiro.tb_nota_fiscal")
@Table(schema = "financeiro")
public class NotaFiscal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario usuario;

    @NotNull
    @OneToOne(fetch = FetchType.LAZY)
    private NotaFiscalXml notaFiscalXml;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Empresa prestador;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Cliente tomador;

    @NotNull
    @Column(name = "numero_nota")
    private Long numeroNota;

    @Column(length = 6)
    private String competencia;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_emissao")
    private Date dataHoraEmissao;

    @NotNull
    @Column(name = "codigo_verificacao")
    private String codigoVerificacao;

    @NotNull
    @Column(name = "valor_total")
    private BigDecimal valorTotal;

    @OneToMany(mappedBy = "notaFiscal", targetEntity = ContratoMensalidade.class)
    private List<ContratoMensalidade> mensalidadesAssociadas;
    
    @OneToMany(mappedBy = "notaFiscal", targetEntity = OrdemServico.class)
    private List<OrdemServico> ordemServicoAssociadas;

    @OneToMany(mappedBy = "notaFiscalComplementar", targetEntity = ContratoMensalidade.class)
    private List<ContratoMensalidade> mensalidadesComplementaresAssociadas;

    @OneToMany(mappedBy = "notaFiscal", targetEntity = HistoricoVinculacaoNotaFiscal.class, fetch = FetchType.LAZY)
    private List<HistoricoVinculacaoNotaFiscal> historicoVinculacaoNotaFiscal;

    @OneToMany(mappedBy = "notaFiscalComplementar", targetEntity = HistoricoVinculacaoNotaFiscal.class, fetch = FetchType.LAZY)
    private List<HistoricoVinculacaoNotaFiscal> historicoVinculacaoNotaFiscalComplementar;

    @Size(max = 20)
    private String status;

    @Transient
    private boolean notaEstaVinculada;
    
    @Transient
    private String statusVinculacao;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empresa getPrestador() {
        return prestador;
    }

    public void setPrestador(Empresa prestador) {
        this.prestador = prestador;
    }

    public Cliente getTomador() {
        return tomador;
    }

    public void setTomador(Cliente tomador) {
        this.tomador = tomador;
    }

    public Long getNumeroNota() {
        return numeroNota;
    }

    public void setNumeroNota(Long numeroNota) {
        this.numeroNota = numeroNota;
    }

    public String getCompetencia() {
        return competencia;
    }
    
    public String getCompetenciaComMascara() {
        return Utils.getCompetencia(competencia);
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }

    public Date getDataHoraEmissao() {
        return dataHoraEmissao;
    }

    public void setDataHoraEmissao(Date dataHoraEmissao) {
        this.dataHoraEmissao = dataHoraEmissao;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public NotaFiscalXml getNotaFiscalXml() {
        return notaFiscalXml;
    }

    public void setNotaFiscalXml(NotaFiscalXml notaFiscalXml) {
        this.notaFiscalXml = notaFiscalXml;
    }

    public String getCodigoVerificacao() {
        return codigoVerificacao;
    }

    public void setCodigoVerificacao(String codigoVerificacao) {
        this.codigoVerificacao = codigoVerificacao;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public String getValorTotalComMascara() {
        return Utils.formatarParaMonetario(valorTotal).replace("R$", "");
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public boolean isNotaEstaVinculada() {
        return notaEstaVinculada;
    }

    public void setNotaEstaVinculada(boolean notaEstaVinculada) {
        this.notaEstaVinculada = notaEstaVinculada;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrdemServico> getOrdemServicoAssociadas() {
        return ordemServicoAssociadas;
    }

    public void setOrdemServicoAssociadas(List<OrdemServico> ordemServicoAssociadas) {
        this.ordemServicoAssociadas = ordemServicoAssociadas;
    }

    public List<HistoricoVinculacaoNotaFiscal> getHistoricoVinculacaoNotaFiscal() {
        return historicoVinculacaoNotaFiscal;
    }

    public void setHistoricoVinculacaoNotaFiscal(List<HistoricoVinculacaoNotaFiscal> historicoVinculacaoNotaFiscal) {
        this.historicoVinculacaoNotaFiscal = historicoVinculacaoNotaFiscal;
    }

    public List<HistoricoVinculacaoNotaFiscal> getHistoricoVinculacaoNotaFiscalComplementar() {
        return historicoVinculacaoNotaFiscalComplementar;
    }

    public void setHistoricoVinculacaoNotaFiscalComplementar(List<HistoricoVinculacaoNotaFiscal> historicoVinculacaoNotaFiscalComplementar) {
        this.historicoVinculacaoNotaFiscalComplementar = historicoVinculacaoNotaFiscalComplementar;
    }

    public List<ContratoMensalidade> getMensalidadesComplementaresAssociadas() {
        return mensalidadesComplementaresAssociadas;
    }

    public void setMensalidadesComplementaresAssociadas(List<ContratoMensalidade> mensalidadesComplementaresAssociadas) {
        this.mensalidadesComplementaresAssociadas = mensalidadesComplementaresAssociadas;
    }

    public String getStatusVinculacao() {
        return statusVinculacao;
    }

    public void setStatusVinculacao(String statusVinculacao) {
        this.statusVinculacao = statusVinculacao;
    }

    public List<ContratoMensalidade> getMensalidadesAssociadas() {
        return mensalidadesAssociadas;
    }

    public void setMensalidadesAssociadas(List<ContratoMensalidade> mensalidadesAssociadas) {
        this.mensalidadesAssociadas = mensalidadesAssociadas;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals() and hashCode()">
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotaFiscal other = (NotaFiscal) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        return "" + numeroNota;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="labelForAutoComplete()">
    public String labelForAutoComplete() {
        if (numeroNota != null) {
            String descricao = "Nº " + numeroNota + " - " + tomador.getNome() + " - R$" + valorTotal;
            if (competencia != null && competencia.length() == 6) {
                descricao += " (" + competencia.substring(4, 6) + "/" + competencia.substring(0, 4) + ")";
            }
            return descricao;
        }
        return null;
    }
    //</editor-fold>
}
