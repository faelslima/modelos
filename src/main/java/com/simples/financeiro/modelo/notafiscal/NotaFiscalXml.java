/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.notafiscal;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.util.Utils;
import com.xpert.jasper.JRBeanCollectionDataSource;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.Email;

/**
 *
 * @author Rafael
 */
@Entity(name = "financeiro.tb_nota_fiscal_xml")
@Table(schema = "financeiro")
public class NotaFiscalXml implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario usuario;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "notaFiscalXml")
    private List<NotaFiscalXmlItem> itensNotaFiscal;

    @NotNull
    @Size(max = 1)
    private String status; //A - Aguardando Processamento / P - Processado / I Inconsistente

    @Column(name = "motivo_inconsistencia")
    private String motivoInconsistencia;

    @NotNull
    private String tipo;

    @NotNull
    @Column(name = "numero_nota")
    private Long numeroNota;

    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_emissao")
    private Date dataHoraEmissao;

    @NotNull
    @Column(name = "dia_emissao")
    private Integer diaEmissao;

    @NotNull
    @Column(name = "mes_competencia", length = 6)
    private String mesCompetencia;

    @NotNull
    @Column(name = "situacao_nota_fiscal")
    @Size(max = 20)
    private String situacaoNotaFiscal;

    @Column(name = "codigo_cidade")
    private Long codigoCidade;

    @NotNull
    @Column(name = "usuario_cpf_cnpj", length = 14)
    private String usuarioCpfCnpj;

    @NotNull
    @Column(name = "usuario_razao_social")
    private String usuarioRazaoSocial;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_cancelamento")
    private Date dataHoraCancelamento;

    @Column(name = "rps_emissao")
    private Long rpsEmissao;

    @Column(name = "sub_emissao")
    private Long subEmissao;

    @NotNull
    @Column(name = "prestador_cpf_cnpj", length = 14)
    private String prestadorCpfCnpj;

    @NotNull
    @Column(name = "prestador_inscricao_municipal")
    private Long prestadorInscricaoMunicipal;

    @NotNull
    @Column(name = "prestador_razao_social")
    private String prestadorRazaoSocial;

    @NotNull
    @Column(name = "prestador_nome_fantasia")
    private String prestadorNomeFantasia;

    @NotNull
    @Column(name = "prestador_tipo_logradouro")
    private String prestadorTipoLogradouro;

    @NotNull
    @Column(name = "prestador_logradouro")
    private String prestadorLogradouro;

    @Column(name = "prestador_logradouro_numero")
    private String prestadorLogradouroNumero;

    @Column(name = "prestador_complemento")
    private String prestadorComplemento;

    @NotNull
    @Column(name = "prestador_tipo_bairro")
    private String prestadorTipoBairro;

    @NotNull
    @Column(name = "prestador_bairro")
    private String prestadorBairro;

    @Column(name = "prestador_cidade_codigo")
    private Long prestadorCidadeCodigo;

    @NotNull
    @Column(name = "prestador_cidade")
    private String prestadorCidade;

    @NotNull
    @Column(name = "prestador_uf", length = 2)
    private String prestadorUf;

    @NotNull
    @Column(name = "prestador_cep", length = 8)
    private String prestadorCep;

    @NotNull
    @Column(name = "prestador_ddd_telefone", length = 2)
    private String prestadorDddTelefone;

    @NotNull
    @Column(name = "prestador_telefone", length = 10)
    private String prestadorTelefone;

    @NotNull
    @Column(name = "tomador_cpf_cnpj", length = 14)
    private String tomadorCpfCnpj;

    @NotNull
    @Column(name = "tomador_razao_social")
    private String tomadorRazaoSocial;

    @NotNull
    @Column(name = "tomador_tipo_logradouro")
    private String tomadorTipoLogradouro;

    @NotNull
    @Column(name = "tomador_logradouro")
    private String tomadorLogradouro;

    @Column(name = "tomador_logradouro_numero")
    private String tomadorLogradouroNumero;

    @NotNull
    @Column(name = "tomador_tipo_bairro")
    private String tomadorTipoBairro;

    @NotNull
    @Column(name = "tomador_bairro")
    private String tomadorBairro;

    @Column(name = "tomador_cidade_codigo")
    private Long tomadorCidadeCodigo;

    @NotNull
    @Column(name = "tomador_cidade")
    private String tomadorCidade;

    @NotNull
    @Column(name = "tomador_uf", length = 2)
    private String tomadorUf;

    @NotNull
    @Column(name = "tomador_cep", length = 8)
    private String tomadorCep;

    @Email
    @Column(name = "tomador_email")
    private String tomadorEmail;

    @NotNull
    @Column(name = "tomador_optante_simples_nacional")
    private boolean tomadorOptanteSimples;

    @NotNull
    @Column(name = "valor_nota")
    private BigDecimal valorNota;

    @Column(name = "valor_deducao")
    private BigDecimal valorDeducao;

    @NotNull
    @Column(name = "valor_servico")
    private BigDecimal valorServico;

    @NotNull
    @Column(name = "valor_iss")
    private BigDecimal valorIss;

    @Column(name = "valor_pis")
    private BigDecimal valorPis;

    @Column(name = "valor_cofins")
    private BigDecimal valorCofins;

    @Column(name = "valor_inss")
    private BigDecimal valorInss;

    @Column(name = "valor_ir")
    private BigDecimal valorIr;

    @Column(name = "valor_Csll")
    private BigDecimal valorCsll;

    @Column(name = "aliquota_pis")
    private BigDecimal aliquotaPis;

    @Column(name = "aliquota_cofins")
    private BigDecimal aliquotaCofins;

    @Column(name = "aliquota_inss")
    private BigDecimal aliquotaInss;

    @Column(name = "aliquota_ir")
    private BigDecimal aliquotaIr;

    @Column(name = "aliquota_Csll")
    private BigDecimal aliquotaCsll;

    @NotNull
    @Column(name = "codigo_atividade")
    private String codigoAtividade;

    @NotNull
    @Column(name = "descricao_atividade")
    private String descricaoAtividade;

    @NotNull
    @Column(name = "grupo_atividade", length = 1)
    private String grupoAtividade;

    @NotNull
    @Column(name = "enquadramento_atividade", length = 1)
    private String enquadramentoAtividade;

    @NotNull
    @Column(name = "local_incidencia_atividade")
    private String localIncidenciaAtividade;

    @NotNull
    @Column(name = "tributavel_atividade")
    private boolean tributavelAtividade;

    @Column(name = "deducao_valor_atividade")
    private BigDecimal deducaoValorAtividade;

    @Column(name = "deducao_atividade")
    private String deducaoAtividade;

    @NotNull
    @Column(name = "atv_econ_atv")
    private boolean atvEconAtv;

    @NotNull
    @Column(name = "cos_servico")
    private String cosServico;

    @NotNull
    @Column(name = "descricao_servico")
    private String descricaoServico;

    @NotNull
    private BigDecimal aliquota;

    @NotNull
    @Column(name = "tipo_recolhimento", length = 1)
    private String tipoRecolhimento;

    @NotNull
    @Column(name = "operacao_tributacao", length = 1)
    private String operacaoTributacao;

    @Column(name = "motivo_pagamento")
    private String motivoPagamento;

    @NotNull
    @Column(name = "codigo_regime")
    private Long codigoRegime;

    @Column(name = "cidade_codigo_prestacao")
    private Long cidadeCodigoPrestacao;

    @NotNull
    @Column(name = "cidade_prestacao")
    private String cidadePrestacao;

    @NotNull
    @Column(name = "uf_prestacao")
    private String ufPrestacao;

    @NotNull
    @Column(name = "documento_prestacao", length = 5)
    private String documentoPrestacao;

    @NotNull
    @Column(name = "serie_prestacao")
    private Long seriePrestacao;

    @NotNull
    @Column(name = "tributacao_prestacao", length = 1)
    private String tributacaoPrestacao;

    @NotNull
    @Column(name = "descricao_nota", columnDefinition = "text")
    private String descricaoNota;

    @NotNull
    @Column(name = "codigo_verificacao")
    private String codigoVerificacao;

    @Column(name = "id_nota_fiscal")
    private Long idNotaFiscal;

    @Column(name = "valor_iss_ret")
    private BigDecimal valorIssRet;

    @Column(name = "aliq_ret")
    private BigDecimal aliqRet;

    @Column(name = "desconto_ret")
    private BigDecimal descontoRet;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getNumeroNota() {
        return numeroNota;
    }

    public String getNumeroNotaString() {
        String num = "0";
        if (numeroNota != null) {
            num = StringUtils.leftPad(String.valueOf(numeroNota), 8, "0");
        }
        return num;
    }

    public void setNumeroNota(Long numeroNota) {
        this.numeroNota = numeroNota;
    }

    public Date getDataHoraEmissao() {
        return dataHoraEmissao;
    }

    public void setDataHoraEmissao(Date dataHoraEmissao) {
        this.dataHoraEmissao = dataHoraEmissao;
    }

    public Integer getDiaEmissao() {
        return diaEmissao;
    }

    public void setDiaEmissao(Integer diaEmissao) {
        this.diaEmissao = diaEmissao;
    }

    public String getMesCompetencia() {
        return mesCompetencia;
    }

    public void setMesCompetencia(String mesCompetencia) {
        this.mesCompetencia = mesCompetencia;
    }

    public String getSituacaoNotaFiscal() {
        return situacaoNotaFiscal;
    }

    public void setSituacaoNotaFiscal(String situacaoNotaFiscal) {
        this.situacaoNotaFiscal = situacaoNotaFiscal;
    }

    public Long getCodigoCidade() {
        return codigoCidade;
    }

    public void setCodigoCidade(Long codigoCidade) {
        this.codigoCidade = codigoCidade;
    }

    public String getUsuarioCpfCnpj() {
        return usuarioCpfCnpj;
    }

    public void setUsuarioCpfCnpj(String usuarioCpfCnpj) {
        this.usuarioCpfCnpj = getCpfCnpjLeftPad(usuarioCpfCnpj);
    }

    public String getUsuarioRazaoSocial() {
        return usuarioRazaoSocial;
    }

    public void setUsuarioRazaoSocial(String usuarioRazaoSocial) {
        this.usuarioRazaoSocial = usuarioRazaoSocial;
    }

    public Date getDataHoraCancelamento() {
        return dataHoraCancelamento;
    }

    public void setDataHoraCancelamento(Date dataHoraCancelamento) {
        this.dataHoraCancelamento = dataHoraCancelamento;
    }

    public Long getRpsEmissao() {
        return rpsEmissao;
    }

    public void setRpsEmissao(Long rpsEmissao) {
        this.rpsEmissao = rpsEmissao;
    }

    public Long getSubEmissao() {
        return subEmissao;
    }

    public void setSubEmissao(Long subEmissao) {
        this.subEmissao = subEmissao;
    }

    public String getPrestadorCpfCnpj() {
        return prestadorCpfCnpj;
    }

    public String getPrestadorCpfCnpjComMascara() {
        return Utils.formatarCpfCnpj(prestadorCpfCnpj);
    }

    public void setPrestadorCpfCnpj(String prestadorCpfCnpj) {
        if (prestadorCpfCnpj != null && !prestadorCpfCnpj.isEmpty()) {
            prestadorCpfCnpj = StringUtils.leftPad(prestadorCpfCnpj, 14, "0");
        }
        this.prestadorCpfCnpj = getCpfCnpjLeftPad(prestadorCpfCnpj);
    }

    public Long getPrestadorInscricaoMunicipal() {
        return prestadorInscricaoMunicipal;
    }

    public void setPrestadorInscricaoMunicipal(Long prestadorInscricaoMunicipal) {
        this.prestadorInscricaoMunicipal = prestadorInscricaoMunicipal;
    }

    public String getPrestadorRazaoSocial() {
        return prestadorRazaoSocial;
    }

    public void setPrestadorRazaoSocial(String prestadorRazaoSocial) {
        this.prestadorRazaoSocial = prestadorRazaoSocial;
    }

    public String getPrestadorNomeFantasia() {
        return prestadorNomeFantasia;
    }

    public void setPrestadorNomeFantasia(String prestadorNomeFantasia) {
        this.prestadorNomeFantasia = prestadorNomeFantasia;
    }

    public String getPrestadorTipoLogradouro() {
        return prestadorTipoLogradouro;
    }

    public void setPrestadorTipoLogradouro(String prestadorTipoLogradouro) {
        this.prestadorTipoLogradouro = prestadorTipoLogradouro;
    }

    public String getPrestadorLogradouro() {
        return prestadorLogradouro;
    }

    public void setPrestadorLogradouro(String prestadorLogradouro) {
        this.prestadorLogradouro = prestadorLogradouro;
    }

    public String getPrestadorLogradouroNumero() {
        return prestadorLogradouroNumero;
    }

    public void setPrestadorLogradouroNumero(String prestadorLogradouroNumero) {
        this.prestadorLogradouroNumero = prestadorLogradouroNumero;
    }

    public String getPrestadorComplemento() {
        return prestadorComplemento;
    }

    public void setPrestadorComplemento(String prestadorComplemento) {
        this.prestadorComplemento = prestadorComplemento;
    }

    public String getPrestadorTipoBairro() {
        return prestadorTipoBairro;
    }

    public void setPrestadorTipoBairro(String prestadorTipoBairro) {
        this.prestadorTipoBairro = prestadorTipoBairro;
    }

    public String getPrestadorBairro() {
        return prestadorBairro;
    }

    public void setPrestadorBairro(String prestadorBairro) {
        this.prestadorBairro = prestadorBairro;
    }

    public Long getPrestadorCidadeCodigo() {
        return prestadorCidadeCodigo;
    }

    public void setPrestadorCidadeCodigo(Long prestadorCidadeCodigo) {
        this.prestadorCidadeCodigo = prestadorCidadeCodigo;
    }

    public String getPrestadorCidade() {
        return prestadorCidade;
    }

    public void setPrestadorCidade(String prestadorCidade) {
        this.prestadorCidade = prestadorCidade;
    }

    public String getPrestadorUf() {
        return prestadorUf;
    }

    public void setPrestadorUf(String prestadorUf) {
        this.prestadorUf = prestadorUf;
    }

    public String getPrestadorCep() {
        return prestadorCep;
    }

    public String getPrestadorCepComMascara() {
        return Utils.formatarCep(prestadorCep);
    }

    public void setPrestadorCep(String prestadorCep) {
        this.prestadorCep = prestadorCep;
    }

    public String getPrestadorDddTelefone() {
        return prestadorDddTelefone;
    }

    public void setPrestadorDddTelefone(String prestadorDddTelefone) {
        this.prestadorDddTelefone = prestadorDddTelefone;
    }

    public String getPrestadorTelefone() {
        return prestadorTelefone;
    }

    public void setPrestadorTelefone(String prestadorTelefone) {
        this.prestadorTelefone = prestadorTelefone;
    }

    public String getTomadorCpfCnpj() {
        return tomadorCpfCnpj;
    }

    public String getTomadorCpfCnpjComMascara() {
        return Utils.formatarCpfCnpj(tomadorCpfCnpj);
    }

    public void setTomadorCpfCnpj(String tomadorCpfCnpj) {
        if (tomadorCpfCnpj != null && !tomadorCpfCnpj.isEmpty()) {
            tomadorCpfCnpj = StringUtils.leftPad(tomadorCpfCnpj, 14, "0");
        }
        this.tomadorCpfCnpj = getCpfCnpjLeftPad(tomadorCpfCnpj);
    }

    public String getTomadorRazaoSocial() {
        return tomadorRazaoSocial;
    }

    public void setTomadorRazaoSocial(String tomadorRazaoSocial) {
        this.tomadorRazaoSocial = tomadorRazaoSocial;
    }

    public String getTomadorTipoLogradouro() {
        return tomadorTipoLogradouro;
    }

    public void setTomadorTipoLogradouro(String tomadorTipoLogradouro) {
        this.tomadorTipoLogradouro = tomadorTipoLogradouro;
    }

    public String getTomadorLogradouro() {
        return tomadorLogradouro;
    }

    public void setTomadorLogradouro(String tomadorLogradouro) {
        this.tomadorLogradouro = tomadorLogradouro;
    }

    public String getTomadorLogradouroNumero() {
        return tomadorLogradouroNumero;
    }

    public void setTomadorLogradouroNumero(String tomadorLogradouroNumero) {
        this.tomadorLogradouroNumero = tomadorLogradouroNumero;
    }

    public String getTomadorTipoBairro() {
        return tomadorTipoBairro;
    }

    public void setTomadorTipoBairro(String tomadorTipoBairro) {
        this.tomadorTipoBairro = tomadorTipoBairro;
    }

    public String getTomadorBairro() {
        return tomadorBairro;
    }

    public void setTomadorBairro(String tomadorBairro) {
        this.tomadorBairro = tomadorBairro;
    }

    public Long getTomadorCidadeCodigo() {
        return tomadorCidadeCodigo;
    }

    public void setTomadorCidadeCodigo(Long tomadorCidadeCodigo) {
        this.tomadorCidadeCodigo = tomadorCidadeCodigo;
    }

    public String getTomadorCidade() {
        return tomadorCidade;
    }

    public void setTomadorCidade(String tomadorCidade) {
        this.tomadorCidade = tomadorCidade;
    }

    public String getTomadorUf() {
        return tomadorUf;
    }

    public void setTomadorUf(String tomadorUf) {
        this.tomadorUf = tomadorUf;
    }

    public String getTomadorCep() {
        return tomadorCep;
    }

    public String getTomadorCepComMascara() {
        return Utils.formatarCep(tomadorCep);
    }

    public void setTomadorCep(String tomadorCep) {
        this.tomadorCep = tomadorCep;
    }

    public String getTomadorEmail() {
        return tomadorEmail;
    }

    public void setTomadorEmail(String tomadorEmail) {
        this.tomadorEmail = tomadorEmail;
    }

    public boolean isTomadorOptanteSimples() {
        return tomadorOptanteSimples;
    }

    public void setTomadorOptanteSimples(boolean tomadorOptanteSimples) {
        this.tomadorOptanteSimples = tomadorOptanteSimples;
    }

    public BigDecimal getValorNota() {
        return valorNota;
    }

    public void setValorNota(BigDecimal valorNota) {
        this.valorNota = valorNota;
    }

    public BigDecimal getValorDeducao() {
        return valorDeducao;
    }

    public void setValorDeducao(BigDecimal valorDeducao) {
        this.valorDeducao = valorDeducao;
    }

    public BigDecimal getValorServico() {
        return valorServico;
    }

    public void setValorServico(BigDecimal valorServico) {
        this.valorServico = valorServico;
    }

    public BigDecimal getValorIss() {
        return valorIss;
    }

    public void setValorIss(BigDecimal valorIss) {
        this.valorIss = valorIss;
    }

    public BigDecimal getValorPis() {
        return valorPis;
    }

    public void setValorPis(BigDecimal valorPis) {
        this.valorPis = valorPis;
    }

    public BigDecimal getValorCofins() {
        return valorCofins;
    }

    public void setValorCofins(BigDecimal valorCofins) {
        this.valorCofins = valorCofins;
    }

    public BigDecimal getValorInss() {
        return valorInss;
    }

    public void setValorInss(BigDecimal valorInss) {
        this.valorInss = valorInss;
    }

    public BigDecimal getValorIr() {
        return valorIr;
    }

    public void setValorIr(BigDecimal valorIr) {
        this.valorIr = valorIr;
    }

    public BigDecimal getValorCsll() {
        return valorCsll;
    }

    public void setValorCsll(BigDecimal valorCsll) {
        this.valorCsll = valorCsll;
    }

    public BigDecimal getAliquotaPis() {
        return aliquotaPis;
    }

    public void setAliquotaPis(BigDecimal aliquotaPis) {
        this.aliquotaPis = aliquotaPis;
    }

    public BigDecimal getAliquotaCofins() {
        return aliquotaCofins;
    }

    public void setAliquotaCofins(BigDecimal aliquotaCofins) {
        this.aliquotaCofins = aliquotaCofins;
    }

    public BigDecimal getAliquotaInss() {
        return aliquotaInss;
    }

    public void setAliquotaInss(BigDecimal aliquotaInss) {
        this.aliquotaInss = aliquotaInss;
    }

    public BigDecimal getAliquotaIr() {
        return aliquotaIr;
    }

    public void setAliquotaIr(BigDecimal aliquotaIr) {
        this.aliquotaIr = aliquotaIr;
    }

    public BigDecimal getAliquotaCsll() {
        return aliquotaCsll;
    }

    public void setAliquotaCsll(BigDecimal aliquotaCsll) {
        this.aliquotaCsll = aliquotaCsll;
    }

    public String getCodigoAtividade() {
        return codigoAtividade;
    }

    public void setCodigoAtividade(String codigoAtividade) {
        this.codigoAtividade = codigoAtividade;
    }

    public String getDescricaoAtividade() {
        return descricaoAtividade;
    }

    public void setDescricaoAtividade(String descricaoAtividade) {
        this.descricaoAtividade = descricaoAtividade;
    }

    public String getGrupoAtividade() {
        return grupoAtividade;
    }

    public void setGrupoAtividade(String grupoAtividade) {
        this.grupoAtividade = grupoAtividade;
    }

    public String getEnquadramentoAtividade() {
        return enquadramentoAtividade;
    }

    public void setEnquadramentoAtividade(String enquadramentoAtividade) {
        this.enquadramentoAtividade = enquadramentoAtividade;
    }

    public String getLocalIncidenciaAtividade() {
        return localIncidenciaAtividade;
    }

    public void setLocalIncidenciaAtividade(String localIncidenciaAtividade) {
        this.localIncidenciaAtividade = localIncidenciaAtividade;
    }

    public boolean isTributavelAtividade() {
        return tributavelAtividade;
    }

    public void setTributavelAtividade(boolean tributavelAtividade) {
        this.tributavelAtividade = tributavelAtividade;
    }

    public BigDecimal getDeducaoValorAtividade() {
        return deducaoValorAtividade;
    }

    public void setDeducaoValorAtividade(BigDecimal deducaoValorAtividade) {
        this.deducaoValorAtividade = deducaoValorAtividade;
    }

    public String getDeducaoAtividade() {
        return deducaoAtividade;
    }

    public void setDeducaoAtividade(String deducaoAtividade) {
        this.deducaoAtividade = deducaoAtividade;
    }

    public boolean isAtvEconAtv() {
        return atvEconAtv;
    }

    public void setAtvEconAtv(boolean atvEconAtv) {
        this.atvEconAtv = atvEconAtv;
    }

    public String getCosServico() {
        return cosServico;
    }

    public void setCosServico(String cosServico) {
        this.cosServico = cosServico;
    }

    public String getDescricaoServico() {
        return descricaoServico;
    }

    public void setDescricaoServico(String descricaoServico) {
        this.descricaoServico = descricaoServico;
    }

    public BigDecimal getAliquota() {
        return aliquota;
    }

    public void setAliquota(BigDecimal aliquota) {
        this.aliquota = aliquota;
    }

    public String getTipoRecolhimento() {
        return tipoRecolhimento;
    }

    public void setTipoRecolhimento(String tipoRecolhimento) {
        this.tipoRecolhimento = tipoRecolhimento;
    }

    public String getOperacaoTributacao() {
        return operacaoTributacao;
    }

    public void setOperacaoTributacao(String operacaoTributacao) {
        this.operacaoTributacao = operacaoTributacao;
    }

    public String getMotivoPagamento() {
        return motivoPagamento;
    }

    public void setMotivoPagamento(String motivoPagamento) {
        this.motivoPagamento = motivoPagamento;
    }

    public Long getCodigoRegime() {
        return codigoRegime;
    }

    public void setCodigoRegime(Long codigoRegime) {
        this.codigoRegime = codigoRegime;
    }

    public Long getCidadeCodigoPrestacao() {
        return cidadeCodigoPrestacao;
    }

    public void setCidadeCodigoPrestacao(Long cidadeCodigoPrestacao) {
        this.cidadeCodigoPrestacao = cidadeCodigoPrestacao;
    }

    public String getCidadePrestacao() {
        return cidadePrestacao;
    }

    public void setCidadePrestacao(String cidadePrestacao) {
        this.cidadePrestacao = cidadePrestacao;
    }

    public String getUfPrestacao() {
        return ufPrestacao;
    }

    public void setUfPrestacao(String ufPrestacao) {
        this.ufPrestacao = ufPrestacao;
    }

    public String getDocumentoPrestacao() {
        return documentoPrestacao;
    }

    public void setDocumentoPrestacao(String documentoPrestacao) {
        this.documentoPrestacao = documentoPrestacao;
    }

    public Long getSeriePrestacao() {
        return seriePrestacao;
    }

    public void setSeriePrestacao(Long seriePrestacao) {
        this.seriePrestacao = seriePrestacao;
    }

    public String getTributacaoPrestacao() {
        return tributacaoPrestacao;
    }

    public void setTributacaoPrestacao(String tributacaoPrestacao) {
        this.tributacaoPrestacao = tributacaoPrestacao;
    }

    public String getDescricaoNota() {
        return descricaoNota;
    }

    public void setDescricaoNota(String descricaoNota) {
        this.descricaoNota = descricaoNota;
    }

    public String getCodigoVerificacao() {
        return codigoVerificacao;
    }

    public void setCodigoVerificacao(String codigoVerificacao) {
        this.codigoVerificacao = codigoVerificacao;
    }

    public Long getIdNotaFiscal() {
        return idNotaFiscal;
    }

    public void setIdNotaFiscal(Long idNotaFiscal) {
        this.idNotaFiscal = idNotaFiscal;
    }

    public BigDecimal getValorIssRet() {
        return valorIssRet;
    }

    public void setValorIssRet(BigDecimal valorIssRet) {
        this.valorIssRet = valorIssRet;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMotivoInconsistencia() {
        return motivoInconsistencia;
    }

    public void setMotivoInconsistencia(String motivoInconsistencia) {
        this.motivoInconsistencia = motivoInconsistencia;
    }

    public BigDecimal getAliqRet() {
        return aliqRet;
    }

    public void setAliqRet(BigDecimal aliqRet) {
        this.aliqRet = aliqRet;
    }

    public BigDecimal getDescontoRet() {
        return descontoRet;
    }

    public void setDescontoRet(BigDecimal descontoRet) {
        this.descontoRet = descontoRet;
    }

    public List<NotaFiscalXmlItem> getItensNotaFiscal() {
        return itensNotaFiscal;
    }

    public JRBeanCollectionDataSource getItensNotaFiscalJrDataSource() {
        return new JRBeanCollectionDataSource(itensNotaFiscal);
    }

    public void setItensNotaFiscal(List<NotaFiscalXmlItem> itensNotaFiscal) {
        this.itensNotaFiscal = itensNotaFiscal;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotaFiscalXml other = (NotaFiscalXml) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "" + numeroNota;
    }

    private String getCpfCnpjLeftPad(String cpfCnpj) {
        if (cpfCnpj != null && cpfCnpj.trim().isEmpty()) {
            if (cpfCnpj.length() <= 11) {
                cpfCnpj = StringUtils.leftPad(cpfCnpj, 11, "0");
                cpfCnpj = StringUtils.leftPad(cpfCnpj, 11, "0");
            } else if (cpfCnpj.length() > 11) {
                cpfCnpj = StringUtils.leftPad(cpfCnpj, 14, "0");

            }
        }
        return cpfCnpj;
    }

}
