/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.notafiscal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Rafael
 */
@Entity(name = "financeiro.tb_nota_fiscal_xml_itens")
@Table(schema = "financeiro")
public class NotaFiscalXmlItem implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nota_fiscal_xml_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_xml_id"))
    private NotaFiscalXml notaFiscalXml;

    private boolean tributavel;
    
    private String descricao;
    
    private Integer quantidade;
    
    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;
    
    @Column(name = "valor_total")
    private BigDecimal valorTotal;
    
    private boolean deducao;
    
    @Column(name = "valor_iss_unitario")
    private BigDecimal valorIssUnitario;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NotaFiscalXml getNotaFiscalXml() {
        return notaFiscalXml;
    }

    public void setNotaFiscalXml(NotaFiscalXml notaFiscalXml) {
        this.notaFiscalXml = notaFiscalXml;
    }

    public boolean isTributavel() {
        return tributavel;
    }

    public void setTributavel(boolean tributavel) {
        this.tributavel = tributavel;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public BigDecimal getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(BigDecimal valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public BigDecimal getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(BigDecimal valorTotal) {
        this.valorTotal = valorTotal;
    }

    public boolean isDeducao() {
        return deducao;
    }

    public void setDeducao(boolean deducao) {
        this.deducao = deducao;
    }

    public BigDecimal getValorIssUnitario() {
        return valorIssUnitario;
    }

    public void setValorIssUnitario(BigDecimal valorIssUnitario) {
        this.valorIssUnitario = valorIssUnitario;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NotaFiscalXmlItem other = (NotaFiscalXmlItem) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
