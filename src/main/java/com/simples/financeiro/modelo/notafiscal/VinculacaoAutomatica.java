/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.notafiscal;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.administracao.Empresa;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */
@Entity(name = "financeiro.tb_vinculacao_automatica_nf")
@Table(schema = "financeiro")
public class VinculacaoAutomatica implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_id", foreignKey = @ForeignKey(name = "fk_empresa_id"))
    private Empresa empresa;
    
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora")
    private Date dataHora;
    
    @NotNull
    @Column(name = "total_mens_sem_nota_antes_vinculacao")
    private Integer totalMensalidadesSemNotaAntesVinculacao;
    
    @NotNull
    @Column(name = "total_os_sem_nota_antes_vinculacao")
    private Integer totalOrdemServicoSemNotaAntesVinculacao;
    
    @NotNull
    @Column(name = "total_mens_sem_nota_depois_vinculacao")
    private Integer totalMensalidadesSemNotaDepoisVinculacao;
    
    @NotNull
    @Column(name = "total_os_sem_nota_depois_vinculacao")
    private Integer totalOrdemServicoSemNotaDepoisVinculacao;
    
    @NotNull
    @Column(name = "total_notas_vinculadas")
    private Integer totalNotasVinculadas;
    
    @ManyToOne
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario usuario;
    
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "vinculacao")
    @OrderBy(value = "numeroNota desc")
    private List<VinculacaoAutomaticaItem> notasVinculadas;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public Integer getTotalMensalidadesSemNotaAntesVinculacao() {
        return totalMensalidadesSemNotaAntesVinculacao;
    }

    public void setTotalMensalidadesSemNotaAntesVinculacao(Integer totalMensalidadesSemNotaAntesVinculacao) {
        this.totalMensalidadesSemNotaAntesVinculacao = totalMensalidadesSemNotaAntesVinculacao;
    }

    public Integer getTotalOrdemServicoSemNotaAntesVinculacao() {
        return totalOrdemServicoSemNotaAntesVinculacao;
    }

    public void setTotalOrdemServicoSemNotaAntesVinculacao(Integer totalOrdemServicoSemNotaAntesVinculacao) {
        this.totalOrdemServicoSemNotaAntesVinculacao = totalOrdemServicoSemNotaAntesVinculacao;
    }

    public Integer getTotalMensalidadesSemNotaDepoisVinculacao() {
        return totalMensalidadesSemNotaDepoisVinculacao;
    }

    public void setTotalMensalidadesSemNotaDepoisVinculacao(Integer totalMensalidadesSemNotaDepoisVinculacao) {
        this.totalMensalidadesSemNotaDepoisVinculacao = totalMensalidadesSemNotaDepoisVinculacao;
    }

    public Integer getTotalOrdemServicoSemNotaDepoisVinculacao() {
        return totalOrdemServicoSemNotaDepoisVinculacao;
    }

    public void setTotalOrdemServicoSemNotaDepoisVinculacao(Integer totalOrdemServicoSemNotaDepoisVinculacao) {
        this.totalOrdemServicoSemNotaDepoisVinculacao = totalOrdemServicoSemNotaDepoisVinculacao;
    }

    public Integer getTotalNotasVinculadas() {
        return totalNotasVinculadas;
    }

    public void setTotalNotasVinculadas(Integer totalNotasVinculadas) {
        this.totalNotasVinculadas = totalNotasVinculadas;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<VinculacaoAutomaticaItem> getNotasVinculadas() {
        return notasVinculadas;
    }

    public void setNotasVinculadas(List<VinculacaoAutomaticaItem> notasVinculadas) {
        this.notasVinculadas = notasVinculadas;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VinculacaoAutomatica other = (VinculacaoAutomatica) obj;
        return Objects.equals(this.id, other.id);
    }

    @Override
    public String toString() {
        return "Notas Vinculadas: " + totalNotasVinculadas;
    }
}
