/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.notafiscal;

import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.financeiro.modelo.administracao.OrdemServico;
import com.simples.util.Utils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */

@Entity(name = "financeiro.tb_vinculacao_automatica_nf_item")
@Table(schema = "financeiro")
public class VinculacaoAutomaticaItem implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @Column(name = "numero_nota")
    private Long numeroNota;
    
    @NotNull
    @Column(name = "valor_nota")
    private BigDecimal valorNota;
    
    @Column(name="competencia_nf", length = 6)
    private String competenciaNotaFiscal;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "mensalidade_id", foreignKey = @ForeignKey(name = "fk_mensalidade_id"))
    private ContratoMensalidade mensalidade;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ordem_servico_id", foreignKey = @ForeignKey(name = "fk_ordem_servico_id"))
    private OrdemServico ordemServico;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "nota_fiscal_id", foreignKey = @ForeignKey(name = "fk_nota_fiscal_id"))
    private NotaFiscal notaFiscal;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "vinculacao_id", foreignKey = @ForeignKey(name = "fk_vinculacao_id"))
    private VinculacaoAutomatica vinculacao;
    
    @Transient
    private String tipoDocumento; //Utilização no filtro do datatable - Tipos: Mensalidade / Ordemd e Serviço

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumeroNota() {
        return numeroNota;
    }

    public void setNumeroNota(Long numeroNota) {
        this.numeroNota = numeroNota;
    }

    public BigDecimal getValorNota() {
        return valorNota;
    }

    public void setValorNota(BigDecimal valorNota) {
        this.valorNota = valorNota;
    }

    public NotaFiscal getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscal notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public String getCompetenciaNotaFiscal() {
        return competenciaNotaFiscal;
    }

    public void setCompetenciaNotaFiscal(String competenciaNotaFiscal) {
        this.competenciaNotaFiscal = competenciaNotaFiscal;
    }

    public ContratoMensalidade getMensalidade() {
        return mensalidade;
    }

    public void setMensalidade(ContratoMensalidade mensalidade) {
        this.mensalidade = mensalidade;
    }

    public OrdemServico getOrdemServico() {
        return ordemServico;
    }

    public void setOrdemServico(OrdemServico ordemServico) {
        this.ordemServico = ordemServico;
    }

    public VinculacaoAutomatica getVinculacao() {
        return vinculacao;
    }

    public void setVinculacao(VinculacaoAutomatica vinculacao) {
        this.vinculacao = vinculacao;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final VinculacaoAutomaticaItem other = (VinculacaoAutomaticaItem) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Nota: " + numeroNota + ", valor: " + Utils.formatarParaMonetario(valorNota);
    }
}
