/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.report;

import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.util.Utils;
import com.xpert.utils.CollectionsUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * @author Rafael
 */
public class ClienteComProduto implements Serializable {

    private String clienteContratante;
    private String clienteContratanteCnpj;
    private String periodoLicenciado;
    private String periodoPago;
    private String valorMensal;
    private String listaProdutos;

    public String getClienteContratante() {
        return clienteContratante;
    }

    public void setClienteContratante(String clienteContratante) {
        this.clienteContratante = clienteContratante;
    }

    public String getClienteContratanteCnpj() {
        return clienteContratanteCnpj;
    }

    public void setClienteContratanteCnpj(String clienteContratanteCnpj) {
        this.clienteContratanteCnpj = Utils.formatarCpfCnpj(clienteContratanteCnpj);
    }

    public String getPeriodoLicenciado() {
        return periodoLicenciado;
    }

    public void setPeriodoLicenciado(String periodoLicenciado) {
        this.periodoLicenciado = periodoLicenciado;
    }

    public void setPeriodoLicenciado(Date dataInicio, Date dataFim) {
        if (dataInicio != null && dataFim != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            periodoLicenciado = sdf.format(dataInicio) + " a " + sdf.format(dataFim);
        }
    }

    public String getPeriodoPago() {
        return periodoPago;
    }

    public void setPeriodoPago(String periodoPago) {
        this.periodoPago = periodoPago;
    }

    public void setPeriodoPago(List<ContratoMensalidade> mensalidades) {
        if (mensalidades != null && !mensalidades.isEmpty()) {
            CollectionsUtils.order(mensalidades, "competencia");
            List<String> competenciasPagas = new LinkedList();
            for (ContratoMensalidade mensalidade : mensalidades) {
                BigDecimal diferenca = mensalidade.getValorTotal().subtract(mensalidade.getValorPago());
                if (diferenca == BigDecimal.ZERO || mensalidade.getValorPago().compareTo(BigDecimal.ZERO) > 0) {
                    competenciasPagas.add(mensalidade.getCompetencia());
                }
            }
            if (!competenciasPagas.isEmpty()) {
                periodoPago = Utils.getCompetencia(competenciasPagas.get(0));
                if(competenciasPagas.size() > 1) {
                    periodoPago += " a " + Utils.getCompetencia(competenciasPagas.get(competenciasPagas.size() - 1));
                }
            }
        }
    }

    public String getValorMensal() {
        return valorMensal;
    }

    public void setValorMensal(String valorMensal) {
        this.valorMensal = valorMensal;
    }

    public void setValorMensal(Integer quantidadeParcelas, BigDecimal valor) {
        if (quantidadeParcelas != null && valor != null) {
            valorMensal = quantidadeParcelas + "x de " + Utils.formatarParaMonetario(valor).replace("R$", "");
        }
    }

    public String getListaProdutos() {
        return listaProdutos;
    }

    public void setListaProdutos(String listaProdutos) {
        this.listaProdutos = listaProdutos;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.clienteContratante);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ClienteComProduto other = (ClienteComProduto) obj;
        if (!Objects.equals(this.clienteContratante, other.clienteContratante)) {
            return false;
        }
        return true;
    }

}
