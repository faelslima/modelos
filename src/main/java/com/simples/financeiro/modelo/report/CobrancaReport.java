/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.report;

import com.simples.financeiro.modelo.notafiscal.NotaFiscal;
import com.simples.util.Utils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author Rafael Lima
 */
public class CobrancaReport implements Serializable {

    private Integer numeroContrato;
    private Integer anoContrato;
    private String clienteContratante;
    private String clienteContratanteCnpj;
    private String competencia;
    private String valorMensalidade;
    private String valorPago;
    private String valorDebito;
    private String status;
    private Integer numeroNotaFiscal;
    private String telefone;
    private String email;

    public Integer getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(Integer numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public void setNumeroContrato(Long numeroContrato) {
        this.numeroContrato = Integer.valueOf(String.valueOf(numeroContrato));
    }

    public Integer getAnoContrato() {
        return anoContrato;
    }

    public void setAnoContrato(Integer anoContrato) {
        this.anoContrato = anoContrato;
    }

    public void setAnoContrato(Long anoContrato) {
        this.anoContrato = Integer.valueOf(String.valueOf(anoContrato));
    }

    public String getClienteContratante() {
        return clienteContratante;
    }

    public void setClienteContratante(String clienteContratante) {
        this.clienteContratante = clienteContratante;
    }

    public String getClienteContratanteCnpj() {
        return clienteContratanteCnpj;
    }

    public void setClienteContratanteCnpj(String clienteContratanteCnpj) {
        this.clienteContratanteCnpj = Utils.formatarCpfCnpj(clienteContratanteCnpj);
    }

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = Utils.getCompetencia(competencia);
    }

    public String getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(String valorMensalidade) {
        this.valorMensalidade = valorMensalidade;
    }

    public void setValorMensalidade(BigDecimal valor) {
        this.valorMensalidade = Utils.formatarParaMonetario(valor);
    }

    public void setValorMensalidade(BigDecimal valorMensal, BigDecimal valorPago) {
        this.valorMensalidade = Utils.formatarParaMonetario(valorMensal);
        this.valorPago = Utils.formatarParaMonetario(valorPago);
        this.valorDebito = Utils.formatarParaMonetario(valorMensal.subtract(valorPago));
    }

    public Integer getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(Integer numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(Long numeroNotaFiscal) {
        this.numeroNotaFiscal = Integer.valueOf(String.valueOf(numeroNotaFiscal));
    }

    public void setNumeroNotaFiscal(NotaFiscal notaFiscal) {
        if (notaFiscal != null) {
            setNumeroNotaFiscal(notaFiscal.getNumeroNota());
        }
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = Utils.formatarTelefone(telefone);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getValorPago() {
        return valorPago;
    }

    public void setValorPago(String valorPago) {
        this.valorPago = valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = Utils.formatarParaMonetario(valorPago);
    }

    public String getValorDebito() {
        return valorDebito;
    }

    public void setValorDebito(String valorDebito) {
        this.valorDebito = valorDebito;
    }

    public void setValorDebito(BigDecimal valorDebito) {
        this.valorDebito = Utils.formatarParaMonetario(valorDebito);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setStatus(BigDecimal valorTotal, BigDecimal valorPago, Date dataVencimento) {
        if (valorPago != null && valorPago.compareTo(BigDecimal.ZERO) > 0) {
            if (valorPago.compareTo(valorTotal) == 0) {
                status = "PAGO";
            } else if (valorTotal.compareTo(valorPago) > 0) {
                status = "PAG. PARCIAL";
            }

        } else if (dataVencimento.compareTo(new Date()) < 0) {
            status = "ATRASADA";
        } else {
            status = "PENDENTE";
        }
    }
}
