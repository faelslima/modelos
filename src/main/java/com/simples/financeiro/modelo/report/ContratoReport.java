/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.report;

import com.simples.financeiro.modelo.administracao.ContaCorrente;
import com.simples.financeiro.modelo.administracao.ContaCorrenteContrato;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.util.Utils;
import com.xpert.jasper.JRBeanCollectionDataSource;
import com.xpert.utils.CollectionsUtils;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Rafael Lima
 */
public class ContratoReport implements Serializable {

    private Integer numeroContrato;
    private Integer anoContrato;
    private String clienteContratante;
    private String clienteContratanteCnpj;
    private String clienteContratanteTelefone;
    private String clienteContratanteEmail;
    private String tipoCliente;
    private String periodoLicenciado;
    private Integer quantidadePrestacoes;
    private String valorMensal;
    private String valorTotal;
    private JRBeanCollectionDataSource mensalidades;

    private List<EmpresaLicenciadaReport> empresasLicenciadas;

    public Integer getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(Integer numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public void setNumeroContrato(Long numeroContrato) {
        this.numeroContrato = Integer.valueOf(String.valueOf(numeroContrato));
    }

    public String getPeriodoLicenciado() {
        return periodoLicenciado;
    }

    public void setPeriodoLicenciado(String periodoLicenciado) {
        this.periodoLicenciado = periodoLicenciado;
    }

    public void setPeriodoLicenciado(Date dataInicio, Date dataEncerramento) {
        if (dataInicio != null && dataEncerramento != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String periodoInicio = sdf.format(dataInicio);
            String periodoFim = sdf.format(dataEncerramento);

            periodoLicenciado = periodoInicio + " a " + periodoFim;
        }
    }

    public String getTipoCliente() {
        return tipoCliente;
    }

    public void setTipoCliente(String tipoCliente) {
        this.tipoCliente = tipoCliente;
    }

    public String getClienteContratante() {
        return clienteContratante;
    }

    public void setClienteContratante(String clienteContratante) {
        this.clienteContratante = clienteContratante;
    }

    public String getClienteContratanteCnpj() {
        return clienteContratanteCnpj;
    }

    public void setClienteContratanteCnpj(String clienteContratanteCnpj) {
        this.clienteContratanteCnpj = Utils.formatarCpfCnpj(clienteContratanteCnpj);
    }

    public String getClienteContratanteTelefone() {
        return clienteContratanteTelefone;
    }

    public void setClienteContratanteTelefone(String clienteContratanteTelefone) {
        this.clienteContratanteTelefone = Utils.formatarTelefone(clienteContratanteTelefone);
    }

    public String getClienteContratanteEmail() {
        return clienteContratanteEmail;
    }

    public void setClienteContratanteEmail(String clienteContratanteEmail) {
        this.clienteContratanteEmail = clienteContratanteEmail;
    }

    public Integer getQuantidadePrestacoes() {
        return quantidadePrestacoes;
    }

    public void setQuantidadePrestacoes(Integer quantidadePrestacoes) {
        this.quantidadePrestacoes = quantidadePrestacoes;
    }

    public String getValorMensal() {
        return valorMensal;
    }

    public void setValorMensal(String valorMensal) {
        this.valorMensal = valorMensal;
    }

    public String getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }

    public List<EmpresaLicenciadaReport> getEmpresasLicenciadas() {
        return empresasLicenciadas;
    }

    public JRBeanCollectionDataSource getEmpresasLicenciadasJRBeanCollectionDataSource() {
        if (empresasLicenciadas != null && !empresasLicenciadas.isEmpty()) {
            return new JRBeanCollectionDataSource(empresasLicenciadas);
        }
        return null;
    }

    public void setEmpresasLicenciadas(List<EmpresaLicenciadaReport> empresasLicenciadas) {
        this.empresasLicenciadas = empresasLicenciadas;
    }

    public Integer getAnoContrato() {
        return anoContrato;
    }

    public void setAnoContrato(Integer anoContrato) {
        this.anoContrato = anoContrato;
    }

    public void setAnoContrato(Long anoContrato) {
        this.anoContrato = Integer.valueOf(String.valueOf(anoContrato));
    }

    public JRBeanCollectionDataSource getMensalidades() {
        return mensalidades;
    }

    public void setMensalidades(JRBeanCollectionDataSource mensalidades) {
        this.mensalidades = mensalidades;
    }

    public void setLancamentosReport(List<ContratoMensalidade> mensalidades) {
        List<MensalidadeReport> mensalidadesReport = new LinkedList();
        if (mensalidades != null && !mensalidades.isEmpty()) {
            for (int x = 0; x < mensalidades.size(); x++) {
                ContratoMensalidade cm = mensalidades.get(x);
                List<ContaCorrenteContrato> movimentacao = cm.getMovimentacoesContaCorrente();

                if (movimentacao != null && !movimentacao.isEmpty()) {
                    CollectionsUtils.orderAsc(movimentacao, "pagamento.dataLancamento");
                    for (ContaCorrenteContrato cc : movimentacao) {
                        if (cc.getCancelamento() == null && cc.getCompetencia().equals(cm.getCompetencia())) {
                            MensalidadeReport item = new MensalidadeReport();
                            item.setCompetencia(cm.getCompetencia());
                            item.setDataLancamento(cc.getPagamento().getDataLancamento());
                            item.setDataVencimento(cm.getDataVencimento());
                            item.setNumeroDaParcela(x + 1);
                            item.setNumeroNotaFiscal(cm.getNotaFiscal());
                            item.setNumeroNotaFiscalComplementar(cm.getNotaFiscalComplementar());
                            item.setValorMensalidade(cm.getValorTotal());
                            item.setValorPago(cc.getValorTotal());
                            item.setStatusPagamento(cm.getValorTotal(), cc.getValorTotal(), cm.getDataVencimento());

                            mensalidadesReport.add(item);
                        }
                    }
                } else {
                    MensalidadeReport item = new MensalidadeReport();
                    item.setCompetencia(cm.getCompetencia());
                    item.setDataLancamento(cm.getDataPagamento());
                    item.setDataVencimento(cm.getDataVencimento());
                    item.setNumeroDaParcela(x + 1);
                    item.setNumeroNotaFiscal(cm.getNotaFiscal() != null ? cm.getNotaFiscal().getNumeroNota() : null);
                    item.setValorMensalidade(cm.getValorTotal());
                    item.setValorPago(cm.getValorPago());
                    item.setStatusPagamento(cm.getValorTotal(), cm.getValorPago(), cm.getDataVencimento());

                    mensalidadesReport.add(item);
                }
            }
        }
        this.mensalidades = new JRBeanCollectionDataSource(mensalidadesReport);
    }
}
