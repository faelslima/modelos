/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.report;

import com.simples.util.Utils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

/**
 *
 * @author Rafael Lima
 */
public class EmpresaLicenciadaReport implements Serializable {
    
    private String razaoSocial;
    private String cpfCnpj;
    private String tipoPessoa;
    private List<ProdutoLicenciadoReport> produtosLicenciados;
    private String valorMensal;

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public List<ProdutoLicenciadoReport> getProdutosLicenciados() {
        return produtosLicenciados;
    }

    public String getValorMensal() {
        if(valorMensal == null && produtosLicenciados != null && !produtosLicenciados.isEmpty()) {
            BigDecimal mensalidade = BigDecimal.ZERO;
            for(ProdutoLicenciadoReport plr : produtosLicenciados) {
                mensalidade = mensalidade.add(getValorMensalProduto(plr.getValor()));
            }
            valorMensal = Utils.formatarParaMonetario(mensalidade);
        }
        return valorMensal;
    }

    public void setValorMensal(String valorMensal) {
        this.valorMensal = valorMensal;
    }

    public void setProdutosLicenciados(List<ProdutoLicenciadoReport> produtosLicenciados) {
        this.produtosLicenciados = produtosLicenciados;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 41 * hash + Objects.hashCode(this.razaoSocial);
        hash = 41 * hash + Objects.hashCode(this.cpfCnpj);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final EmpresaLicenciadaReport other = (EmpresaLicenciadaReport) obj;
        if (!Objects.equals(this.razaoSocial, other.razaoSocial)) {
            return false;
        }
        if (!Objects.equals(this.cpfCnpj, other.cpfCnpj)) {
            return false;
        }
        return true;
    }

    private BigDecimal getValorMensalProduto(String valor) {
        valor = valor.replace("R$ ", "").replace(".", "").replace(",", ".");
        return new BigDecimal(valor);
    }
}
