/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.report;

import com.simples.financeiro.modelo.notafiscal.NotaFiscal;
import com.simples.util.Utils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author Rafael Lima
 */
public class MensalidadeReport implements Serializable {

    private Integer numeroDaParcela;
    private String competencia;
    private Date dataVencimento;
    private Date dataLancamento;
    private String valorMensalidade;
    private String valorPago;
    private Integer numeroNotaFiscal;
    private Integer numeroNotaFiscalComplementar;
    private String notasFiscais;
    private String statusPagamento;

    public String getCompetencia() {
        return competencia;
    }

    public void setCompetencia(String competencia) {
        this.competencia = competencia;
    }

    public Date getDataVencimento() {
        return dataVencimento;
    }

    public void setDataVencimento(Date dataVencimento) {
        this.dataVencimento = dataVencimento;
    }

    public Date getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(Date dataLancamento) {
        this.dataLancamento = dataLancamento;
    }

    public String getValorMensalidade() {
        return valorMensalidade;
    }

    public void setValorMensalidade(String valorMensalidade) {
        this.valorMensalidade = Utils.formatarParaMonetario(new BigDecimal(valorMensalidade));
    }

    public void setValorMensalidade(BigDecimal valorMensalidade) {
        this.valorMensalidade = Utils.formatarParaMonetario(valorMensalidade);
    }

    public String getStatusPagamento() {
        return statusPagamento;
    }

    public void setStatusPagamento(String statusPagamento) {
        this.statusPagamento = statusPagamento;
    }

    public String getValorPago() {
        return valorPago;
    }

    public void setValorPago(String valorPago) {
        this.valorPago = Utils.formatarParaMonetario(new BigDecimal(valorPago));
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = Utils.formatarParaMonetario(valorPago);
    }

    public Integer getNumeroDaParcela() {
        return numeroDaParcela;
    }

    public void setNumeroDaParcela(Integer numeroDaParcela) {
        this.numeroDaParcela = numeroDaParcela;
    }

    public Integer getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(Integer numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    public void setNumeroNotaFiscal(Long numeroNotaFiscal) {
        if (numeroNotaFiscal != null) {
            this.numeroNotaFiscal = Integer.valueOf(String.valueOf(numeroNotaFiscal));
        }
    }

    public void setNumeroNotaFiscal(NotaFiscal notaFiscal) {
        if (notaFiscal != null) {
            this.numeroNotaFiscal = Integer.valueOf(String.valueOf(notaFiscal.getNumeroNota()));
        }
    }

    public Integer getNumeroNotaFiscalComplementar() {
        return numeroNotaFiscalComplementar;
    }

    public void setNumeroNotaFiscalComplementar(Integer numeroNotaFiscal) {
        this.numeroNotaFiscalComplementar = numeroNotaFiscal;
    }

    public void setNumeroNotaFiscalComplementar(NotaFiscal notaFiscal) {
        if (notaFiscal != null) {
            this.numeroNotaFiscalComplementar = Integer.valueOf(String.valueOf(notaFiscal.getNumeroNota()));
        }
    }

    public String getNotasFiscais() {
        notasFiscais = "";
        if (numeroNotaFiscal != null && numeroNotaFiscalComplementar == null) {
            notasFiscais = String.valueOf(numeroNotaFiscal);

        } else if (numeroNotaFiscal != null && numeroNotaFiscalComplementar != null) {
            notasFiscais = String.valueOf(numeroNotaFiscal) + " / " + String.valueOf(numeroNotaFiscalComplementar);
        }
        return notasFiscais;
    }

    public void setNotasFiscais(String notasFiscais) {
        this.notasFiscais = notasFiscais;
    }

    public void setStatusPagamento(BigDecimal valorTotal, BigDecimal valorPago, Date dataVencimento) {
        if (valorTotal.compareTo(BigDecimal.ZERO) == 0) {
            statusPagamento = "CORTESIA";
            return;
        }
        if (valorPago != null && valorPago.compareTo(BigDecimal.ZERO) > 0) {
            if (valorPago.compareTo(valorTotal) == 0) {
                statusPagamento = "PAGO";

            } else if (valorTotal.compareTo(valorPago) > 0) {
                statusPagamento = "PAG. PARCIAL";
            }

        } else if (dataVencimento.compareTo(new Date()) < 0) {
            statusPagamento = "ATRASADA";

        } else {
            statusPagamento = "PENDENTE";
        }
    }
}
