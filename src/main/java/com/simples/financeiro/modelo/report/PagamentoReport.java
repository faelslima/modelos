/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.financeiro.modelo.report;

import com.simples.financeiro.modelo.administracao.ContaCorrenteContrato;
import com.simples.financeiro.modelo.administracao.ContratoMensalidade;
import com.simples.util.Utils;
import com.xpert.jasper.JRBeanCollectionDataSource;
import com.xpert.utils.CollectionsUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Rafael Lima
 */
public class PagamentoReport implements Serializable {

    private Integer numeroContrato;
    private Integer anoContrato;
    private String periodoLicenciado;
    private Integer quantidadePrestacoes;
    private String valorTotalContrato;
    private String valorTotalJaPago;
    private String valorMensal;
    private String empresaContratada;
    private String empresaContratadaCnpj;
    private String clienteContratante;
    private String clienteContratanteCnpj;
    private JRBeanCollectionDataSource lancamentos;

    public Integer getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(Integer numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public void setNumeroContrato(Long numeroContrato) {
        this.numeroContrato = Integer.valueOf(String.valueOf(numeroContrato));
    }

    public Integer getAnoContrato() {
        return anoContrato;
    }

    public void setAnoContrato(Integer anoContrato) {
        this.anoContrato = anoContrato;
    }

    public void setAnoContrato(Long anoContrato) {
        this.anoContrato = Integer.valueOf(String.valueOf(anoContrato));
    }

    public String getPeriodoLicenciado() {
        return periodoLicenciado;
    }

    public void setPeriodoLicenciado(String periodo) {
        this.periodoLicenciado = periodo;
    }

    public void setPeriodoContratado(Date dataInicio, Date dataEncerramento) {
        if (dataInicio != null && dataEncerramento != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String periodoInicio = sdf.format(dataInicio);
            String periodoFim = sdf.format(dataEncerramento);

            periodoLicenciado = periodoInicio + " a " + periodoFim;
        }
    }

    public Integer getQuantidadePrestacoes() {
        return quantidadePrestacoes;
    }

    public void setQuantidadePrestacoes(Integer quantidadePrestacoes) {
        this.quantidadePrestacoes = quantidadePrestacoes;
    }

    public String getValorTotalContrato() {
        return valorTotalContrato;
    }

    public void setValorTotalContrato(String valorTotalContrato) {
        this.valorTotalContrato = valorTotalContrato;
    }

    public void setValorTotalContrato(BigDecimal valorTotalContrato) {
        this.valorTotalContrato = Utils.formatarParaMonetario(valorTotalContrato);
    }

    public String getValorTotalJaPago() {
        return valorTotalJaPago;
    }

    public void setValorTotalJaPago(String valorTotalJaPago) {
        this.valorTotalJaPago = valorTotalJaPago;
    }

    public void setValorTotalJaPago(BigDecimal valorTotalJaPago) {
        this.valorTotalJaPago = Utils.formatarParaMonetario(valorTotalJaPago);
    }

    public void setValorTotalJaPago(List<ContratoMensalidade> mensalidades) {
        BigDecimal totalJaPago = BigDecimal.ZERO;
        if (mensalidades != null && !mensalidades.isEmpty()) {
            for (ContratoMensalidade cm : mensalidades) {
                if (cm.getValorPago() != null && cm.getValorPago().compareTo(BigDecimal.ZERO) > 0) {
                    totalJaPago = totalJaPago.add(cm.getValorPago());
                }
            }
        }
        this.valorTotalJaPago = Utils.formatarParaMonetario(totalJaPago);
    }

    public String getEmpresaContratada() {
        return empresaContratada;
    }

    public void setEmpresaContratada(String empresaContratada) {
        this.empresaContratada = empresaContratada;
    }

    public String getEmpresaContratadaCnpj() {
        return Utils.formatarCpfCnpj(empresaContratadaCnpj);
    }

    public void setEmpresaContratadaCnpj(String empresaContratadaCnpj) {
        this.empresaContratadaCnpj = empresaContratadaCnpj;
    }

    public String getClienteContratante() {
        return clienteContratante;
    }

    public void setClienteContratante(String clienteContratante) {
        this.clienteContratante = clienteContratante;
    }

    public String getClienteContratanteCnpj() {
        return Utils.formatarCpfCnpj(clienteContratanteCnpj);
    }

    public String getValorMensal() {
        return valorMensal;
    }

    public void setValorMensal(String valorMensal) {
        this.valorMensal = valorMensal;
    }

    public void setValorMensal(BigDecimal valorMensal) {
        this.valorMensal = Utils.formatarParaMonetario(valorMensal);
    }

    public void setClienteContratanteCnpj(String clienteContratanteCnpj) {
        this.clienteContratanteCnpj = clienteContratanteCnpj;
    }

    public JRBeanCollectionDataSource getLancamentos() {
        return lancamentos;
    }

    public void setLancamentos(JRBeanCollectionDataSource lancamentos) {
        this.lancamentos = lancamentos;
    }

    public void setLancamentosReport(List<ContratoMensalidade> mensalidades) {
        List<MensalidadeReport> mensalidadesReport = new LinkedList();
        if (mensalidades != null && !mensalidades.isEmpty()) {
            for (int x = 0; x < mensalidades.size(); x++) {
                ContratoMensalidade cm = mensalidades.get(x);
                List<ContaCorrenteContrato> movimentacao = cm.getMovimentacoesContaCorrente();

                if (movimentacao != null && !movimentacao.isEmpty()) {
                    CollectionsUtils.orderAsc(movimentacao, "pagamento.dataLancamento");
                    for (ContaCorrenteContrato cc : movimentacao) {
                        if (cc.getCancelamento() == null && cc.getCompetencia().equals(cm.getCompetencia())) {
                            MensalidadeReport item = new MensalidadeReport();
                            item.setCompetencia(cm.getCompetencia());
                            item.setDataLancamento(cc.getPagamento().getDataLancamento());
                            item.setDataVencimento(cm.getDataVencimento());
                            item.setNumeroDaParcela(x + 1);
                            item.setNumeroNotaFiscal(cm.getNotaFiscal());
                            item.setNumeroNotaFiscalComplementar(cm.getNotaFiscalComplementar());
                            item.setValorMensalidade(cm.getValorTotal());
                            item.setValorPago(cc.getValorTotal());
                            item.setStatusPagamento(cm.getValorTotal(), cc.getValorTotal(), cm.getDataVencimento());

                            mensalidadesReport.add(item);
                        }
                    }
                } else {
                    MensalidadeReport item = new MensalidadeReport();
                    item.setCompetencia(cm.getCompetencia());
                    item.setDataLancamento(cm.getDataPagamento());
                    item.setDataVencimento(cm.getDataVencimento());
                    item.setNumeroDaParcela(x + 1);
                    item.setNumeroNotaFiscal(cm.getNotaFiscal());
                    item.setNumeroNotaFiscalComplementar(cm.getNotaFiscalComplementar());
                    item.setValorMensalidade(cm.getValorTotal());
                    item.setValorPago(cm.getValorPago());
                    item.setStatusPagamento(cm.getValorTotal(), cm.getValorPago(), cm.getDataVencimento());

                    mensalidadesReport.add(item);
                }
            }
        }
        lancamentos = new JRBeanCollectionDataSource(mensalidadesReport);
    }
}
