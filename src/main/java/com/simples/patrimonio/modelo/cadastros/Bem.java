package com.simples.patrimonio.modelo.cadastros;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.simples.controleacesso.modelo.EmpresaLicenciada;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author sigfran
 */
@Entity(name = "patrimonio.tb_bem")
@Table(schema = "patrimonio")
public class Bem implements Serializable, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(columnDefinition = "text")
    private String descricao;
    
    private Boolean ativo = true;

    @Size(max = 20)
    private String numeroProcesso;
    
    private String observacao;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada"))
    private EmpresaLicenciada empresaLicenciada;
    
    @NotNull
    @ManyToOne
    private Responsavel responsavel;
    
    @Size(max = 200)
    private String requerente;
    
    @NotNull
    @ManyToOne
    private Competencia competencia;
    
    private Integer quantidade = 1;
    
    @NotNull
    @ManyToOne
    private Fornecedor fornecedor;
    
    private BigDecimal valorOriginal;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAquisicao;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "data_entrou_em_uso")
    private Date dataEntrouEmUso;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataDepreciacao;
    
    private String numeroNotaFiscal;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataBaixa;

    private BigDecimal valorDepreciado;
    
    @NotNull
    @ManyToOne
    private TipoIdentificacaoBem tipoIdentificacaoBem;
    
    private int plaqueta;
    
    private String placa;
    
    private String chassi;
    
    private String matricula;
    
    private Integer garantia = 0; // Em meses
    
    @Size(max = 30)
    private String numeroEmpenho;
    
    @Size(max = 100)
    private String projetoAtividade;
    
    @Size(max = 100)
    private String elementoDespesa;
    
    @Size(max = 100)
    private String fonteRecurso;
    
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Localizacao localizacao;
    
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private PlanoDeConta planoDeConta;

    @Column(name = "vida_util")
    private Integer vidaUtil;

    @Column(name = "valor_residual")
    private BigDecimal valorResidual;
    
    @Column(name = "valor_entrada_ajustado")
    private BigDecimal valorEntradaAjustado;
    
    @Column(name = "valor_contabil_bruto")
    private BigDecimal valorContabilBruto;
    
    @OneToMany(mappedBy = "bem", fetch = FetchType.LAZY)
    @OrderBy(value = "periodo desc")
    private List<Depreciacao> listaDepreciacoes;
    
    @Transient
    private Depreciacao depreciacaoAtual;
    

    /**
     * @return the identificacao
     */
    public String getIdentificacao() {
        String identificacao = "";
        if (placa != null && !placa.equals("")) {
            identificacao = placa;
        }
        if (matricula != null && !matricula.equals("")) {
            identificacao = matricula;
        }
        if (chassi != null && !chassi.equals("")) {
            identificacao = chassi;
        }
        if (plaqueta > 0) {
            identificacao = Integer.toString(plaqueta);
        }
        return identificacao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public String getRequerente() {
        return requerente;
    }

    public void setRequerente(String requerente) {
        this.requerente = requerente;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /**
     * @return the empresaLicenciada
     */
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    /**
     * @param empresaLicenciada the empresaLicenciada to set
     */
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
       
        this.placa = placa;
    }

    /**
     * @return the fornecedor
     */
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the valorOriginal
     */
    public BigDecimal getValorOriginal() {
        return valorOriginal;
    }

    /**
     * @param valorOriginal the valorOriginal to set
     */
    public void setValorOriginal(BigDecimal valorOriginal) {
        this.valorOriginal = valorOriginal;
    }

    /**
     * @return the dataAquisicao
     */
    public Date getDataAquisicao() {
        return dataAquisicao;
    }

    /**
     * @param dataAquisicao the dataAquisicao to set
     */
    public void setDataAquisicao(Date dataAquisicao) {
        this.dataAquisicao = dataAquisicao;
    }

    /**
     * @return the dataDepreciacao
     */
    public Date getDataDepreciacao() {
        return dataDepreciacao;
    }

    /**
     * @param dataDepreciacao the dataDepreciacao to set
     */
    public void setDataDepreciacao(Date dataDepreciacao) {
        this.dataDepreciacao = dataDepreciacao;
    }

    public Date getDataEntrouEmUso() {
        return dataEntrouEmUso;
    }

    public void setDataEntrouEmUso(Date dataEntrouEmUso) {
        this.dataEntrouEmUso = dataEntrouEmUso;
    }

    /**
     * @return the numeroNotaFiscal
     */
    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    /**
     * @param numeroNotaFiscal the numeroNotaFiscal to set
     */
    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    /**
     * @return the dataBaixa
     */
    public Date getDataBaixa() {
        return dataBaixa;
    }

    /**
     * @param dataBaixa the dataBaixa to set
     */
    public void setDataBaixa(Date dataBaixa) {
        this.dataBaixa = dataBaixa;
    }

    /**
     * @return the responsavel
     */
    public Responsavel getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(Responsavel responsavel) {
        this.responsavel = responsavel;
    }

    /**
     * @return the quantidade
     */
    public Integer getQuantidade() {
        return quantidade;
    }

    /**
     * @param quantidade the quantidade to set
     */
    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {

       

        this.matricula = matricula;
    }

    /**
     * @return the garantia
     */
    public Integer getGarantia() {
        return garantia;
    }

    /**
     * @param garantia the garantia to set
     */
    public void setGarantia(Integer garantia) {
        this.garantia = garantia;
    }

   

    /**
     * @return the tipoIdentificacaoBem
     */
    public TipoIdentificacaoBem getTipoIdentificacaoBem() {
        return tipoIdentificacaoBem;
    }

    /**
     * @param tipoIdentificacaoBem the tipoIdentificacaoBem to set
     */
    public void setTipoIdentificacaoBem(TipoIdentificacaoBem tipoIdentificacaoBem) {
        this.tipoIdentificacaoBem = tipoIdentificacaoBem;
    }

    /**
     * @return the projetoAtividade
     */
    public String getProjetoAtividade() {
        return projetoAtividade;
    }

    /**
     * @param projetoAtividade the projetoAtividade to set
     */
    public void setProjetoAtividade(String projetoAtividade) {
        this.projetoAtividade = projetoAtividade;
    }

    /**
     * @return the elementoDespesa
     */
    public String getElementoDespesa() {
        return elementoDespesa;
    }

    /**
     * @param elementoDespesa the elementoDespesa to set
     */
    public void setElementoDespesa(String elementoDespesa) {
        this.elementoDespesa = elementoDespesa;
    }

    /**
     * @return the fonteRecurso
     */
    public String getFonteRecurso() {
        return fonteRecurso;
    }

    /**
     * @param fonteRecurso the fonteRecurso to set
     */
    public void setFonteRecurso(String fonteRecurso) {
        this.fonteRecurso = fonteRecurso;
    }

    /**
     * @return the numeroEmpenho
     */
    public String getNumeroEmpenho() {
        return numeroEmpenho;
    }

    /**
     * @param numeroEmpenho the numeroEmpenho to set
     */
    public void setNumeroEmpenho(String numeroEmpenho) {
        this.numeroEmpenho = numeroEmpenho;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the numeroProcesso
     */
    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    /**
     * @param numeroProcesso the numeroProcesso to set
     */
    public void setNumeroProcesso(String numeroProcesso) {
        this.numeroProcesso = numeroProcesso;
    }

    /**
     * @return the competencia
     */
    public Competencia getCompetencia() {
        return competencia;
    }

    /**
     * @param competencia the competencia to set
     */
    public void setCompetencia(Competencia competencia) {
        this.competencia = competencia;
    }

    /**
     * @return the plaqueta
     */
    public int getPlaqueta() {
        return plaqueta;
    }

    /**
     * @param plaqueta the plaqueta to set
     */
    public void setPlaqueta(int plaqueta) {
     
        this.plaqueta = plaqueta;
    }

    /**
     * @return the chassi
     */
    public String getChassi() {
        return chassi;
    }

    /**
     * @param chassi the chassi to set
     */
    public void setChassi(String chassi) {
       
        this.chassi = chassi;
    }

    /**
     * @return the valorDepreciado
     */
    public BigDecimal getValorDepreciado() {
        return valorDepreciado;
    }

    /**
     * @param valorDepreciado the valorDepreciado to set
     */
    public void setValorDepreciado(BigDecimal valorDepreciado) {
        this.valorDepreciado = valorDepreciado;
    }

    /**
     * @return the localizacao
     */
    public Localizacao getLocalizacao() {
        return localizacao;
    }

    /**
     * @param localizacao the localizacao to set
     */
    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public PlanoDeConta getPlanoDeConta() {
        return planoDeConta;
    }

    public void setPlanoDeConta(PlanoDeConta planoDeConta) {
        this.planoDeConta = planoDeConta;
    }

    public Integer getVidaUtil() {
        return vidaUtil;
    }

    public void setVidaUtil(Integer vidaUtil) {
        this.vidaUtil = vidaUtil;
    }

    public BigDecimal getValorResidual() {
        return valorResidual;
    }

    public void setValorResidual(BigDecimal valorResidual) {
        this.valorResidual = valorResidual;
    }

    public BigDecimal getValorEntradaAjustado() {
        return valorEntradaAjustado;
    }

    public void setValorEntradaAjustado(BigDecimal valorEntradaAjustado) {
        this.valorEntradaAjustado = valorEntradaAjustado;
    }

    public BigDecimal getValorContabilBruto() {
        return valorContabilBruto;
    }

    public void setValorContabilBruto(BigDecimal valorContabilBruto) {
        this.valorContabilBruto = valorContabilBruto;
    }

    public List<Depreciacao> getListaDepreciacoes() {
        return listaDepreciacoes;
    }

    public void setListaDepreciacoes(List<Depreciacao> listaDepreciacoes) {
        this.listaDepreciacoes = listaDepreciacoes;
    }

    public Depreciacao getDepreciacaoAtual() {
        return depreciacaoAtual;
    }

    public void setDepreciacaoAtual(Depreciacao depreciacaoAtual) {
        this.depreciacaoAtual = depreciacaoAtual;
    }
    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bem other = (Bem) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descricao;
    }

}
