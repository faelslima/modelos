/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.controleacesso.modelo.Usuario;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author sigfran
 */
@Entity(name = "patrimonio.tb_competencia")
@Table(schema = "patrimonio")
public class Competencia implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private Integer ano;

    @NotNull
    private Integer mes;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataCadastro;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada_id"))
    private EmpresaLicenciada empresaLicenciada;

    private String situacao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "responsavel_fechamento_id", foreignKey = @ForeignKey(name = "fk_usuario_fech_id"))
    private Usuario responsavelFechamento;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataFechamento;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date dataReabertura;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "responsavel_reabertura_id", foreignKey = @ForeignKey(name = "fk_usuario_reab_id"))
    private Usuario responsavelReabertura;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAno() {
        return ano;
    }

    public void setAno(Integer ano) {
        this.ano = ano;
    }

    public Integer getMes() {
        return mes;
    }

    public void setMes(Integer mes) {
        this.mes = mes;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public Usuario getResponsavelFechamento() {
        return responsavelFechamento;
    }

    public void setResponsavelFechamento(Usuario responsavelFechamento) {
        this.responsavelFechamento = responsavelFechamento;
    }

    public Date getDataFechamento() {
        return dataFechamento;
    }

    public void setDataFechamento(Date dataFechamento) {
        this.dataFechamento = dataFechamento;
    }

    public Date getDataReabertura() {
        return dataReabertura;
    }

    public void setDataReabertura(Date dataReabertura) {
        this.dataReabertura = dataReabertura;
    }

    public Usuario getResponsavelReabertura() {
        return responsavelReabertura;
    }

    public void setResponsavelReabertura(Usuario responsavelReabertura) {
        this.responsavelReabertura = responsavelReabertura;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Competencia other = (Competencia) obj;
        return Objects.equals(this.id, other.id);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="toString()">
    @Override
    public String toString() {
        if (mes != null && ano != null) {
            return StringUtils.leftPad(String.valueOf(mes), 2, "0") + "/" + ano;
        }
        return "";
    }
    //</editor-fold>
}
