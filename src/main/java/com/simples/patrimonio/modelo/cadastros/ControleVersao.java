/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.cadastros;

import java.io.Serializable;

/**
 *
 * @author sigfran
 */
public class ControleVersao implements Serializable{
    private String versaoRelatorioSintetico;
    private String versaoRelatorioAnalitico;
    private String versaoAplicacao;

    /**
     * @return the versaoRelatorioSintetico
     */
    public String getVersaoRelatorioSintetico() {
        return versaoRelatorioSintetico;
    }

    /**
     * @param versaoRelatorioSintetico the versaoRelatorioSintetico to set
     */
    public void setVersaoRelatorioSintetico(String versaoRelatorioSintetico) {
        this.versaoRelatorioSintetico = versaoRelatorioSintetico;
    }

    /**
     * @return the versaoRelatorioAnalitico
     */
    public String getVersaoRelatorioAnalitico() {
        return versaoRelatorioAnalitico;
    }

    /**
     * @param versaoRelatorioAnalitico the versaoRelatorioAnalitico to set
     */
    public void setVersaoRelatorioAnalitico(String versaoRelatorioAnalitico) {
        this.versaoRelatorioAnalitico = versaoRelatorioAnalitico;
    }

    /**
     * @return the versaoAplicacao
     */
    public String getVersaoAplicacao() {
        return versaoAplicacao;
    }

    /**
     * @param versaoAplicacao the versaoAplicacao to set
     */
    public void setVersaoAplicacao(String versaoAplicacao) {
        this.versaoAplicacao = versaoAplicacao;
    }
    
    
}
