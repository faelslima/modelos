/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;

/**
 *
 * @author sigfran
 */
@Entity(name = "patrimonio.tb_depreciacao")
@Table(schema = "patrimonio")
public class Depreciacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne
    private Bem bem;
    
    @NotNull
    private BigDecimal valorOriginal;
    
    private Integer vidaUtil;
    
    private BigDecimal valorResidual;
    
    private BigDecimal valorDepreciavel;
    
    private BigDecimal valorDepreciacaoMensal;
    
    private BigDecimal valorDepreciacaoAnual;
    
    private BigDecimal valorBemDepreciado;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAquisicao;
    
    private String periodo; //aaaamm
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada"))
    private EmpresaLicenciada empresaLicenciada;

    
    
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the bem
     */
    public Bem getBem() {
        return bem;
    }

    /**
     * @param bem the bem to set
     */
    public void setBem(Bem bem) {
        this.bem = bem;
    }

    /**
     * @return the valorOriginal
     */
    public BigDecimal getValorOriginal() {
        return valorOriginal;
    }

    /**
     * @param valorOriginal the valorOriginal to set
     */
    public void setValorOriginal(BigDecimal valorOriginal) {
        this.valorOriginal = valorOriginal;
    }

    /**
     * @return the vidaUtil
     */
    public Integer getVidaUtil() {
        return vidaUtil;
    }

    /**
     * @param vidaUtil the vidaUtil to set
     */
    public void setVidaUtil(Integer vidaUtil) {
        this.vidaUtil = vidaUtil;
    }

    /**
     * @return the valorResidual
     */
    public BigDecimal getValorResidual() {
        return valorResidual;
    }

    /**
     * @param valorResidual the valorResidual to set
     */
    public void setValorResidual(BigDecimal valorResidual) {
        this.valorResidual = valorResidual;
    }

    /**
     * @return the valorDepreciavel
     */
    public BigDecimal getValorDepreciavel() {
        return valorDepreciavel;
    }

    /**
     * @param valorDepreciavel the valorDepreciavel to set
     */
    public void setValorDepreciavel(BigDecimal valorDepreciavel) {
        this.valorDepreciavel = valorDepreciavel;
    }

    /**
     * @return the valorDepreciacaoMensal
     */
    public BigDecimal getValorDepreciacaoMensal() {
        return valorDepreciacaoMensal;
    }

    /**
     * @param valorDepreciacaoMensal the valorDepreciacaoMensal to set
     */
    public void setValorDepreciacaoMensal(BigDecimal valorDepreciacaoMensal) {
        this.valorDepreciacaoMensal = valorDepreciacaoMensal;
    }

    /**
     * @return the valorDepreciacaoAnual
     */
    public BigDecimal getValorDepreciacaoAnual() {
        return valorDepreciacaoAnual;
    }

    /**
     * @param valorDepreciacaoAnual the valorDepreciacaoAnual to set
     */
    public void setValorDepreciacaoAnual(BigDecimal valorDepreciacaoAnual) {
        this.valorDepreciacaoAnual = valorDepreciacaoAnual;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the empresaLicenciada
     */
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    /**
     * @param empresaLicenciada the empresaLicenciada to set
     */
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    /**
     * @return the dataAquisicao
     */
    public Date getDataAquisicao() {
        return dataAquisicao;
    }

    /**
     * @param dataAquisicao the dataAquisicao to set
     */
    public void setDataAquisicao(Date dataAquisicao) {
        this.dataAquisicao = dataAquisicao;
    }

    /**
     * @return the valorBemDepreciado
     */
    public BigDecimal getValorBemDepreciado() {
        return valorBemDepreciado;
    }

    /**
     * @param valorBemDepreciado the valorBemDepreciado to set
     */
    public void setValorBemDepreciado(BigDecimal valorBemDepreciado) {
        this.valorBemDepreciado = valorBemDepreciado;
    }

}
