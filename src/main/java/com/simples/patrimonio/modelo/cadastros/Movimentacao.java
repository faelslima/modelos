/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.util.Utils;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.servlet.http.Part;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author sigfran
 */
@Entity(name = "patrimonio.tb_movimentacao")
@Table(schema = "patrimonio")
public class Movimentacao implements Serializable, Cloneable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataInicio;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataFim;

    private String observacoes;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataBaixa;

    @Size(max = 100)
    @Column(name = "nome_arquivo_laudo_baixa")
    private String nomeArquivoLaudoBaixa;

    @Size(max = 200)
    @Column(name = "content_type_laudo_baixa")
    private String contentTypeLaudoBaixa;

    @Column(name = "laudo_baixa")
    private byte[] laudoBaixa;

    @NotNull
    @ManyToOne
    private TipoMovimentacao tipoMovimentacao;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private Bem bem;

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataMovimentacao = new Date();

    @ManyToOne
    private TipoBaixa tipoBaixa;

    @ManyToOne
    private Localizacao localizacaoDestino;

    @ManyToOne
    private Localizacao localizacaoOrigem;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada"))
    private EmpresaLicenciada empresaLicenciada;

    @ManyToOne
    private Fornecedor fornecedor;

    @NotNull
    @ManyToOne
    private Competencia competencia;

    @Transient
    private String responsavel;

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the dataInicio
     */
    public Date getDataInicio() {
        return dataInicio;
    }

    /**
     * @param dataInicio the dataInicio to set
     */
    public void setDataInicio(Date dataInicio) {
        this.dataInicio = dataInicio;
    }

    /**
     * @return the dataFim
     */
    public Date getDataFim() {
        return dataFim;
    }

    /**
     * @param dataFim the dataFim to set
     */
    public void setDataFim(Date dataFim) {
        this.dataFim = dataFim;
    }

    /**
     * @return the observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * @param observacoes the observacoes to set
     */
    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

    /**
     * @return the tipoMovimentacao
     */
    public TipoMovimentacao getTipoMovimentacao() {
        return tipoMovimentacao;
    }

    /**
     * @param tipoMovimentacao the tipoMovimentacao to set
     */
    public void setTipoMovimentacao(TipoMovimentacao tipoMovimentacao) {
        this.tipoMovimentacao = tipoMovimentacao;
    }

    /**
     * @return the bem
     */
    public Bem getBem() {
        return bem;
    }

    /**
     * @param bem the bem to set
     */
    public void setBem(Bem bem) {
        this.bem = bem;
    }

    public String getNomeArquivoLaudoBaixa() {
        return nomeArquivoLaudoBaixa;
    }

    public void setNomeArquivoLaudoBaixa(String nomeArquivoLaudoBaixa) {
        this.nomeArquivoLaudoBaixa = nomeArquivoLaudoBaixa;
    }

    public String getContentTypeLaudoBaixa() {
        return contentTypeLaudoBaixa;
    }

    public void setContentTypeLaudoBaixa(String contentTypeLaudoBaixa) {
        this.contentTypeLaudoBaixa = contentTypeLaudoBaixa;
    }

    public byte[] getLaudoBaixa() {
        return laudoBaixa;
    }

    public void setLaudoBaixa(byte[] laudoBaixa) {
        this.laudoBaixa = laudoBaixa;
    }

    public void setLaudoBaixa(Part laudoBaixa) {
        contentTypeLaudoBaixa = laudoBaixa.getContentType();
        nomeArquivoLaudoBaixa = getNomeArquivoLaudoBaixa(Utils.getPartFileName(laudoBaixa));
        this.laudoBaixa = Utils.getByteArray(laudoBaixa);
    }
    
    public void excluirLaudo() {
        contentTypeLaudoBaixa = null;
        nomeArquivoLaudoBaixa = null;
        this.laudoBaixa = null;;
    }

    private String getNomeArquivoLaudoBaixa(String nomeArquivoLaudoBaixa) {
        if (nomeArquivoLaudoBaixa != null && !nomeArquivoLaudoBaixa.trim().isEmpty() && nomeArquivoLaudoBaixa.split("\\.").length > 2) {
            String[] nomes = nomeArquivoLaudoBaixa.split("\\.");
            String nome = "";
            for (int x = 0; x < nomes.length - 1; x++) {
                nome += nomes[x];
            }
            nome += "." + nomes[nomes.length - 1];
            nomeArquivoLaudoBaixa = nome;
        }
        return nomeArquivoLaudoBaixa;
    }

    public void setLaudoBaixa(UploadedFile laudoBaixa) throws IOException {
        this.laudoBaixa = Utils.getByteArray(laudoBaixa.getInputstream());
        contentTypeLaudoBaixa = laudoBaixa.getContentType();
        nomeArquivoLaudoBaixa = getNomeArquivoLaudoBaixa(laudoBaixa.getFileName());
    }

    public void setLaudoBaixaFileUploadEvent(FileUploadEvent eventLaudoBaixa) throws IOException {
        setLaudoBaixa(eventLaudoBaixa.getFile());
    }

    /**
     * @return the empresaLicenciada
     */
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    /**
     * @param empresaLicenciada the empresaLicenciada to set
     */
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    /**
     * @return the fornecedor
     */
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the dataBaixa
     */
    public Date getDataBaixa() {
        return dataBaixa;
    }

    /**
     * @param dataBaixa the dataBaixa to set
     */
    public void setDataBaixa(Date dataBaixa) {
        this.dataBaixa = dataBaixa;
    }

    /**
     * @return the dataMovimentacao
     */
    public Date getDataMovimentacao() {
        return dataMovimentacao;
    }

    /**
     * @param dataMovimentacao the dataMovimentacao to set
     */
    public void setDataMovimentacao(Date dataMovimentacao) {
        this.dataMovimentacao = dataMovimentacao;
    }

    /**
     * @return the tipoBaixa
     */
    public TipoBaixa getTipoBaixa() {
        return tipoBaixa;
    }

    /**
     * @param tipoBaixa the tipoBaixa to set
     */
    public void setTipoBaixa(TipoBaixa tipoBaixa) {
        this.tipoBaixa = tipoBaixa;
    }

    /**
     * @return the competencia
     */
    public Competencia getCompetencia() {
        return competencia;
    }

    /**
     * @param competencia the competencia to set
     */
    public void setCompetencia(Competencia competencia) {
        this.competencia = competencia;
    }

    /**
     * @return the responsavel
     */
    public String getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(String responsavel) {
        this.responsavel = responsavel;
    }

    /**
     * @return the localizacaoDestino
     */
    public Localizacao getLocalizacaoDestino() {
        return localizacaoDestino;
    }

    /**
     * @param localizacaoDestino the localizacaoDestino to set
     */
    public void setLocalizacaoDestino(Localizacao localizacaoDestino) {
        this.localizacaoDestino = localizacaoDestino;
    }

    /**
     * @return the localizacaoOrigem
     */
    public Localizacao getLocalizacaoOrigem() {
        return localizacaoOrigem;
    }

    /**
     * @param localizacaoOrigem the localizacaoOrigem to set
     */
    public void setLocalizacaoOrigem(Localizacao localizacaoOrigem) {
        this.localizacaoOrigem = localizacaoOrigem;
    }

}
