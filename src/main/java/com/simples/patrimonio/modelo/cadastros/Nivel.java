/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author sigfran
 */
@Entity(name = "patrimonio.tb_nivel")
@Table(schema = "patrimonio")
public class Nivel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada"))
    private EmpresaLicenciada empresaLicenciada;
    
    @NotBlank
    private String descricao;
    
    @NotNull
    private Integer quantidadeDigitos;
    
    private Integer grau;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the empresaLicenciada
     */
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    /**
     * @param empresaLicenciada the empresaLicenciada to set
     */
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the quantidadeDigitos
     */
    public Integer getQuantidadeDigitos() {
        return quantidadeDigitos;
    }

    /**
     * @param quantidadeDigitos the quantidadeDigitos to set
     */
    public void setQuantidadeDigitos(Integer quantidadeDigitos) {
        this.quantidadeDigitos = quantidadeDigitos;
    }
    
    
     @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Nivel other = (Nivel) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return descricao;
    }

    /**
     * @return the grau
     */
    public Integer getGrau() {
        return grau;
    }

    /**
     * @param grau the grau to set
     */
    public void setGrau(Integer grau) {
        this.grau = grau;
    }
}
