/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.util.PlanoDeContaEnum;
import com.simples.util.Utils;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author sigfran
 */
@Entity(name = "patrimonio.tb_plano_conta")
@Table(schema = "patrimonio")
public class PlanoDeConta implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotBlank
    @Size(max = 100)
    private String codigo;
    
    @NotBlank
    @Size(max = 1)
    private String escritura;
    
    @NotBlank
    @Size(max = 1)
    private String natureza;
    
    @NotBlank
    @Size(max = 100)
    private String titulo;
    
    @Size(max = 4000)
    private String observacao;
    
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada"))
    private EmpresaLicenciada empresaLicenciada;
 
    private Integer vidaUtil;

    private Integer taxaAnualDepreciacao;

    public Integer getVidaUtil() {
        return vidaUtil;
    }

    public void setVidaUtil(Integer vidaUtil) {
        this.vidaUtil = vidaUtil;
    }

    public Integer getTaxaAnualDepreciacao() {
        return taxaAnualDepreciacao;
    }

    public void setTaxaAnualDepreciacao(Integer taxaAnualDepreciacao) {
        this.taxaAnualDepreciacao = taxaAnualDepreciacao;
    }
    
    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the escritura
     */
    public String getEscritura() {
        return escritura;
    }

    /**
     * @param escritura the escritura to set
     */
    public void setEscritura(String escritura) {
        this.escritura = escritura;
    }

    /**
     * @return the natureza
     */
    public String getNatureza() {
        return natureza;
    }

    /**
     * @param natureza the natureza to set
     */
    public void setNatureza(String natureza) {
        this.natureza = natureza;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the observacao
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * @param observacao the observacao to set
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /**
     * @return the empresaLicenciada
     */
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    /**
     * @param empresaLicenciada the empresaLicenciada to set
     */
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final PlanoDeConta other = (PlanoDeConta) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Utils.format(PlanoDeContaEnum.getMascara(codigo.length()), codigo) + " - " + titulo;

    }

  

}
