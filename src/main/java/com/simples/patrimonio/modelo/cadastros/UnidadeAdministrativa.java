package com.simples.patrimonio.modelo.cadastros;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.simples.controleacesso.modelo.EmpresaLicenciada;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author sigfran
 */
//@Entity(name = "patrimonio.tb_unidade_administrativa")
//@Table(schema = "patrimonio")
public class UnidadeAdministrativa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotBlank
    private String cnpj;
    
    private String sigla;
    
    @NotBlank
    @Size(max = 700)
    private String nome;
    
    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada"))
    private EmpresaLicenciada empresaLicenciada;

    public UnidadeAdministrativa() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 43 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final UnidadeAdministrativa other = (UnidadeAdministrativa) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nome;
    }

    /**
     * @return the empresaLicenciada
     */
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    /**
     * @param empresaLicenciada the empresaLicenciada to set
     */
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    /**
     * @return the sigla
     */
    public String getSigla() {
        return sigla;
    }

    /**
     * @param sigla the sigla to set
     */
    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

}
