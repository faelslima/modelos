/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.controleacesso;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.patrimonio.modelo.cadastros.Competencia;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author sigfran
 */
@Entity(name = "patrimonio.ti_controle_aplicacao")
@Table(schema = "patrimonio")
public class ControleAplicacao implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
    private Usuario usuario;
    
    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(foreignKey = @ForeignKey(name = "fk_competencia_id"))
    private Competencia competencia;
    
    @NotBlank
    private String aplicacao;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date ultimoAcesso;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "empresa_licenciada_id", foreignKey = @ForeignKey(name = "fk_empresa_licenciada_id"))
    private EmpresaLicenciada empresaLicenciada;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public Usuario getUsuario() {
        return usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public Competencia getCompetencia() {
        return competencia;
    }
    
    public void setCompetencia(Competencia competencia) {
        this.competencia = competencia;
    }
    
    public String getAplicacao() {
        return aplicacao;
    }
    
    public void setAplicacao(String aplicacao) {
        this.aplicacao = aplicacao;
    }
    
    public Date getUltimoAcesso() {
        return ultimoAcesso;
    }
    
    public void setUltimoAcesso(Date ultimoAcesso) {
        this.ultimoAcesso = ultimoAcesso;
    }
    
    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }
    
    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals and hashCode()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 43 * hash + Objects.hashCode(this.id);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ControleAplicacao other = (ControleAplicacao) obj;
        return Objects.equals(this.id, other.id);
    }
    //</editor-fold>

}
