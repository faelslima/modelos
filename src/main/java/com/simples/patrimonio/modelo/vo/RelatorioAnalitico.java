/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.vo;


import com.simples.patrimonio.modelo.cadastros.Localizacao;
import com.simples.patrimonio.modelo.cadastros.Responsavel;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author sigfran
 */
public class RelatorioAnalitico {
    private String codigo;
    private String descricao;
    private Date dataAquisicao;
    private String identificacao;
    private BigDecimal valor;
    private BigDecimal valorDepreciado;
    private Localizacao localizacao;
    private Responsavel responsavel;

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * @return the dataAquisicao
     */
    public Date getDataAquisicao() {
        return dataAquisicao;
    }

    /**
     * @param dataAquisicao the dataAquisicao to set
     */
    public void setDataAquisicao(Date dataAquisicao) {
        this.dataAquisicao = dataAquisicao;
    }

    /**
     * @return the identificacao
     */
    public String getIdentificacao() {
        return identificacao;
    }

    /**
     * @param identificacao the identificacao to set
     */
    public void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }

    /**
     * @return the valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    /**
     * @return the valorDepreciado
     */
    public BigDecimal getValorDepreciado() {
        return valorDepreciado;
    }

    /**
     * @param valorDepreciado the valorDepreciado to set
     */
    public void setValorDepreciado(BigDecimal valorDepreciado) {
        this.valorDepreciado = valorDepreciado;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    public Responsavel getResponsavel() {
        return responsavel;
    }

    public void setResponsavel(Responsavel responsavel) {
        this.responsavel = responsavel;
    }
}
 