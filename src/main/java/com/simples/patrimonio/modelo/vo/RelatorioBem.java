/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.vo;

import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.patrimonio.modelo.cadastros.Competencia;
import com.simples.patrimonio.modelo.cadastros.Fornecedor;
import com.simples.patrimonio.modelo.cadastros.Localizacao;
import com.simples.patrimonio.modelo.cadastros.Responsavel;
import com.simples.patrimonio.modelo.cadastros.TipoIdentificacaoBem;
import com.simples.util.Constantes;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Temporal;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sigfran
 */
public class RelatorioBem {
  
    private String descricao;
    private int situacao = 0;
    private String numeroProcesso;
    private String observacao;
    private Empresa empresa;
    @NotNull
    private Responsavel responsavel;
    private Competencia competencia;
    private Fornecedor fornecedor;
    private BigDecimal valorOriginal;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataAquisicao;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataDepreciacao;
    private String numeroNotaFiscal;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataBaixa;
    private Localizacao localizacao;
    private TipoIdentificacaoBem tipoIdentificacaoBem;
    private int plaqueta;
    private String placa;
    private String chassi;
    private String matricula;
    @Size(max = 30)
    private String numeroEmpenho;
    @Size(max = 100)
    private String projetoAtividade;
    @Size(max = 100)
    private String elementoDespesa;
    @Size(max = 100)
    private String fonteRecurso;
    
     public String getIdentificacao() {
        String identificacao = "";
        if (placa != null && !placa.equals("")) {
            identificacao = placa;
        }
        if (matricula != null && !matricula.equals("")) {
            identificacao = matricula;
        }
        if (chassi != null && !chassi.equals("")) {
            identificacao = chassi;
        }
        if (plaqueta > 0) {
            identificacao = Integer.toString(plaqueta);
        }
        return identificacao;
    }

    /**
     * @return the descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * @param descricao the descricao to set
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

  

    /**
     * @return the numeroProcesso
     */
    public String getNumeroProcesso() {
        return numeroProcesso;
    }

    /**
     * @param numeroProcesso the numeroProcesso to set
     */
    public void setNumeroProcesso(String numeroProcesso) {
        this.numeroProcesso = numeroProcesso;
    }

    /**
     * @return the observacao
     */
    public String getObservacao() {
        return observacao;
    }

    /**
     * @param observacao the observacao to set
     */
    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    /**
     * @return the empresa
     */
    public Empresa getEmpresa() {
        return empresa;
    }

    /**
     * @param empresa the empresa to set
     */
    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    /**
     * @return the responsavel
     */
    public Responsavel getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(Responsavel responsavel) {
        this.responsavel = responsavel;
    }

    /**
     * @return the competencia
     */
    public Competencia getCompetencia() {
        return competencia;
    }

    /**
     * @param competencia the competencia to set
     */
    public void setCompetencia(Competencia competencia) {
        this.competencia = competencia;
    }

    /**
     * @return the fornecedor
     */
    public Fornecedor getFornecedor() {
        return fornecedor;
    }

    /**
     * @param fornecedor the fornecedor to set
     */
    public void setFornecedor(Fornecedor fornecedor) {
        this.fornecedor = fornecedor;
    }

    /**
     * @return the valorOriginal
     */
    public BigDecimal getValorOriginal() {
        return valorOriginal;
    }

    /**
     * @param valorOriginal the valorOriginal to set
     */
    public void setValorOriginal(BigDecimal valorOriginal) {
        this.valorOriginal = valorOriginal;
    }

    /**
     * @return the dataAquisicao
     */
    public Date getDataAquisicao() {
        return dataAquisicao;
    }

    /**
     * @param dataAquisicao the dataAquisicao to set
     */
    public void setDataAquisicao(Date dataAquisicao) {
        this.dataAquisicao = dataAquisicao;
    }

    /**
     * @return the dataDepreciacao
     */
    public Date getDataDepreciacao() {
        return dataDepreciacao;
    }

    /**
     * @param dataDepreciacao the dataDepreciacao to set
     */
    public void setDataDepreciacao(Date dataDepreciacao) {
        this.dataDepreciacao = dataDepreciacao;
    }

    /**
     * @return the numeroNotaFiscal
     */
    public String getNumeroNotaFiscal() {
        return numeroNotaFiscal;
    }

    /**
     * @param numeroNotaFiscal the numeroNotaFiscal to set
     */
    public void setNumeroNotaFiscal(String numeroNotaFiscal) {
        this.numeroNotaFiscal = numeroNotaFiscal;
    }

    /**
     * @return the dataBaixa
     */
    public Date getDataBaixa() {
        return dataBaixa;
    }

    /**
     * @param dataBaixa the dataBaixa to set
     */
    public void setDataBaixa(Date dataBaixa) {
        this.dataBaixa = dataBaixa;
    }

   

    /**
     * @return the tipoIdentificacaoBem
     */
    public TipoIdentificacaoBem getTipoIdentificacaoBem() {
        return tipoIdentificacaoBem;
    }

    /**
     * @param tipoIdentificacaoBem the tipoIdentificacaoBem to set
     */
    public void setTipoIdentificacaoBem(TipoIdentificacaoBem tipoIdentificacaoBem) {
        this.tipoIdentificacaoBem = tipoIdentificacaoBem;
    }

    /**
     * @return the plaqueta
     */
    public int getPlaqueta() {
        return plaqueta;
    }

    /**
     * @param plaqueta the plaqueta to set
     */
    public void setPlaqueta(int plaqueta) {
        this.plaqueta = plaqueta;
    }

    /**
     * @return the placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * @param placa the placa to set
     */
    public void setPlaca(String placa) {
        this.placa = placa;
    }

    /**
     * @return the chassi
     */
    public String getChassi() {
        return chassi;
    }

    /**
     * @param chassi the chassi to set
     */
    public void setChassi(String chassi) {
        this.chassi = chassi;
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * @return the numeroEmpenho
     */
    public String getNumeroEmpenho() {
        return numeroEmpenho;
    }

    /**
     * @param numeroEmpenho the numeroEmpenho to set
     */
    public void setNumeroEmpenho(String numeroEmpenho) {
        this.numeroEmpenho = numeroEmpenho;
    }

    /**
     * @return the projetoAtividade
     */
    public String getProjetoAtividade() {
        return projetoAtividade;
    }

    /**
     * @param projetoAtividade the projetoAtividade to set
     */
    public void setProjetoAtividade(String projetoAtividade) {
        this.projetoAtividade = projetoAtividade;
    }

    /**
     * @return the elementoDespesa
     */
    public String getElementoDespesa() {
        return elementoDespesa;
    }

    /**
     * @param elementoDespesa the elementoDespesa to set
     */
    public void setElementoDespesa(String elementoDespesa) {
        this.elementoDespesa = elementoDespesa;
    }

    /**
     * @return the fonteRecurso
     */
    public String getFonteRecurso() {
        return fonteRecurso;
    }

    /**
     * @param fonteRecurso the fonteRecurso to set
     */
    public void setFonteRecurso(String fonteRecurso) {
        this.fonteRecurso = fonteRecurso;
    }

    /**
     * @return the situacao
     */
    public int getSituacao() {
        return situacao;
    }

    /**
     * @param situacao the situacao to set
     */
    public void setSituacao(int situacao) {
        this.situacao = situacao;
    }

    /**
     * @return the localizacao
     */
    public Localizacao getLocalizacao() {
        return localizacao;
    }

    /**
     * @param localizacao the localizacao to set
     */
    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }
}
