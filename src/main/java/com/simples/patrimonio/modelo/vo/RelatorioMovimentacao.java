/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.vo;

import com.simples.patrimonio.modelo.cadastros.Movimentacao;
import com.simples.util.DateUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author sigfran
 */
public class RelatorioMovimentacao extends Movimentacao implements Serializable {

    private int tipoRelatorio = 1;
    private String nomeEmpresa;
    private Date dataMovimentacaoInicial;
    private Date dataMovimentacaoFinal;
    private Integer totalDiasMovimentacao;
    private boolean todasCompetencias;
    private String nomeResponsavel;
    private String telefoneResponsavel;
    private List<Movimentacao> movimentacao = new ArrayList<Movimentacao>();

    /**
     * @return the tipoRelatorio
     */
    public int getTipoRelatorio() {
        return tipoRelatorio;
    }

    /**
     * @param tipoRelatorio the tipoRelatorio to set
     */
    public void setTipoRelatorio(int tipoRelatorio) {
        this.tipoRelatorio = tipoRelatorio;
    }

    /**
     * @return the nomeEmpresa
     */
    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    /**
     * @param nomeEmpresa the nomeEmpresa to set
     */
    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    /**
     * @return the dataMovimentacaoInicial
     */
    public Date getDataMovimentacaoInicial() {
        return dataMovimentacaoInicial;
    }

    /**
     * @param dataMovimentacaoInicial the dataMovimentacaoInicial to set
     */
    public void setDataMovimentacaoInicial(Date dataMovimentacaoInicial) {
        this.dataMovimentacaoInicial = dataMovimentacaoInicial;
    }

    /**
     * @return the dataMovimentacaoFinal
     */
    public Date getDataMovimentacaoFinal() {
        return dataMovimentacaoFinal;
    }

    /**
     * @param dataMovimentacaoFinal the dataMovimentacaoFinal to set
     */
    public void setDataMovimentacaoFinal(Date dataMovimentacaoFinal) {
        this.dataMovimentacaoFinal = dataMovimentacaoFinal;
    }

    /**
     * @return the todasCompetencias
     */
    public boolean isTodasCompetencias() {
        return todasCompetencias;
    }

    /**
     * @param todasCompetencias the todasCompetencias to set
     */
    public void setTodasCompetencias(boolean todasCompetencias) {
        this.todasCompetencias = todasCompetencias;
    }

    /**
     * @return the nomeResponsavel
     */
    public String getNomeResponsavel() {
        return nomeResponsavel;
    }

    /**
     * @param nomeResponsavel the nomeResponsavel to set
     */
    public void setNomeResponsavel(String nomeResponsavel) {
        this.nomeResponsavel = nomeResponsavel;
    }

    /**
     * @return the telefoneResponsavel
     */
    public String getTelefoneResponsavel() {
        return telefoneResponsavel;
    }

    /**
     * @param telefoneResponsavel the telefoneResponsavel to set
     */
    public void setTelefoneResponsavel(String telefoneResponsavel) {
        this.telefoneResponsavel = telefoneResponsavel;
    }

    /**
     * @return the movimentacao
     */
    public List<Movimentacao> getMovimentacao() {
        return movimentacao;
    }

    /**
     * @param movimentacao the movimentacao to set
     */
    public void setMovimentacao(List<Movimentacao> movimentacao) {
        this.movimentacao = movimentacao;
    }

    public Integer getTotalDiasMovimentacao() {
        if (dataMovimentacaoInicial != null && dataMovimentacaoFinal != null) {
            totalDiasMovimentacao = DateUtils.getTotalDiasEntreDatas(dataMovimentacaoInicial, dataMovimentacaoFinal);
        }
        return totalDiasMovimentacao;
    }

    public void setTotalDiasMovimentacao(Integer totalDiasMovimentacao) {
        this.totalDiasMovimentacao = totalDiasMovimentacao;
    }

}
