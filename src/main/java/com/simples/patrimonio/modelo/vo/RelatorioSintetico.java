/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.patrimonio.modelo.vo;

import com.simples.patrimonio.modelo.cadastros.Localizacao;
import com.simples.patrimonio.modelo.cadastros.PlanoDeConta;
import com.simples.patrimonio.modelo.cadastros.Responsavel;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author sigfran
 */
public class RelatorioSintetico implements Serializable{
    private String codigo;
    private String titulo;
    private Integer quantidadeCompetenciaAtual;
    private Integer quantidadeCompetenciaAnterior;
    private Integer saldoQuantidade;
    private BigDecimal valorCompetenciaAtual;
    private BigDecimal valorCompetenciaAnterior;
    private BigDecimal saldoValor;
    private Localizacao localizacao;
    private Responsavel responsavel;
    private PlanoDeConta planoDeConta;

    /**
     * @return the codigo
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the titulo
     */
    public String getTitulo() {
        return titulo;
    }

    /**
     * @param titulo the titulo to set
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    /**
     * @return the quantidadeCompetenciaAtual
     */
    public Integer getQuantidadeCompetenciaAtual() {
        return quantidadeCompetenciaAtual;
    }

    /**
     * @param quantidadeCompetenciaAtual the quantidadeCompetenciaAtual to set
     */
    public void setQuantidadeCompetenciaAtual(Integer quantidadeCompetenciaAtual) {
        this.quantidadeCompetenciaAtual = quantidadeCompetenciaAtual;
    }

    /**
     * @return the quantidadeCompetenciaAnterior
     */
    public Integer getQuantidadeCompetenciaAnterior() {
        return quantidadeCompetenciaAnterior;
    }

    /**
     * @param quantidadeCompetenciaAnterior the quantidadeCompetenciaAnterior to set
     */
    public void setQuantidadeCompetenciaAnterior(Integer quantidadeCompetenciaAnterior) {
        this.quantidadeCompetenciaAnterior = quantidadeCompetenciaAnterior;
    }

    /**
     * @return the saldoQuantidade
     */
    public Integer getSaldoQuantidade() {
        return saldoQuantidade;
    }

    /**
     * @param saldoQuantidade the saldoQuantidade to set
     */
    public void setSaldoQuantidade(Integer saldoQuantidade) {
        this.saldoQuantidade = saldoQuantidade;
    }

    /**
     * @return the valorCompetenciaAtual
     */
    public BigDecimal getValorCompetenciaAtual() {
        return valorCompetenciaAtual;
    }

    /**
     * @param valorCompetenciaAtual the valorCompetenciaAtual to set
     */
    public void setValorCompetenciaAtual(BigDecimal valorCompetenciaAtual) {
        this.valorCompetenciaAtual = valorCompetenciaAtual;
    }

    /**
     * @return the valorCompetenciaAnterior
     */
    public BigDecimal getValorCompetenciaAnterior() {
        return valorCompetenciaAnterior;
    }

    /**
     * @param valorCompetenciaAnterior the valorCompetenciaAnterior to set
     */
    public void setValorCompetenciaAnterior(BigDecimal valorCompetenciaAnterior) {
        this.valorCompetenciaAnterior = valorCompetenciaAnterior;
    }

    /**
     * @return the saldoValor
     */
    public BigDecimal getSaldoValor() {
        return saldoValor;
    }

    /**
     * @param saldoValor the saldoValor to set
     */
    public void setSaldoValor(BigDecimal saldoValor) {
        this.saldoValor = saldoValor;
    }

    public Localizacao getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(Localizacao localizacao) {
        this.localizacao = localizacao;
    }

    /**
     * @return the responsavel
     */
    public Responsavel getResponsavel() {
        return responsavel;
    }

    /**
     * @param responsavel the responsavel to set
     */
    public void setResponsavel(Responsavel responsavel) {
        this.responsavel = responsavel;
    }

    /**
     * @return the planoDeConta
     */
    public PlanoDeConta getPlanoDeConta() {
        return planoDeConta;
    }

    /**
     * @param planoDeConta the planoDeConta to set
     */
    public void setPlanoDeConta(PlanoDeConta planoDeConta) {
        this.planoDeConta = planoDeConta;
    }

    

  
}
