/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.protocolo.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.util.Utils;
import java.io.Serializable;
import java.util.Locale;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

/**
 * @author Rafael Lima
 */
@Entity(name = "protocolo.tb_cargo")
@Table(schema = "protocolo", uniqueConstraints = @UniqueConstraint(columnNames = {"empresa_licenciada_id", "descricao"}))
public class Cargo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @OneToOne
    @JoinColumn(name = "empresa_licenciada_id")
    private EmpresaLicenciada empresaLicenciada;
    
    @Size(min = 3, max = 60)
    private String descricao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresa) {
        this.empresaLicenciada = empresa;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao.toUpperCase(new Locale("pt", "BR"));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 89 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cargo other = (Cargo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Utils.getCapitular(descricao);
    }
}
