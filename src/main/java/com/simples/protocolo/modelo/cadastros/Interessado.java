/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.protocolo.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.util.Utils;
import java.io.Serializable;
import java.util.Locale;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

/**
 *
 * @author rafael
 */
@Entity(name = "protocolo.tb_interessado")
@Table(schema = "protocolo", uniqueConstraints = @UniqueConstraint(columnNames = {"empresa_licenciada_id", "cpf_cnpj", "nome"}))
public class Interessado implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tipo_pessoa", columnDefinition = "VARCHAR(2) DEFAULT 'F'")
    private String tipoPessoa;

    @Size(min = 2, max = 150, message = "O nome do interessado deve conter no mínimo 2 caracteres.")
    private String nome;

    @Size(max = 14, message = "Verifique se o CPF/CNPJ foi digitado corretamente.")
    @Column(name = "cpf_cnpj")
    private String cpfCnpj;

    @OneToOne
    @JoinColumn(name = "empresa_licenciada_id")
    private EmpresaLicenciada empresaLicenciada;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(String tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome.toUpperCase(new Locale("pt", "BR"));
    }

    public String getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(String cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresa) {
        this.empresaLicenciada = empresa;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Interessado other = (Interessado) obj;
        return !(this.id != other.id && (this.id == null || !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return Utils.getCapitular(nome);
    }

}
