/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.protocolo.modelo.cadastros;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.util.Utils;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author rafael
 */
@Entity(name = "protocolo.tb_orgao")
@Table(schema = "protocolo", uniqueConstraints = @UniqueConstraint(columnNames = {"empresa_licenciada_id", "nome_orgao"}))
public class Orgao implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotBlank
    @Size(min = 2, max = 20)
    private String sigla;
    
    @Size(min = 2, max = 100, message = "O nome do órgão deve estar entre 3 e 100 caracteres.")
    @Column(name = "nome_orgao")
    private String nomeOrgao;
    
    @Column(length = 14)
    private String cnpj;
    
    @Column(columnDefinition = "boolean DEFAULT true")
    private boolean status = true;
    
    @NotNull(message = "Informe uma empresa para cadastrar o Órgão")
    @OneToOne
    @JoinColumn(name = "empresa_licenciada_id")
    private EmpresaLicenciada empresaLicenciada;
    
    @OneToMany(mappedBy = "orgao", targetEntity = Setor.class)    
    private List<Setor> listaSetores = new LinkedList();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla.toUpperCase(new Locale("pt", "BR"));
    }

    public String getNomeOrgao() {
        return nomeOrgao;
    }

    public void setNomeOrgao(String nomeOrgao) {
        this.nomeOrgao = nomeOrgao.toUpperCase(new Locale("pt", "BR"));
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public List<Setor> getListaSetores() {
        return listaSetores;
    }

    public void setListaSetores(List<Setor> listaSetores) {
        this.listaSetores = listaSetores;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresa) {
        this.empresaLicenciada = empresa;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Orgao other = (Orgao) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return Utils.getCapitular(nomeOrgao);
    }
}
