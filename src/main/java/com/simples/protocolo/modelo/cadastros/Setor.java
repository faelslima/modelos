/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.protocolo.modelo.cadastros;

import com.simples.protocolo.modelo.controleacesso.Usuario;
import com.simples.protocolo.modelo.processo.Processo;
import com.simples.util.Utils;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author rafael
 */
@Entity(name = "protocolo.tb_setor")
@Table(schema = "protocolo", uniqueConstraints = @UniqueConstraint(columnNames = {"orgao_id", "descricao"}))
public class Setor implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull(message = "Órgão não pode ser vazio.")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "orgao_id")
    private Orgao orgao;
    
    @Size(min = 2, max = 20, message = "A sigla deve estar entre 2 e 20 caracteres.")
    private String sigla;
    
    @Size(min = 2, max = 200, message = "A descrição do setor deve estar entre 2 e 200 caracteres.")
    private String descricao;
    
    private boolean situacao = true;
    
    @Transient
    private String orgaoSetor;
    
    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "setor_id")
    private List<Processo> listaProcessos = new LinkedList();
    
    @OrderBy(value = "nome")
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Usuario.class)
    private List<Usuario> usuarios;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Orgao getOrgao() {
        return orgao;
    }

    public void setOrgao(Orgao orgao) {
        this.orgao = orgao;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla.toUpperCase();
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao.toUpperCase();
    }

    public boolean isSituacao() {
        return situacao;
    }

    public void setSituacao(boolean situacao) {
        this.situacao = situacao;
    }

    public List<Processo> getListaProcessos() {
        return listaProcessos;
    }

    public void setListaProcessos(List<Processo> listaProcessos) {
        this.listaProcessos = listaProcessos;
    }

    public String getOrgaoSetor() {
        if(orgao != null) {
            orgaoSetor = orgao.getNomeOrgao() + " - " + Utils.getCapitular(descricao);
        } else {
            orgaoSetor = descricao;
        }
        return orgaoSetor;
    }
    
    public String getSiglaOrgaoSetor() {
        if(orgao != null) {
            orgaoSetor = orgao.getSigla() + " - " + Utils.getCapitular(descricao);
        } else {
            orgaoSetor = Utils.getCapitular(descricao);
        }
        return orgaoSetor;
    }

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Setor other = (Setor) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return sigla.toUpperCase(new Locale("pt", "BR")) + " - " + Utils.getCapitular(descricao);
    }    
}
