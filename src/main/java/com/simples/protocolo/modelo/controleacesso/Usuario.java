/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.protocolo.modelo.controleacesso;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.controleacesso.modelo.Perfil;
import com.simples.controleacesso.modelo.UsuarioEmail;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.protocolo.modelo.cadastros.Cargo;
import com.simples.protocolo.modelo.cadastros.Setor;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 *
 * @author Rafael
 */
@Entity(name = "protocolo.tb_usuario")
@Table(schema = "protocolo")
@PrimaryKeyJoinColumn(name = "id", foreignKey = @ForeignKey(name = "fk_usuario_id"))
public class Usuario extends com.simples.controleacesso.modelo.Usuario implements Serializable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "setor_id", foreignKey = @ForeignKey(name = "fk_setor_id"))
    private Setor setor;
    
    @Size(max = 10)
    private String matricula;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cargo_id", foreignKey = @ForeignKey(name = "fk_cargo_id"))
    private Cargo cargo;
    
    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Setor getSetor() {
        return setor;
    }
    
    public void setSetor(Setor setor) {
        this.setor = setor;
    }
    
    public String getMatricula() {
        return matricula;
    }
    
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
    
    public Cargo getCargo() {
        return cargo;
    }
    
    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }
    
    @Override
    public String getUserLogin() {
        return super.getUserLogin();
    }
    
    @Override
    public List<UsuarioEmail> getEmails() {
        return super.getEmails();
    }
    
//    @Override
//    public List<Perfil> getPerfis() {
//        return super.getPerfis();
//    }

    @Override
    public EmpresaLicenciada getEmpresaLicenciada() {
        return super.getEmpresaLicenciada();
    }

    @Override
    public Empresa getEmpresa() {
        return super.getEmpresa();
    }

//    @Override
//    public List<EmpresaLicenciada> getEmpresasLicenciadas() {
//        return super.getEmpresasLicenciadas();
//    }

    @Override
    public List<Empresa> getEmpresasFinanceiro() {
        return super.getEmpresasFinanceiro();
    }

    @Override
    public boolean isSuperUsuario() {
        return super.isSuperUsuario();
    }
    
    @Override
    public String getNome() {
        return super.getNome();
    }
    //</editor-fold>

    @Override
    public String toString() {
        return super.toString();
    }
}
