/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.simples.protocolo.modelo.processo;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.protocolo.modelo.cadastros.Setor;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;

/**
 * @author Rafael Lima
 */
@Entity(name = "protocolo.tb_arquivamento")
@Table(schema = "protocolo")
public class Arquivamento implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @OneToOne
    @JoinColumn(name = "processo_id")
    private Processo processo;
    
    @OneToOne
    @JoinColumn(name = "setor_arquivamento_id")
    private Setor setorArquivamento;
    
    @OneToOne
    @JoinColumn(name = "empresa_licenciada_id")
    private EmpresaLicenciada empresaLicenciada;
    
    @OneToOne
    @JoinColumn(name = "usuario_arquivamento_id")
    private Usuario usuarioArquivamento;
    
    @OneToOne
    @JoinColumn(name = "usuario_desarquivamento_id")
    private Usuario usuarioDesarquivamento;
    
    @Size(min=5, max=600, message = "Informe um motivo.")
    @Column(name = "motivo_arquivamento", columnDefinition = "text not null")
    private String motivoArquivamento;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_arquivamento")
    private Date dataArquivamento;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_desarquivamento")
    private Date dataDesarquivamento;
    
    @Size(max=600, message = "Motivo deve ser até 600 caracteres.")
    @Column(name = "motivo_desarquivamento", columnDefinition = "text")
    private String motivoDesarquivamento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public Setor getSetorArquivamento() {
        return setorArquivamento;
    }

    public void setSetorArquivamento(Setor setorArquivamento) {
        this.setorArquivamento = setorArquivamento;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresa) {
        this.empresaLicenciada = empresa;
    }

    public Usuario getUsuarioArquivamento() {
        return usuarioArquivamento;
    }

    public void setUsuarioArquivamento(Usuario usuarioArquivamento) {
        this.usuarioArquivamento = usuarioArquivamento;
    }

    public Usuario getUsuarioDesarquivamento() {
        return usuarioDesarquivamento;
    }

    public void setUsuarioDesarquivamento(Usuario usuarioDesarquivamento) {
        this.usuarioDesarquivamento = usuarioDesarquivamento;
    }

    public String getMotivoArquivamento() {
        return motivoArquivamento;
    }

    public void setMotivoArquivamento(String motivoArquivamento) {
        this.motivoArquivamento = motivoArquivamento;
    }

    public Date getDataArquivamento() {
        return dataArquivamento;
    }

    public void setDataArquivamento(Date dataArquivamento) {
        this.dataArquivamento = dataArquivamento;
    }

    public Date getDataDesarquivamento() {
        return dataDesarquivamento;
    }

    public void setDataDesarquivamento(Date dataDesarquivamento) {
        this.dataDesarquivamento = dataDesarquivamento;
    }

    public String getMotivoDesarquivamento() {
        return motivoDesarquivamento;
    }

    public void setMotivoDesarquivamento(String motivoDesarquivamento) {
        this.motivoDesarquivamento = motivoDesarquivamento;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Arquivamento other = (Arquivamento) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
