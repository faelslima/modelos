/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.protocolo.modelo.processo;

import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.protocolo.modelo.cadastros.Interessado;
import com.simples.protocolo.modelo.cadastros.Natureza;
import com.simples.protocolo.modelo.cadastros.Setor;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;


/**
 * @author rafael
 */
@Entity(name = "protocolo.tb_processo")
@Table(schema = "protocolo", uniqueConstraints = @UniqueConstraint(columnNames = {"empresa_licenciada_id", "numero_processo", "ano_processo"}))
public class Processo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "empresa_licenciada_id")
    private EmpresaLicenciada empresaLicenciada;

    @NotNull(message = "Informe o interessado.")
    @ManyToOne
    @JoinColumn(name = "interessado_id")
    private Interessado interessado = new Interessado();

    @NotNull(message = "Informe a natureza do processo.")
    @ManyToOne
    @JoinColumn(name = "natureza_id")
    private Natureza natureza = new Natureza();

    @ManyToOne
    @JoinColumn(name = "setor_id")
    private Setor setor = new Setor();

    @OneToOne
    @JoinColumn(name = "usuario_id")
    private Usuario usuario = new Usuario();

    @NotNull
    @Column(name = "numero_processo", length = 6)
    private Integer numeroProcesso;

    @NotNull
    @Column(name = "ano_processo", length = 4)
    private Integer anoProcesso;

    @NotNull
    @Column(length = 150)
    private String titulo;

    @NotNull
    @Column(columnDefinition = "text")
    private String assunto;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_cadastro")
    private Date dataCadastro;

    private Integer volumes = 1;
    private Integer paginas = 2;

    @OneToMany(mappedBy = "processo", targetEntity = Tramite.class)
    @OrderBy(value = "dataHoraRecebimento desc")
    private List<Tramite> listaTramites;
    
    @OneToMany(mappedBy = "processo", targetEntity = Arquivamento.class)
    @OrderBy(value = "dataDesarquivamento desc, dataArquivamento desc")
    private List<Arquivamento> listaArquivamentos;

    @Column(name = "observacao_arquivamento", columnDefinition = "text")
    private String observacaoArquivamento;

    //<editor-fold defaultstate="collapsed" desc="Getter and Setter">
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Interessado getInteressado() {
        return interessado;
    }

    public void setInteressado(Interessado interessado) {
        this.interessado = interessado;
    }

    public Natureza getNatureza() {
        return natureza;
    }

    public void setNatureza(Natureza natureza) {
        this.natureza = natureza;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Integer getNumeroProcesso() {
        return numeroProcesso;
    }

    public void setNumeroProcesso(Integer numeroProcesso) {
        this.numeroProcesso = numeroProcesso;
    }

    public Integer getAnoProcesso() {
        return anoProcesso;
    }

    public void setAnoProcesso(Integer anoProcesso) {
        this.anoProcesso = anoProcesso;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAssunto() {
        return assunto;
    }

    public void setAssunto(String assunto) {
        this.assunto = assunto;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public EmpresaLicenciada getEmpresaLicenciada() {
        return empresaLicenciada;
    }

    public void setEmpresaLicenciada(EmpresaLicenciada empresaLicenciada) {
        this.empresaLicenciada = empresaLicenciada;
    }

    public Setor getSetor() {
        return setor;
    }

    public void setSetor(Setor setor) {
        this.setor = setor;
    }

    public Integer getVolumes() {
        return volumes;
    }

    public void setVolumes(Integer volumes) {
        this.volumes = volumes;
    }

    public Integer getPaginas() {
        return paginas;
    }

    public void setPaginas(Integer paginas) {
        this.paginas = paginas;
    }

    public List<Tramite> getListaTramites() {
        return listaTramites;
    }

    public void setListaTramites(List<Tramite> listaTramites) {
        this.listaTramites = listaTramites;
    }

    public List<Arquivamento> getListaArquivamentos() {
        return listaArquivamentos;
    }

    public void setListaArquivamentos(List<Arquivamento> listaArquivamentos) {
        this.listaArquivamentos = listaArquivamentos;
    }

    public String getObservacaoArquivamento() {
        return observacaoArquivamento;
    }

    public void setObservacaoArquivamento(String observacaoArquivamento) {
        this.observacaoArquivamento = observacaoArquivamento;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="equals(), hashCode() and toString()">
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Processo other = (Processo) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Processo " + numeroProcesso + "/" + anoProcesso;
    }
    //</editor-fold>

}
