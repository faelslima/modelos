/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.protocolo.modelo.processo;

import com.simples.controleacesso.modelo.Usuario;
import com.simples.protocolo.modelo.cadastros.Setor;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * @author Rafael Lima
 */
@Entity(name = "protocolo.tb_tramite")
@Table(schema = "protocolo")
public class Tramite implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @NotNull(message = "Informe o processo a ser tramitado.")
    @ManyToOne
    @JoinColumn(name = "processo_id")
    private Processo processo;
    
    @NotNull
    @ManyToOne
    @JoinColumn(name = "setor_origem_id")
    private Setor setorOrigem;
    
    @NotNull(message = "Informe o setor para onde o processo será encaminhado.")
    @ManyToOne
    @JoinColumn(name = "setor_recebimento_id")
    private Setor setorRecebimento;
    
    @OneToOne
    @JoinColumn(name = "usuario_origem_id")
    private Usuario usuarioOrigem;
    
    @OneToOne
    @JoinColumn(name = "usuario_recebimento_id")
    private Usuario usuarioRecebimento;
    
    @OneToOne
    @JoinColumn(name = "usuario_cancelamento_id")
    private Usuario usuarioCancelamento;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_envio")
    private Date dataHoraEnvio;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_recebimento")
    private Date dataHoraRecebimento;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "data_hora_cancelamento")
    private Date dataHoraCancelamento;
    
    @NotNull(message = "Informe a quantidade de páginas do processo.")
    @Column(name = "qtd_paginas")
    private Integer quantidadePaginas;
    
    @Column(columnDefinition = "text")
    private String observacao;
    
    @Column(columnDefinition = "boolean default false")
    private boolean cancelado;
    
    @Column(name = "motivo_cancelamento", columnDefinition = "text")
    private String motivoCancelamento;

    @Transient
    private String processoRecebido;
    
    @Transient
    private String situacaoCancelamento;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Processo getProcesso() {
        return processo;
    }

    public void setProcesso(Processo processo) {
        this.processo = processo;
    }

    public Setor getSetorOrigem() {
        return setorOrigem;
    }

    public void setSetorOrigem(Setor setorOrigem) {
        this.setorOrigem = setorOrigem;
    }

    public Setor getSetorRecebimento() {
        return setorRecebimento;
    }

    public void setSetorRecebimento(Setor setorRecebimento) {
        this.setorRecebimento = setorRecebimento;
    }

    public Usuario getUsuarioOrigem() {
        return usuarioOrigem;
    }

    public void setUsuarioOrigem(Usuario usuarioOrigem) {
        this.usuarioOrigem = usuarioOrigem;
    }

    public Usuario getUsuarioRecebimento() {
        return usuarioRecebimento;
    }

    public void setUsuarioRecebimento(Usuario usuarioRecebimento) {
        this.usuarioRecebimento = usuarioRecebimento;
    }

    public Usuario getUsuarioCancelamento() {
        return usuarioCancelamento;
    }

    public void setUsuarioCancelamento(Usuario usuarioCancelamento) {
        this.usuarioCancelamento = usuarioCancelamento;
    }

    public Date getDataHoraEnvio() {
        return dataHoraEnvio;
    }

    public void setDataHoraEnvio(Date dataHoraEnvio) {
        this.dataHoraEnvio = dataHoraEnvio;
    }

    public Date getDataHoraRecebimento() {
        return dataHoraRecebimento;
    }

    public void setDataHoraRecebimento(Date dataHoraRecebimento) {
        this.dataHoraRecebimento = dataHoraRecebimento;
    }

    public Date getDataHoraCancelamento() {
        return dataHoraCancelamento;
    }

    public void setDataHoraCancelamento(Date dataHoraCancelamento) {
        this.dataHoraCancelamento = dataHoraCancelamento;
    }

    public Integer getQuantidadePaginas() {
        return quantidadePaginas;
    }

    public void setQuantidadePaginas(Integer quantidadePaginas) {
        this.quantidadePaginas = quantidadePaginas;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public boolean isCancelado() {
        return cancelado;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }

    public String getMotivoCancelamento() {
        return motivoCancelamento;
    }

    public void setMotivoCancelamento(String motivoCancelamento) {
        this.motivoCancelamento = motivoCancelamento;
    }

    public String getProcessoRecebido() {
        if (dataHoraRecebimento != null) {
            processoRecebido = "Sim";
        } else {
            processoRecebido = "Não";
        }
        return processoRecebido;
    }
    
    public String getSituacaoCancelamento() {
        situacaoCancelamento = isCancelado() ? "Cancelado" : "Não";
        return situacaoCancelamento;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (this.id != null ? this.id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tramite other = (Tramite) obj;
        if (this.id != other.id && (this.id == null || !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Trâmite " + processo;
    }
}
