/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.rest.modelo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * @author Rafael Lima
 */
public class LogSyncDTO implements Serializable {

    @SerializedName(value = "DataHora")
    private Date dataHora;

    @SerializedName(value = "Usuario")
    private String usuario;

    @SerializedName(value = "Tipo")
    private String tipo;

    @SerializedName(value = "Subtipo")
    private String subtipo;

    @SerializedName(value = "Historico")
    private String historico;

    @SerializedName(value = "Sistema")
    private String sistema;

    @SerializedName(value = "VersaoSistema")
    private String versaoSistema;

    @SerializedName(value = "VersaoBanco")
    private String versaoBanco;

    @SerializedName(value = "EstacaoSerialHd")
    private String estacaoSerialHd;

    @SerializedName(value = "EstacaoSistemaOperacional")
    private String estacaoSistemaOperacional;
    
    @SerializedName(value = "EstacaoVariaveisAmbiente")
    private String estacaoVariaveisAmbiente;

    @SerializedName(value = "EstacaoIp")
    private String estacaoIp;

    @SerializedName(value = "EstacaoHostname")
    private String estacaoHostname;

    @SerializedName(value = "LicencaEmpresaCnpj")
    private String licencaEmpresaCnpj;

    @SerializedName(value = "LicencaEmpresaRazaoSocial")
    private String licencaEmpresaRazaoSocial;

    @SerializedName(value = "LicencaPeriodoLicenciado")
    private String licencaPeriodoLicenciado;

    @SerializedName(value = "LicencaCompetenciaInicial")
    private String licencaCompetenciaInicial;

    @SerializedName(value = "LicencaCompetenciaFinal")
    private String licencaCompetenciaFinal;

    @SerializedName(value = "SyncDataHora")
    private Date syncDataHora;

    @SerializedName(value = "SyncDataHoraCliente")
    private Date syncDataHoraCliente;

    @SerializedName(value = "SyncQtdTentativas")
    private Integer syncQtdTentativas;

    @SerializedName(value = "SyncHistoricoTentativas")
    private String syncHistoricoTentativas;

    @SerializedName(value = "Erro")
    private String erro;

    @SerializedName(value = "Modulo")
    private String modulo;

    @SerializedName(value = "PathBanco")
    private String pathBanco;

    @SerializedName(value = "VersaoSistemaBuild")
    private Integer versaoSistemaBuild;

    @SerializedName(value = "EstacaoUsername")
    private String estacaoUsername;

    public Date getDataHora() {
        return dataHora;
    }

    public void setDataHora(Date dataHora) {
        this.dataHora = dataHora;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getSubtipo() {
        return subtipo;
    }

    public void setSubtipo(String subtipo) {
        this.subtipo = subtipo;
    }

    public String getHistorico() {
        return historico;
    }

    public void setHistorico(String historico) {
        this.historico = historico;
    }

    public String getSistema() {
        return sistema;
    }

    public void setSistema(String sistema) {
        this.sistema = sistema;
    }

    public String getVersaoSistema() {
        return versaoSistema;
    }

    public void setVersaoSistema(String versaoSistema) {
        this.versaoSistema = versaoSistema;
    }

    public String getVersaoBanco() {
        return versaoBanco;
    }

    public void setVersaoBanco(String versaoBanco) {
        this.versaoBanco = versaoBanco;
    }

    public String getEstacaoSerialHd() {
        return estacaoSerialHd;
    }

    public void setEstacaoSerialHd(String estacaoSerialHd) {
        this.estacaoSerialHd = estacaoSerialHd;
    }

    public String getEstacaoVariaveisAmbiente() {
        return estacaoVariaveisAmbiente;
    }

    public void setEstacaoVariaveisAmbiente(String estacaoVariaveisAmbiente) {
        this.estacaoVariaveisAmbiente = estacaoVariaveisAmbiente;
    }

    public String getEstacaoSistemaOperacional() {
        return estacaoSistemaOperacional;
    }

    public void setEstacaoSistemaOperacional(String estacaoSistemaOperacional) {
        this.estacaoSistemaOperacional = estacaoSistemaOperacional;
    }

    public String getEstacaoIp() {
        return estacaoIp;
    }

    public void setEstacaoIp(String estacaoIp) {
        this.estacaoIp = estacaoIp;
    }

    public String getEstacaoHostname() {
        return estacaoHostname;
    }

    public void setEstacaoHostname(String estacaoHostname) {
        this.estacaoHostname = estacaoHostname;
    }

    public String getLicencaEmpresaCnpj() {
        return licencaEmpresaCnpj;
    }

    public void setLicencaEmpresaCnpj(String licencaEmpresaCnpj) {
        this.licencaEmpresaCnpj = licencaEmpresaCnpj;
    }

    public String getLicencaEmpresaRazaoSocial() {
        return licencaEmpresaRazaoSocial;
    }

    public void setLicencaEmpresaRazaoSocial(String licencaEmpresaRazaoSocial) {
        this.licencaEmpresaRazaoSocial = licencaEmpresaRazaoSocial;
    }

    public String getLicencaPeriodoLicenciado() {
        return licencaPeriodoLicenciado;
    }

    public void setLicencaPeriodoLicenciado(String licencaPeriodoLicenciado) {
        this.licencaPeriodoLicenciado = licencaPeriodoLicenciado;
    }

    public String getLicencaCompetenciaInicial() {
        return licencaCompetenciaInicial;
    }

    public void setLicencaCompetenciaInicial(String licencaCompetenciaInicial) {
        this.licencaCompetenciaInicial = licencaCompetenciaInicial;
    }

    public String getLicencaCompetenciaFinal() {
        return licencaCompetenciaFinal;
    }

    public void setLicencaCompetenciaFinal(String licencaCompetenciaFinal) {
        this.licencaCompetenciaFinal = licencaCompetenciaFinal;
    }

    public Date getSyncDataHora() {
        return syncDataHora;
    }

    public void setSyncDataHora(Date syncDataHora) {
        this.syncDataHora = syncDataHora;
    }

    public Date getSyncDataHoraCliente() {
        return syncDataHoraCliente;
    }

    public void setSyncDataHoraCliente(Date syncDataHoraCliente) {
        this.syncDataHoraCliente = syncDataHoraCliente;
    }

    public Integer getSyncQtdTentativas() {
        return syncQtdTentativas;
    }

    public void setSyncQtdTentativas(Integer syncQtdTentativas) {
        this.syncQtdTentativas = syncQtdTentativas;
    }

    public String getSyncHistoricoTentativas() {
        return syncHistoricoTentativas;
    }

    public void setSyncHistoricoTentativas(String syncHistoricoTentativas) {
        this.syncHistoricoTentativas = syncHistoricoTentativas;
    }

    public String getErro() {
        return erro;
    }

    public void setErro(String erro) {
        this.erro = erro;
    }

    public String getModulo() {
        return modulo;
    }

    public void setModulo(String modulo) {
        this.modulo = modulo;
    }

    public String getPathBanco() {
        return pathBanco;
    }

    public void setPathBanco(String pathBanco) {
        this.pathBanco = pathBanco;
    }

    public Integer getVersaoSistemaBuild() {
        return versaoSistemaBuild;
    }

    public void setVersaoSistemaBuild(Integer versaoSistemaBuild) {
        this.versaoSistemaBuild = versaoSistemaBuild;
    }

    public String getEstacaoUsername() {
        return estacaoUsername;
    }

    public void setEstacaoUsername(String estacaoUsername) {
        this.estacaoUsername = estacaoUsername;
    }
    
    //<editor-fold defaultstate="collapsed" desc="Gson">
    private static Gson getGson() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Date.class, new DateSerializer());
        builder.registerTypeAdapter(Date.class, new DateDeserializer());
        return builder.create();
    }
    
    public static String toJson(LogSyncDTO log) {
        return getGson().toJson(log);
    }
    
    public static LogSyncDTO fromJson(String json) {
        return getGson().fromJson(json, LogSyncDTO.class);
    }
    
    /**
     * Converte uma Date em um valor ISO 8601.
     *
     * @param data Date a ser convertido em String.
     * @return String com a data. Ex: "2013-01-17T00:00:00.124-02:00"
     */
    public static String dateToJson(Date data) {
        if (data == null) {
            return null;
        }
        // O toString do Joda DateTime por padrão retorna um String de data em ISO 8601.
        DateTime dt = new DateTime(data, DateTimeZone.forID("-03:00"));
        return dt.toString().replace("-03:00", "Z");
    }
    
    /**
     * Coverte uma data ISO 8601 em DateTime.
     *
     * @param strDt
     * @return Date Ex: "2013-01-17T00:00:00.124-02:00"
     */
    public static Date jsonToDate(String strDt) {
        if (strDt == null) {
            return null;
        }
        DateTime dt = new DateTime(strDt);
        return dt.toDate();
    }
    
    /**
     * Serializer de Date.
     */
    private static class DateSerializer implements JsonSerializer<Date> {
        @Override
        public JsonElement serialize(Date t, Type type, JsonSerializationContext jsc) {
            return new JsonPrimitive(dateToJson(t));
        }
    }
    
    /**
     * Deserializer de Date.
     */
    private static class DateDeserializer implements JsonDeserializer<Date> {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return jsonToDate(json.getAsJsonPrimitive().getAsString());
        }
    }
    //</editor-fold>
    
}
