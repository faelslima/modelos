package com.simples.util;

/**
 *
 * @author Ayslan
 */
public class Constantes {

    /**
     * Tipo para String sem tamanho definido.
     *
     * Postgres: text Oracle: clob MySQL: longtext (ou text)
     *
     */
    public static final String TIPO_TEXTO_BANCO = "text";
    public static final String URL_XDATA_LOGSERVICE = "https://xdata.simplesi.com.br/logservice/xlogservice/log";
    public static final String URL_XDATA_USUARIO_CREDENCIAMENTO = "https://xdata.simplesi.com.br/login/loginservice/SolicitarCadastro";
    public static final String URL_APLICACAO = Utils.getUrlBase();
    public static final int MINUTOS_VALIDADE_RECUPERACAO_SENHA = 30;

    private Constantes() {
    }
}
