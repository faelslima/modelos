package com.simples.util;

/**
 *
 * @author Ayslan
 */
public class ConstantesPatrimonio {

    public static final String PERSISTENCE_UNIT_NAME = "simples-patrimonioPU";
    /**
     * Tipo para String sem tamanho definido. Postgres: text Oracle: clob MySQL:
     * longtext
     */
    
    public static final String NOME_APLICACAO = "patrimonio";
    public static final String DATASOURCE_NAME = "jdbc/simples-patrimonio";
    public static final String JAVASESSION_MAIL = "mail/simplesSmtp";
//    public static final String DATASOURCE_NAME = "java:/simples-patrimonio";
//    public static final String JAVASESSION_MAIL = "java:jboss/mail/simplesSmtp";
    public static final String TIPO_TEXTO_BANCO = "text";
    public static final int MINUTOS_VALIDADE_RECUPERACAO_SENHA = 30;
    public static final Long TRANSFERENCIA = 1L;
    public static final Long MANUTENCAO = 2L;
    public static final Long BAIXA = 3L;
    public static final int TODOS_BENS = 0;
    public static final int BENS_ATIVOS = 1;
    public static final int BENS_INATIVOS = 2;
    public static final String COMPETENCIA_ABERTA = "A";
    public static final String COMPETENCIA_FECHADA = "F";
    public static final String CADASTRO_LOG_AUTENTICACAO = "A";
    public static final String CADASTRO_LOG_MUDANCA_COMPETENCIA = "C";
    public static final String MENSAL = "M";
    public static final String ANUAL = "A";
            

    private ConstantesPatrimonio() {
    }
}
