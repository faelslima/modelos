/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * @author Rafael Lima
 */
public class DateUtils {

    public static int getTotalMesesEntreDatas(Date dataInicial, Date dataFinal) {
        Calendar dataInicio = Calendar.getInstance(new Locale("pt", "BR"));
        Calendar dataFim = Calendar.getInstance(new Locale("pt", "BR"));

        dataInicio.setTime(dataInicial.compareTo(dataFinal) <= 0 ? dataInicial : dataFinal);
        dataFim.setTime(dataFinal.compareTo(dataInicial) >= 0 ? dataFinal : dataInicial);

        return getTotalMesesEntreDatas(dataInicio, dataFim);
    }
    
    public static int getTotalMesesEntreDatas(Calendar dataInicio, Calendar dataFim) {
        
        int totalMesesEntreDatas = 0;
        int diaMesDataInicio = dataInicio.get(Calendar.DAY_OF_MONTH);
        
        if(diaMesDataInicio > 28) {
            dataInicio.set(Calendar.DAY_OF_MONTH, 28);
        }
        
        while(!(dataInicio.get(Calendar.MONTH) == dataFim.get(Calendar.MONTH) &&
                dataInicio.get(Calendar.YEAR) == dataFim.get(Calendar.YEAR))) {
           dataInicio.add(Calendar.MONTH, 1);
           totalMesesEntreDatas++;
        }

        return totalMesesEntreDatas;
    }

    public static int getTotalDiasEntreDatas(Date dataInicial, Date dataFinal) {
        Calendar dataInicio = Calendar.getInstance(new Locale("pt", "BR"));
        Calendar dataFim = Calendar.getInstance(new Locale("pt", "BR"));

        dataInicio.setTime(dataInicial.compareTo(dataFinal) <= 0 ? dataInicial : dataFinal);
        dataFim.setTime(dataFinal.compareTo(dataInicial) >= 0 ? dataFinal : dataInicial);

        long totalEmMilissegundos = dataFim.getTimeInMillis() - dataInicio.getTimeInMillis();
        long tempoEmDia = 1000 * 60 * 60 * 24;
        long calculoEntreDias = totalEmMilissegundos / tempoEmDia;

        int retorno = (int) calculoEntreDias;
        return retorno >= 0 ? retorno : retorno * (-1);
    }

    public static String getNomeMes(java.util.Date data) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
        return sdf.format(data);
    }

    public static String getNomeMesAtual() {
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
        return sdf.format(new Date());
    }

    public static String getNomeMesAtual(Locale locale) {
        if (locale == null) {
            locale = Locale.getDefault();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("MMMM", locale);
        return sdf.format(new Date());
    }

    public static String getDataHora(java.util.Date dataHora) {
        return getDate(dataHora, "dd/MM/yyyy HH:mm:ss");
    }

    public static String getHora(java.util.Date hora) {
        return getDate(hora, "HH:mm:ss");
    }

    public static String getDate(java.util.Date data) {
        return getDate(data, "dd/MM/yyyy");
    }

    public static String getDate(java.util.Date data, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(data);
    }

    public static java.util.Date getDate(String data) throws ParseException {

        if (data != null && !data.isEmpty() && data.length() >= 8) {

            data = data.trim();
            char ch1 = data.charAt(0);
            char ch2 = data.charAt(2);
            boolean isNumeric = ch1 >= 48 && ch1 <= 57 && ch2 >= 48 && ch2 <= 57;

            if (isNumeric) {
                SimpleDateFormat sdf;

                if (data.length() == 15) {
                    sdf = new SimpleDateFormat("yyyyMMdd HHmmss");
                    return sdf.parse(data);

                } else if (data.length() >= 19 && !data.contains("/") && !data.contains("-") && !data.contains(":")) {
                    sdf = new SimpleDateFormat("yyyyMMdd HHmmss");
                    return sdf.parse(data);

                } else if (data.length() >= 19 && data.contains(":")) {
                    if (data.contains("/")) {
                        sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        return sdf.parse(data);

                    } else if (data.contains("-")) {
                        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        return sdf.parse(data);
                    }

                } else if (data.length() >= 19 && data.contains("/")) {
                    sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    return sdf.parse(data);

                } else if (data.length() >= 19 && data.contains("-")) {
                    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    return sdf.parse(data);

                } else if (data.length() == 10 && data.contains("/")) {
                    sdf = new SimpleDateFormat("yyyy/MM/dd");
                    return sdf.parse(data);

                } else if (data.length() == 10 && data.contains("-")) {
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    return sdf.parse(data);

                } else if (data.length() == 8) {
                    sdf = new SimpleDateFormat("yyyyMMdd");
                    return sdf.parse(data);
                }
            }
        }
        return null;
    }

    public static String getFullDate(java.util.Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd 'de' MMMMM 'de' yyyy");
        return sdf.format(date);
    }

    public static String getFullDate(String date) throws ParseException {
        Date data = getDate(date);
        return getFullDate(data);
    }
}
