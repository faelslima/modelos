            /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.util;

/**
 *
 * @author sigfran
 */
public enum PlanoDeContaEnum {

    M1("#"),
    M2("#.#"),
    M3("#.#.#"),
    M4("#.#.#.#"),
    M5("#.#.#.#.#"),
    M7("#.#.#.#.#.##"),
    M9("#.#.#.#.#.##.##"),
    M11("#.#.#.#.#.##.##.##"),
    M13("#.#.#.#.#.##.##.##.##"),
    M15("#.#.#.#.#.##.##.##.##.##"),
    M17("#.#.#.#.#.##.##.##.##.##.##");
    
    private String descricao;

    private PlanoDeContaEnum(String descricao) {
        this.descricao = descricao;
    }

    public static String getMascara(int tamanho) {
        for (PlanoDeContaEnum plano : PlanoDeContaEnum.values()) {
            if (plano.name().equals("M"+tamanho)) {
                return plano.descricao;
            }
        }
        return "";
    }

}
