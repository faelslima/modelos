/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.util;

import com.simples.controleacesso.modelo.Aplicacao;
import com.simples.controleacesso.modelo.EmpresaLicenciada;
import com.simples.controleacesso.modelo.Usuario;
import com.simples.financeiro.modelo.administracao.Empresa;
import com.simples.financeiro.modelo.cadastro.Cliente;
import com.simples.financeiro.modelo.cadastro.UsuarioCliente;
import com.xpert.faces.utils.FacesUtils;

/**
 *
 * @author Rafael
 */
public class SessaoUtils {

    /**
     * @return o Usuario logado na aplicacao, ou seja o usuario setado no
     * #{sessaoUsuarioMB.user}
     */
    public static Usuario getUser() {
        try {
            return (Usuario) FacesUtils.getBeanByEl("#{sessaoUsuarioMB.user}");
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * @return o Cliente logado na aplicacao, ou seja o cliente setado no
     * #{sessaoUsuarioMB.usuarioCliente}
     */
    public static UsuarioCliente getUsuarioCliente() {
        try {
            return (UsuarioCliente) FacesUtils.getBeanByEl("#{sessaoUsuarioMB.usuarioCliente}");
        } catch (Exception ex) {
            return null;
        }
    }

    public static boolean isSuperUsuario() {
        return getUser() != null ? getUser().isSuperUsuario() : false;
    }

    public static Empresa getEmpresa() {
        if (getUser() != null) {
            return getUser().getEmpresa();
        }
        return null;
    }

    public static EmpresaLicenciada getEmpresaLicenciada() {
        return getUser() != null ? getUser().getEmpresaLicenciada() : null;
    }

    public static Aplicacao getAplicacaoLogada() {
        try {
            return (Aplicacao) FacesUtils.getBeanByEl("#{sessaoUsuarioMB.aplicacao}");
        } catch (Exception ex) {
            return null;
        }
    }

    public static String getAmbienteAplicacao() {
        try {
            return (String) FacesUtils.getBeanByEl("#{sessaoUsuarioMB.ambienteAplicacao}");
        } catch (Exception ex) {
            return null;
        }
    }
}
