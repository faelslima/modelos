/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.simples.util;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

/**
 *
 * @author Commons-lang
 */
public class StringUtils {

    public static String lpad(String valueToPad, String filler, int size) {
        while (valueToPad.length() < size) {
            valueToPad = filler + valueToPad;
        }
        return valueToPad;
    }

    public static String rpad(String valueToPad, String filler, int size) {
        while (valueToPad.length() < size) {
            valueToPad = valueToPad + filler;
        }
        return valueToPad;
    }

    public static String getRealPath(String path) {
        String realPath = getServletContext().getRealPath(path);
        return path.endsWith("/") && realPath != null ? realPath + "/" : realPath;
    }

    public static ServletContext getServletContext() {
        ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
        return (ServletContext) externalContext.getContext();
    }

    public static boolean verificaImagem(String path) {
        File arq = new File(path);
        return arq.exists();
    }
}