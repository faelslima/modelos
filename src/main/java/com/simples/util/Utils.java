package com.simples.util;

import com.xpert.faces.utils.FacesUtils;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Asynchronous;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.swing.ImageIcon;
import javax.swing.text.MaskFormatter;
import org.apache.commons.io.IOUtils;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

public class Utils {

    public static String format(String pattern, Object value) {
        MaskFormatter mask;
        try {
            mask = new MaskFormatter(pattern);
            mask.setValueContainsLiteralCharacters(false);
            return mask.valueToString(value);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String limitaString(String string, int tamanho) {
        if (string != null && string.length() > tamanho) {
            return string.substring(0, tamanho + 1);
        } else {
            return string;
        }
    }

    public static String formatarCpfCnpj(String cpfcnpj) {
        StringBuilder cpfcnpjFormatado = new StringBuilder(cpfcnpj == null ? "" : cpfcnpj);

        if (cpfcnpj != null && cpfcnpj.length() == 11) {
            cpfcnpjFormatado.insert(9, '-');
            cpfcnpjFormatado.insert(6, '.');
            cpfcnpjFormatado.insert(3, '.');

        } else if (cpfcnpj != null && cpfcnpj.length() == 14) {
            cpfcnpjFormatado.insert(12, '-');
            cpfcnpjFormatado.insert(8, '/');
            cpfcnpjFormatado.insert(5, '.');
            cpfcnpjFormatado.insert(2, '.');

        } else if (cpfcnpj != null && cpfcnpj.length() == 12) {
            cpfcnpjFormatado.insert(10, '-');
            cpfcnpjFormatado.insert(6, '.');
            cpfcnpjFormatado.insert(3, '.');
        }
        return cpfcnpjFormatado.toString();
    }

    /**
     * Formata um CEP complementando com zeros a esquerda, caso ele tenha um
     * tamanho igual a 8. Formato: 99.999-999.
     *
     * @param cep O CEP (deve conter apenas digitos).
     *
     * @return O CEP formatado.
     */
    public static String formatarCep(String cep) {
        StringBuilder cepFormatado = new StringBuilder(cep == null ? "" : cep);
        if (cep != null && cep.length() == 8) {
            cepFormatado.insert(5, '-');
            cepFormatado.insert(2, '.');
        }
        return cepFormatado.toString();
    }

    public static String formatarTelefone(String telefone) {
        StringBuilder telefoneFormatado = new StringBuilder(telefone == null ? "" : telefone);
        if (telefone != null && telefone.length() == 10) {
            telefoneFormatado.insert(0, '(');
            telefoneFormatado.insert(3, ')');
            telefoneFormatado.insert(8, '-');
        }
        return telefoneFormatado.toString();
    }

    public static String formatarParaMonetario(BigDecimal valor) {
        if (valor != null) {
            return NumberFormat.getCurrencyInstance(new Locale("pt", "BR")).format(valor).replace("R$ ", "");
        }
        return null;
    }

    public static String removeSpecialChars(String s) {

        s = removeAspasSimples(s);

        return s.replaceAll("\\.", "")
                .replaceAll("\\-", "")
                .replaceAll("\\;", "")
                .replaceAll("\\,", "")
                .replaceAll("\\'", "")
                .replaceAll("\\_", "")
                .replaceAll("\\(", "")
                .replaceAll("\\)", "")
                .replaceAll("\\\\", "")
                .replaceAll("\\/", "")
                .replaceAll("\\:", "")
                .trim();

    }

    public static String removeAspasSimples(String palavra) {

        if (!palavra.trim().isEmpty()) {
            palavra = palavra.trim();
            if (palavra.startsWith("'") && palavra.endsWith("'")) {
                return palavra.substring(1, palavra.length() - 1);
            } else if (palavra.startsWith("'")) {
                return palavra.substring(1, palavra.length());
            } else if (palavra.endsWith("'")) {
                return palavra.substring(0, palavra.length() - 1);
            }
        }
        return palavra;
    }

    /**
     * Recebe uma string qualquer como parâmetro e modifica a string em
     * capitular. Ex.: maria da silva (modifica para) Maria da Silva
     *
     * @param string
     * @return
     */
    public static String getCapitular(String string) {
        String stringModificada = "";

        if (string != null && string.length() > 0) {
            String minusculo = string.toLowerCase();
            String[] quebraEspacos = minusculo.split(" ");

            for (String s : quebraEspacos) {
                if (s.length() == 2 || s.length() == 3) {
                    if (!s.equals("da") && !s.equals("de") && !s.equals("do") && !s.equals("dos") && !s.equals("das") && !s.equals("os") && !s.equals("as")) {
                        stringModificada += String.valueOf(s.charAt(0)).toUpperCase() + s.substring(1, s.length()) + " ";
                    } else {
                        stringModificada += s + " ";
                    }

                } else if (s.length() > 2) {
                    stringModificada += String.valueOf(s.charAt(0)).toUpperCase() + s.substring(1, s.length()) + " ";

                } else if (s.length() == 1) {
                    stringModificada += String.valueOf(s.charAt(0)) + " ";
                }
            }
        }
        return stringModificada.trim();
    }

    public static String removeAcentuacao(String string) {
        if (string != null && !string.trim().isEmpty()) {
            string = string.replaceAll("[ÁÀÂÃÄ]", "A");
            string = string.replaceAll("[ÉÈÊË]", "E");
            string = string.replaceAll("[ÍÌÎÏ]", "I");
            string = string.replaceAll("[ÓÒÔÕÖ]", "O");
            string = string.replaceAll("[ÚÙÛÜ]", "U");
            string = string.replaceAll("[Ç]", "C");

            string = string.replaceAll("[áàâãä]", "a");
            string = string.replaceAll("[éèêë]", "e");
            string = string.replaceAll("[íìîï]", "i");
            string = string.replaceAll("[óòôõö]", "o");
            string = string.replaceAll("[úùûü]", "u");
            string = string.replaceAll("[ç]", "c");
        }
        return string;
    }

    public static Date getDate(String data) throws ParseException {

        if (data != null && !data.isEmpty() && data.length() >= 8) {

            data = data.trim();
            char ch1 = data.charAt(0);
            char ch2 = data.charAt(2);
            boolean isNumeric = ch1 >= 48 && ch1 <= 57 && ch2 >= 48 && ch2 <= 57;

            if (isNumeric) {
                SimpleDateFormat sdf;

                if (data.length() == 15) {
                    sdf = new SimpleDateFormat("yyyyMMdd HHmmss");
                    return sdf.parse(data);

                } else if (data.length() >= 19 && !data.contains("/") && !data.contains("-") && !data.contains(":")) {
                    sdf = new SimpleDateFormat("yyyyMMdd HHmmss");
                    return sdf.parse(data);

                } else if (data.length() >= 19 && data.contains(":")) {
                    if (data.contains("/")) {
                        sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                        return sdf.parse(data);

                    } else if (data.contains("-")) {
                        sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        return sdf.parse(data);
                    }

                } else if (data.length() >= 19 && data.contains("/")) {
                    sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    return sdf.parse(data);

                } else if (data.length() >= 19 && data.contains("-")) {
                    sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    return sdf.parse(data);

                } else if (data.length() == 10 && data.contains("/")) {
                    sdf = new SimpleDateFormat("yyyy/MM/dd");
                    return sdf.parse(data);

                } else if (data.length() == 10 && data.contains("-")) {
                    sdf = new SimpleDateFormat("yyyy-MM-dd");
                    return sdf.parse(data);

                } else if (data.length() == 8) {
                    sdf = new SimpleDateFormat("yyyyMMdd");
                    return sdf.parse(data);
                }
            }
        }
        return null;
    }

    public static String getFullDate(Date date) {
//        SimpleDateFormat sdf = new java.text.SimpleDateFormat("EEEEE, dd 'de' MMMMM 'de' yyyy", new Locale("pt", "BR"));
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd 'de' MMMMM 'de' yyyy", new Locale("pt", "BR"));
        return sdf.format(date);
    }

    public static String getFullDate(String date) throws ParseException {
        Date data = getDate(date);
        return getFullDate(data);
    }

    public static String getUrlBase() {
        String Url = FacesUtils.getRequest().getRequestURL().toString();
        String contextPath = FacesUtils.getServletContext().getContextPath();
        String url[] = Url.split(contextPath);

        String urlBase;
        String[] splitUrl = Url.split("\\/");
        if (splitUrl.length >= 4) {
            urlBase = splitUrl[0] + "//" + splitUrl[2] + contextPath;
            
        } else {
            urlBase = url[0] + contextPath;
        }
        return urlBase;
    }

    public static String getUrlBase(String complemento) {
        if (complemento.startsWith("/")) {
            return getUrlBase() + complemento;
        } else {
            return getUrlBase() + "/" + complemento;
        }
    }

    public static String realPath(String path) {
        String realPath = FacesUtils.getServletContext().getRealPath(path);
        return path.endsWith("/") && realPath != null ? realPath + "/" : realPath;
    }

    public static String getCurrentView() {
        String Url = FacesUtils.getRequest().getRequestURL().toString();
        String url[] = Url.split("/");
        return url[url.length - 1];
    }

    public static Map<String, String> getRequestHeaders() {
        Map<String, String> listaConteudosHeaders = new HashMap();
        HttpServletRequest request = FacesUtils.getRequest();
        if (request != null) {
            Enumeration<String> headers = request.getHeaderNames();
            while (headers.hasMoreElements()) {
                String str = headers.nextElement();
                listaConteudosHeaders.put(str, request.getHeader(str));
            }
            listaConteudosHeaders.put("authType", request.getAuthType());
            listaConteudosHeaders.put("characterEncoding", request.getCharacterEncoding());
            listaConteudosHeaders.put("contentType", request.getContentType());
            listaConteudosHeaders.put("contextPath", request.getContextPath());
            listaConteudosHeaders.put("localAddr", request.getLocalAddr());
            listaConteudosHeaders.put("localName", request.getLocalName());
            listaConteudosHeaders.put("method", request.getMethod());
            listaConteudosHeaders.put("pathInfo", request.getPathInfo());
            listaConteudosHeaders.put("pathTranslated", request.getPathTranslated());
            listaConteudosHeaders.put("protocol", request.getProtocol());
            listaConteudosHeaders.put("queryString", request.getQueryString());
            listaConteudosHeaders.put("remoteAddr", request.getRemoteAddr());
            listaConteudosHeaders.put("remoteHost", request.getRemoteHost());
            listaConteudosHeaders.put("remoteUser", request.getRemoteUser());
            listaConteudosHeaders.put("requestUri", request.getRequestURI());
            listaConteudosHeaders.put("scheme", request.getScheme());
            listaConteudosHeaders.put("serverName", request.getServerName());
            listaConteudosHeaders.put("servletPath", request.getServletPath());
        }
        return listaConteudosHeaders;
    }

    public static Integer getInteger(String numero) {
        numero = removeSpecialChars(numero);
        boolean isNumeric = true;

        if (!numero.isEmpty()) {
            for (int x = 0; x < numero.length(); x++) {
                char ch = numero.charAt(x);
                if (!(ch >= 48 && ch <= 57)) {
                    isNumeric = false;
                    break;
                }
            }
            return isNumeric ? Integer.valueOf(numero) : 0;

        } else {
            return 0;
        }
    }

    public static String getCompetencia(String competencia) {
        if (competencia != null && competencia.length() == 6) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM");
            try {
                Date dataCompetencia = sdf.parse(competencia);
                sdf = new SimpleDateFormat("MM/yyyy");
                competencia = sdf.format(dataCompetencia);

            } catch (ParseException ex) {
                Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return competencia;
    }

    public static boolean fileExists(String path) {
        File file = new File(path);
        return file.exists();
    }

    public static Image getImage(InputStream imageInputStream) throws IOException {
        return getImage(IOUtils.toByteArray(imageInputStream));
    }

    public static Image getImage(byte[] byteImagem) {
        ImageIcon imagem = null;
        if (byteImagem != null) {
            imagem = new ImageIcon(byteImagem);
        }
        return imagem != null ? imagem.getImage() : null;
    }

    public static StreamedContent getImageStreamedContent(byte[] imagem) {
        return getImageStreamedContent(new ByteArrayInputStream(imagem));
    }

    public static StreamedContent getImageStreamedContent(InputStream imageInputStream) {
        if (imageInputStream != null) {
            return new DefaultStreamedContent(imageInputStream);
        }
        return null;
    }

    public static String getPartFileName(Part part) {
        String header = part.getHeader("content-disposition");
        for (String headerPart : header.split(";")) {
            if (headerPart.trim().startsWith("filename")) {
                return headerPart.substring(headerPart.indexOf('=') + 1).trim()
                        .replace("\"", "");
            }
        }
        return null;
    }

    public static byte[] getByteArray(InputStream inputStream) {
        try {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static byte[] getByteArray(Part part) {
        try {
            return getByteArray(part.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static byte[] getByteArray(String texto) {
        return texto.getBytes();
    }

    public static String convertToUtf8(String string) {
        return new String(string.getBytes(StandardCharsets.UTF_8));
    }

    //MÉTODO GET PARA RECEBER UMA STRING JSON
    @Asynchronous
    public static String getJson(String urlRest) {
        try {
            URL url = new URL(urlRest);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Accept", "application/json");

            //200 É O CÓDIGO QUE INDICA QUE A CONEXÃO FOI BEM SUCEDIDA
            if (con.getResponseCode() != 200) {
                throw new RuntimeException("HTTP error code : " + con.getResponseCode());
            }
            BufferedReader br = new BufferedReader(new InputStreamReader((con.getInputStream())));
            String linha;
            String retorno = "";
            while ((linha = br.readLine()) != null) {
                retorno += linha;
            }
            con.disconnect();
            return retorno;
        } catch (MalformedURLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

    //MÉTODO POST PARA ENVIAR UMA STRING JSON
    @Asynchronous
    public static String[] postJson(String urlRest, String json) {
        try {
            URL url = new URL(urlRest);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");

            OutputStream os = conn.getOutputStream();
            os.write(json.getBytes());
            os.flush();

            Integer responseCode = conn.getResponseCode();
            String responseMessage = conn.getResponseMessage();
            if (responseCode >= 400) {
                BufferedReader br = new BufferedReader(new InputStreamReader((conn.getErrorStream())));
                String output;
                String retorno = "Falha ao enviar JSON:\nHTTP error code : " + responseCode + " \nResponse Message: " + responseMessage + "\n ";
                while ((output = br.readLine()) != null) {
                    retorno += output;
                }
                conn.disconnect();
                return new String[]{"ERRO", retorno};
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
            String output;
            String retorno = "";
            while ((output = br.readLine()) != null) {
                retorno += output;
            }
            conn.disconnect();
            return new String[]{"SUCESSO", "HTTP code " + responseCode + " \n " + retorno};

        } catch (MalformedURLException e) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);

        } catch (IOException e) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public static String getEntityName(Class entity) {
        String name;
        Table table = (Table) entity.getAnnotation(Table.class);
        if (table != null && table.name() != null && !table.name().isEmpty()) {
            name = table.name();
        } else {
            Entity entityAnnotation = (Entity) entity.getAnnotation(Entity.class);
            if (entityAnnotation != null && entityAnnotation.name() != null && !entityAnnotation.name().isEmpty()) {
                name = entityAnnotation.name();
            } else {
                name = entity.getSimpleName();
            }
        }
        return name;
    }
}
